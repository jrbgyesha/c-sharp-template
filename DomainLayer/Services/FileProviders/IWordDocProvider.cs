﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;

namespace DomainLayer.Services.FileProviders
{
    public interface IWordDocProvider
    {
        Task<byte[]> CreateFile(Stream file, Dictionary<string, string> conditionals);
    }

    public class NovationLetter : IWordDocProvider
    {

        public async Task<byte[]> CreateFile(Stream file, Dictionary<string, string> conditionals)
        {
            // open xml sdk - docx
            using (var mainDoc = WordprocessingDocument.Open(file, false))
            using (MemoryStream mem = new MemoryStream())
            {
                using (WordprocessingDocument resultDoc = WordprocessingDocument.Create(mem, DocumentFormat.OpenXml.WordprocessingDocumentType.Document, true))
                {
                    foreach (var part in mainDoc.Parts)
                        resultDoc.AddPart(part.OpenXmlPart, part.RelationshipId);

                    var body = resultDoc.MainDocumentPart.Document.Body;
                    var start = body.InsertAt<Paragraph>(await GetSimpleParagraph("PRIVATE AND CONFIDENTIAL", "Verdana", "18", true), 0);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Our Ref: " + conditionals["partnerref"] + conditionals["managerinitials"]), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(DateTime.Now.ToString("dd MMMM yyyy")), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(conditionals["clientname"]), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Attn: " + conditionals["clientcontact"]), start);

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Dear Sirs,"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Transfer of contractual arrangements from Deloitte & Touche LLP to Deloitte Tax Solutions Pte. Ltd.", "Verdana", "18", true), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("We refer to our " + conditionals["formatOfEngagement"] + " with " + conditionals["clientname"] + " (“you” or “Client”) dated " + conditionals["engagementDate"] + " (“Contract”)."), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);

                    var Section1 = new Paragraph();
                    Section1.Append(await GetSimpleRun("As part of an internal group restructuring, the tax and immigration business of Deloitte & Touche LLP will be transferred to Deloitte Tax Solutions Pte. Ltd. (UEN: 202008330C) (“Transfer”). The Transfer is intended to take effect from "));
                    Section1.Append(await GetSimpleRun("1 June 2020 ", "Verdana", "18", true));
                    Section1.Append(await GetSimpleRun("(“Transfer Date”). We will inform you if there is a change to the Transfer Date.  For all practical purposes, there should be minimal or no change in respect of all the services you are receiving.  The persons that have been interacting with you and your representatives and who are providing you the services under the Contract, will continue to do so."));
                    start = body.InsertAfter<Paragraph>(Section1, start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("For legal reasons and to simplify administration going forward, we would appreciate your formal consent and approval to the transfer of the Contract from Deloitte & Touche LLP to Deloitte Tax Solutions Pte. Ltd., upon the following terms:"), start);


                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetParagraphNumbered("as of and effective from the Transfer Date, Deloitte Tax Solutions Pte. Ltd. will replace Deloitte & Touche LLP as a party to the Contract and will be treated in all respects as if it had been named as a party to the Contract instead of Deloitte & Touche LLP;"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetParagraphNumbered("as of and effective from the Transfer Date, Deloitte Tax Solutions Pte. Ltd. will, in lieu of Deloitte & Touche LLP, be entitled to all existing and future rights and benefits, and will assume all existing and future liabilities and obligations, under the Contract;"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);

                    if (conditionals["corppass"].ToLower() == "yes")
                    {
                        start = body.InsertAfter<Paragraph>(await GetParagraphNumbered("you declare that, as of and effective from the Transfer Date, Deloitte Tax Solutions Pte. Ltd. will be appointed in place of Deloitte & Touche LLP as your CorpPass Administrator and shall be validly authorised to represent and act on your behalf for matters relating to the CorpPass Services;"), start);
                        start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    }

                    start = body.InsertAfter<Paragraph>(await GetParagraphNumbered("as of and effective from the Transfer Date, you irrevocably and unconditionally release and discharge Deloitte & Touche LLP from the further performance of all existing and future liabilities, obligations, claims and demands under or in connection with the Contract."), start);

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("There will be no change to our contact details (address, telephone and email) and you will be able to contact Deloitte Tax Solutions Pte. Ltd. at the same address, telephone numbers and email address."), start);

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);

                    Paragraph p1 = new Paragraph();
                    p1.Append(await GetSimpleRun("We kindly ask you to consent to the proposed transfer of the Contract to Deloitte Tax Solutions Pte. Ltd. by completing and signing the acknowledgement below, and returning the signed duplicate to us by "));
                    p1.Append(await GetSimpleRun("no later than Thursday, 30 April 2020", "Verdana", "17", true, true));
                    p1.Append(await GetSimpleRun("."));

                    start = body.InsertAfter<Paragraph>(p1, start);

                    if (conditionals["corppass"].ToLower() == "yes")
                    {
                        start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                        start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("As you may be aware, CorpPass is the only login method for online corporate transactions with government digital services, including that of the Inland Revenue Authority of Singapore and Ministry of Manpower. In this regard, we are happy to inform you that we are working directly with GovTech to migrate the client authorisations existing in CorpPass under Deloitte & Touche LLP to Deloitte Tax Solutions Pte. Ltd. This would mean that our clients do not have to re-authorise us in CorpPass under Deloitte Tax Solutions Pte. Ltd. Please note that you may receive an email notification once the migration to Deloitte Tax Solutions Pte. Ltd. has successfully be updated in your CorpPass account. However, we will hold off all access to the government digital services under Deloitte Tax Solutions Pte. Ltd. until we have received the signed novation letter from the Company. Should you have any queries on the above, please feel free to reach out to us."), start);

                    }

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Before the Transfer Date, we may also be raising our invoice for work done up to 31 May 2020.  If you receive an invoice from us, kindly make the payment in accordance with the details stated in such invoice."), start);

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Please treat this letter as well as any information contained herein confidential."), start);

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("This letter of consent is governed by the laws of Singapore."), start);

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("For the avoidance of doubt, other than any amendments expressly provided for in this letter of consent, all other terms and conditions under the Contract remain the same."), start);


                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("If you have any questions on the above, please contact " + conditionals["managername"] + " at " + conditionals["managerphone"] + "."), start);


                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("We take this opportunity to thank you for your continued trust and support in us. We also thank you for your kind cooperation in relation to the above."), start);

                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Yours faithfully"), start);
                    Paragraph pi = new Paragraph();

                    ImagePart imagePart = resultDoc.MainDocumentPart.AddImagePart(ImagePartType.Png);
                    string sig = "iVBORw0KGgoAAAANSUhEUgAAAJwAAABYCAYAAAAAwZ7vAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAABPkSURBVHhe7d0FbCvH2gbgX+ptq7ZqVWZmZmZmZm5vmVFlZmZmZsZ7y71lZmZmZp5fzzRftPVxTuzEcZxkX2lle3d2Fubdj2f9f6lEiSaiJFyJpqIkXImmoiRciaaiJFyJpqIkXImmoiRciaaiJFyJpqIkXImmoiRciaaiJFyJpqIkXImmoiRciaZiQBDur7/+al/+/PPP9Ouvv6b3338/nXXWWWmbbbZJJ510Unr77bfzNks1xPqvvvoqvfXWW+nLL79s77NE7RgwEi7I9sorr6Rtt902jTzyyGmIIYZI//rXv9JQQw2VlllmmUwmqCRdEOvBBx9MSy+9dBpllFHSvPPOm15++eW2FiVqxYAh3A8//JDOP//8NOGEE6YNN9ww3X333el///tfuu666/Lv0UYbLd1zzz1trQcFok433XRphBFGSBNPPHEaY4wx0g477pB++umnthYlasGAINy7776bllpqqbTgggumm266Kf3222/t6pPk+v7779M666yTbr755rY9/omvv/46LbHEEmnWWWdNTzzxRFbH66+/flp44YXTd99919aqRC3ol4T7448/Mql83n///WnuuedOBx54YJZyHWHXXXdNTz/9dP4e6tenPuw722yzpTfeeKN9+wUXXJBWXnnlbMs1CnE8S39Fv5VwBu3WW29N0047bf4MGNRqoFafffbZ/F2bWEg0ku2BBx7I28D6o48+Oi222GLp888/b1vbfZDEhxx6SHrnnXfa1vQ/9EvCIcTll1+eVWiRRB3h559/TltvvXV67733/tGOlFxrrbXSySefnNf//vvv+fO1115LU089ddpvv/0GcTC6Cv289NJLabvttmuXtP0R/Y5wJBtvkgp8/PHHM0EGRzYw0HvvvXcmHiAWXHLJJdm24xgE2RDjmWeeSfPPP3+WfrX0Xwuc9+uvv56OO+64hvTXqugXhAsiAPU588wzp0ceeST/hsENoG32EZMr9iPWNs8882RyFaWYNi+88ELaZJNN2sMojbK5xAKPOeaYtl/9E/1GwiHFy6+8nO2t66+/Pq/rjAj20Wb//fdPV111Vdvav9fvvvvuaa+99spqNQgbhDzyyCPTFVdckX/Htkbg+eefT5tttlnbr/6JfkM4EmnpZZbOarAokaAjUmjHy9xqq63Sp59+2t6OKl5++eUHyT7Y/u2336aNN944vfrqqw2TbAHHKwnXB0AKMbaPPuboTIJKIgxOCrH3Nt1005zu0k5fO+ywQzrxxBPb7bYi2G3bb799e3vHqiR4V6CvDz/8MJ9Lo4ncSuizhIvB9ilEISf6448/DkKQatAmJBdb7M4778zrEOzhhx9Oyy67bPr444/b2wa0583+5z//aVvTWJCyJBwy91f0acL98ssv2TlYaKGFsoqrF8Ibq622WpYsQTiSUgqsEshN5dlOrfYEZC123nnnnNnor+jTKlW4YpFFFkm33XZb/h3hjFpx9tlnp3333bdd4onZLbDAAjl1VU1Nsg+POuqof0i9RkGfJJvwDHu0v6JPE+7ggw/OKoikC4LUav/In1KdVCgY8MMOOyyXKhXJFjaajIKKkueee65tS2PhGB6YffbZJz311FNta/sf+iThDA5ptPjii6ePPvqobW19uOGGG9JKK62UiQfU5ZJLLtlhfxdffHFOf/WUfRVSVvXKf//73x6Roq2APkU4A2IgeJLzzTdfNva7AhJx9dVXz+VJgRNOOCEdcsghVSXkF198kTMLpKFz6AkEwZzTqaeemr/3R/Q5CWdg2F68UqmoeiRBkIXNJ84mU0CNMdJXWGGFqqoMATkRxSBwT5EO2I/suHrt0b6CPkU4g62igupTUVGrvRaw/yeffJIdA96t/a0T5lhllVWyei0SyqfA8AYbbJCJEO17ElT2AQcc0K7ae/p4zUafIhwJo3rjoosualtTO5CH1JDG2m233fI6g6lPJeeRDgvENnlWwdhmAal33HHHXH3s+I1CELe3CdynCKfoUaC2K2XdCKesnC1WrGFTKSIWR/JVAkE32mijDm3FRg5ekRDKoU477bT8uxEgndmoPWkK1Io+QzgkMXGFd9oV+8b+ysQvvfTSf9x4cTXVvpUw8CbJKE8aXJFlowax2A91HxmH7vTvGtwrBN5jjz1KwtUDAVoFj10ZBBKRGt11t12zhxq2mPVI+NBDD7W1/CcOO/ywdM4557RLnyKsMz9C4Fnetbso2qMffPBBlqycmmrHrgefffZZnnvRlUxMT6BPEM68hFVXXTUPRD0DEOQU7qA2eaMxsJ58dlIxFlcE54S9GNKteFzflZxPNNFEabzxxktzzDFHDp0Eoq0+VBHXK5FDlT/22GP5/OMBq7z24voiYcFvD9SWW26Zr6NV0PKEk5BntF922WX5xlbe9I4QA3XNNddkQrz44ottW/6GAVGWVC1vClStTIZ2lcdE3I023ihPLZxkkkmylHSeAYQRshHrk+yvJEMtYMPJfDh2EMtn5bnEdcYCCg922mmnXAY/zjjj5HKrVkHLEi4G+tprr81T8oqOQtzYzqBIEtlCZRb3I3lMHZTArwQHQlxOKRI4j1hAcHb00UfP/UuPXX311YMQAemcu+PLatQLzoxYIeIGmdwTn998800O5bgvsh/mzDq+bTfeeGOaccYZs4RUPezai9fd22hZwrlJ1JmYmZLuygHtDE8++WSaaeaZ0h133JF/x01HBH0JHpNw8TtgUE3A+fe//51Vsu0BfVinXs6+YmWcijfffLOtxd+IwfcZ3+uFKY1rr712lk6qSNigSudJ00knnTRPyF5zzTXzFEg2mooXEpHE5RjFA1u8tlZAyxLOjTLPoJ6oe0gAZJthhhly2Xi1wRbfYuyzDQPRjmpk191+++35dwyaxX7SW3PNNVd69NFHcxu2JRJWIgY6PjtDsb3juOYzzjgjHX744fm3uRWk1uSTT57XhcRXVj/99NOn4YcfPktTdl8royUJhzhiR6ozPLm1IAadmptpppnS6aef3v6UV4Ka4USQoLYXF9JNCXmlhEBIx5B14JXarjbOLKtqsF+9kk17/bIfp5xyyvxKCVIsAsC2I5v3m8R5aW+C9i233NI+0Tu2tSJaVsLJCLBB3NBa4CaTaNNMM02uuCgiBsAnyWFAqVS/YwEEJCVIyEpow1ZadNFFc72a7wa+s0nLtZBO33Gdvsv1kqKunxds9hh7UmjDg0J1FhHnD8XvrYiWJBy7hVqr9TUKBpX6RTYqxU2PGy/uFrCOh8l7RBgSK9pa9txzz3TssccOoiJj+ymnnJJtN985BGussUZVde+Y2sa51ALHZKshHumO+ILcXjMx/vjj5zc2jTjiiHlydzVHp15p2ltoGcK50QbHp/TV4N5kBDGQ1AmPkjFtIvHgYB9ScIstthjEGVApwmuNuQyV4C1K8EtzCaYut9xy7fZSnEsM+r333pu9WKk4cE0Wpen6d2z7aO+TypQNENeTZeB9zjnXnDkd9cyzz6QVV1wxB4Eb+R6T3kJLEK44AN5g5IZ72mMgq8E2Jd/sNRmImAfQ0T7WW5Q1sXeKIF0Uc1555ZVtawYFMiOkdJcMw+ZbbN5uWwV59Y9UwhneO2JSTMCcU+fKNqMytXW9iKz0iQSjPqlpXid1yhlAQJI0XqQDHV1jT6J4zO4cv6VUqoi/CTGdvQ9EmTepZgANfkiWQEf7kRAkIXUasK/qEyo8JsdU25+XKOYmFIJ4MdMroB/E22WXXdJwww2XJ8P4bfEQTTHFFHlytcg/tYho7D/SC7EEtpFxpJFGytck1uZezD777GmqqabKzkygOwPeHTTiuC1BOBdiUenKIA6JVwlqSam3kIdI+kcff9SurjqDPu+6665MVN5c9E9y8YZJrjiPascW3KXqL7zwwjwvlZ1m0dbx9S9BPssss2QCyRQIXZx33nlpsskmy9JTOw4JYomxeWA4Icrb7c97VtGCjKDU3Js6EZhnXO28mgnnxdSpVllTK1pGwsk7CvIKosaNJTXiu/iX0qJ4iUxXYIJMTJIxwIjH6+usFMg5ILjCSBUrYnCxHvTF8+VZUn2kFseENJNaIp1CCiMh6UdtstsqHZQi9C9WuPnmm6cnnuzaNesjzhMqf1eD6wFmjQeERy9q4Po8AOOOO2569bW/iwHiumpFrxCu8iT9Fvsy8L6HxACSRy5VTErMy1NWi0SrhH4FTqkr0L9cKakVEqUjON7xxx+fb/ZBBx2USRLXwHbkhFB/XrOlX1JYBoD6vu+++3I7iIHWpivXAJ2RpRrsUzxniON7AKh2Xj4JLdHvwafKxxprrOz8jD322PlVtaOOOmrOcPhkk3YFvUK4uGnx6XULJA3vD9wMdpbEt4s2oPHutq4OlP3WW2+9XNmrHzYYW4rXWByIatD+iCOOaPcWAyF1ORzUomNY4jzj+irPOdZD8fvgoF2tbaGyve/sTzFKwWM5WC/+YTOSWDxwlcacKg+KjAZiCcUMO+ywOWVGKpPyxbkf9ZwT9LpKlUryVLFXPIUMbEQgTdhKgqxFQtR7gUUgjYXU9P44RK8cmEqQRrxKTzgpyyNmj3kYJphggjxISMhjdZ6VfXXnfKuhlv6chweUN86JQSb304uwebxIw0kR1qmWnXDN7NOQir5b14hraQrh4kQrP12Ei5aP5MExpgU5hQQi5tSIiwQ3TnyMoS6PqoJD39ZXU3FusnXmNHACSEQBaaktEsBDwsi3byX0293z7qyP2Ob8ebTUOXVPHVKDc845Z5bIhx56aJbE2lQ712ajaRJOKMOTxvBkgBPpPEZPnsQzm4eXKr1UHPzuDhzE4DHoEY76ZrsYgOIgGDyL41sMFkkosm//MPB7auDiPIuIdbGeRApD3j2UXfGQivvJgnhAXJulFdEUwhlEVR9DDz10fvqGHHLILN4Zo2wDNzBuqLbFGwzF710F0ijvoWK8fZyUU1PGYSgSSLGADIHY17rrrpvtygjwNguuNx46sUG1cWeeeWa2QcOAJ2HZYqRuhGcCfgfiAWoVNE2lklwi8GJoUjaqOthA7InKG1ZJsMrf9YIXxuCVJ3UsS8TzGMy2Mf59t/Bc2XfRtjhgBrAWsKGk0SqvrTMgNwdEyIRGcD7q33jxYnE0RTwA+rU4xuBQ6zk3A73mNLChBEQZ8D2BkFyKN70WQvytUhUaLA+CuJrYHunW2eDVAn2ce+65ObQgbBLEKB6fdBVEtg45EUwxJ3uRYa/IkyNlHkcrSajuotcI5+mXTirOBeguDIyBDYjykw6kaTOhjMh8Ah4xQiF/SBlkpCJJURkENqwQBAmm4DK88rgO31tJQnUXvUY40+94UcXBaBRIDI6B8IrMRSURO/reKCAUG5B3iGDOgV3IJpT6YkqoDBHIJmFD8jmXWPoreo1wYllceO56o4C87EOqTKqJV2mpVKWVCIliaYRE4Q0jFq9R8SQj3wPAhqS6PWhjjjlme9WKB6I/qc3BodcIx21nrEfNfgx6NdiGNEGESmIglfkFJJqKjka8QTL6LiJIWSRwSE8PjoeIaiTd2KckWuR9STqLYLYUm4dNemigEC3Qa4QzSGwdwV4uv0EMIkE89RbrYrAricbbRDQVsuagBnkbDcfWd5wHacohEatTrsRWFBDmLEjLkWICzc4lrkHdmyC3UIawDEnYE+fayugVwsWgWUxQFhWX15NYJwU6euq1Z/MIbgp6iukZZOXeEZTVphHQDzJEf74r/VY6jmByvILCCgDkFivVNkkmL2l/25CNVFPaQ7INrrq4P6PXJBzE0y16bi4lb82//8lTChSzw8TpJO+pX6U+bB/vh+MBkjBFyRif0W93gcQMf+81UZaEZLxLhC86O0WyOx+LIlKElG5iv6nkFQKyjzRUvBBxoKFXCVcJg8HDEx6I97h5ybKUl7ylqH+lJKkXQcr4Hkv8JnUUW4rqy4Iozjzq6KMyWWo9tn5IacFj9hpvtBj+IeWkolwb4sbxBwJainCDQ3cGxeCHFEI2v6M/gVmz89WCSX3JTVKF7EEZiiAndOUc7BNLEV4XoQYtzmugoOUIZ2BIkiCGz64MdBH6COLomxT1B20i+gimiIBUVXXsWI5bJCUUiVcvqp1/rGvE9fUl9BkJB9WI0BkQjCpW5q1ymJrk0SqH4uFGfb4+tfXZCAJ01k9scz0DCX2CcAanKGFCAsagxmKdAWQXKe32Zku5SSVRXsuguJOhPtAGuZXQpyRcRxBE5k3yXHmzYmLefqRmTFl1USoGMUv0DvoU4ZAFeUgpwVNpLOEFNWImsQjCqq1jiBelWEmw1kGfIJwIPyKJh3mbEBtMCkuMTuqoSKggZUgyy0DzBFsZXSIcGwliYCNPGKhFbWljgaI0si5UpOpcoQoqEtHkXXmYxWOV6Fuoi3AhMZDCJBcpHYWNcqHSNUGyIFIRQbDoIwx/WQYvaEYmk4ejdEetnBIm1a8kVPQZfZTom6iZcDHgpIvUjqS78mehBmRDoECQKxDfSS5pHYWRAq3mFkhXyaWSZqauFStkQ4LG/vFZou+iLgmHMFSbCTBquULSVJM41nl7kFcVmFHvjUjqwhCMJGPwm3oXBNM+qjEqEdtLwvV91EU4A64yw1Q7M7YRSLUGKec9GkilLEfRoVnbwwwzTH6jtrowKpN0K+2vgY26VGpIGKR5/4P3s+0l56jSw6K8SMCVYa+MKJyLQEiqahKxxMBAXRIuEKTpSMXZVvQ8S5QIdIlwRRQlXyWsR7xSopUIdJtwJUrUg5JwJZqKknAlmoqScCWaipJwJZqKknAlmoqScCWaiJT+H0hf9TZXGE04AAAAAElFTkSuQmCC";
                    var bytes = Convert.FromBase64String(sig);
                    var contents = new MemoryStream(bytes);
                    imagePart.FeedData(contents);

                    start = body.InsertAfter<Paragraph>(await AddImageToBody(resultDoc.MainDocumentPart.GetIdOfPart(imagePart)), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Low Hwee Chua"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Head of Tax, Deloitte & Touche LLP"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Managing Director, Deloitte Tax Solutions Pte. Ltd."), start);
                    Paragraph pBordered = new Paragraph();
                    ParagraphProperties pProps = new ParagraphProperties();
                    ParagraphBorders pb = new ParagraphBorders();
                    pb.BottomBorder = new BottomBorder { Color = new StringValue("000000"), Size = (UInt32Value)12U, Space = (UInt32Value)1U, Val = BorderValues.Single };
                    pProps.Append(pb);
                    pBordered.Append(pProps);
                    start = body.InsertAfter<Paragraph>(pBordered, start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Acknowlegement", "Verdana", "17", true), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("The transfer of the Contract from Deloitte & Touche LLP to Deloitte Tax Solutions Pte. Ltd. upon terms stated above is agreed to and approved for and on behalf of " + conditionals["clientname"] + " by:"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Signature: 	____________________________"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Name: 		____________________________"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Designation: 	____________________________"), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph(""), start);
                    start = body.InsertAfter<Paragraph>(await GetSimpleParagraph("Date: 		____________________________"), start);


                    await UpdateHeaders(resultDoc.MainDocumentPart.HeaderParts, "[Client]", conditionals["clientname"]);
                    await UpdateHeaders(resultDoc.MainDocumentPart.HeaderParts, "[Subject]", "Transfer of contractual arrangements from Deloitte & Touche LLP to Deloitte Tax Solutions Pte. Ltd.");

                    resultDoc.Close();
                }
                return await Task.FromResult(mem.ToArray());
            }

        }


        private async Task UpdateHeaders(IEnumerable<HeaderPart> hParts, string search, string replacement)
        {
            foreach (var h in hParts)
                foreach (var p in h.Header.Elements<Paragraph>().Where(x => x.InnerText.Trim() == search))
                    foreach (var c in p.Elements<Run>().Where(x => x.InnerText.Trim() == search))
                        foreach (var t in c.Elements<Text>().Where(x => x.Text.Trim() == search))
                            t.Text = replacement;

        }
        private async Task<Paragraph> AddImageToBody(string relationshipId)
        {
            // Define the reference of the image.
            var element =
                 new Drawing(
                     new DW.Inline(
                         new DW.Extent() { Cx = 990000L, Cy = 792000L },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         new DW.DocProperties()
                         {
                             Id = (UInt32Value)1U,
                             Name = "Signature"
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A.Graphic(
                             new A.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)0U,
                                             Name = "signature.png"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A.Blip(
                                             new A.BlipExtensionList(
                                                 new A.BlipExtension()
                                                 {
                                                     Uri =
                                                        "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = relationshipId,
                                             CompressionState =
                                             A.BlipCompressionValues.Print
                                         },
                                         new A.Stretch(
                                             new A.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A.Transform2D(
                                             new A.Offset() { X = 0L, Y = 0L },
                                             new A.Extents() { Cx = 990000L, Cy = 792000L }),
                                         new A.PresetGeometry(
                                             new A.AdjustValueList()
                                         )
                                         { Preset = A.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)0U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)0U,
                         DistanceFromRight = (UInt32Value)0U,
                         EditId = "50D07946"
                     });

            return await Task.FromResult(new Paragraph(new Run(element)));
        }

        private async Task<Paragraph> GetSimpleParagraph(string text, string font = "Verdana", string size = "17", bool bold = false)
        {
            FontSize fontSize = new FontSize();
            fontSize.Val = new StringValue(size);
            RunFonts runFont = new RunFonts();
            runFont.Ascii = font;

            Paragraph p = new Paragraph();
            Run r = new Run();
            RunProperties rp = new RunProperties();
            rp.FontSize = fontSize;
            rp.RunFonts = runFont;
            if (bold)
                rp.Bold = new Bold();
            Text t = new Text(text) { Space = SpaceProcessingModeValues.Preserve };

            r.Append(rp);
            r.Append(t);
            p.Append(r);
            return await Task.FromResult(p);
        }
        private async Task<Run> GetSimpleRun(string text, string font = "Verdana", string size = "17", bool bold = false, bool underline = false)
        {
            FontSize fontSize = new FontSize();
            fontSize.Val = new StringValue(size);
            RunFonts runFont = new RunFonts();
            runFont.Ascii = font;

            Run r = new Run();
            RunProperties rp = new RunProperties();
            rp.FontSize = fontSize;
            rp.RunFonts = runFont;
            if (bold)
                rp.Bold = new Bold();
            if (underline)
                rp.Underline = new Underline { Val = UnderlineValues.Single };
            Text t = new Text(text) { Space = SpaceProcessingModeValues.Preserve };

            r.Append(rp);
            r.Append(t);
            return await Task.FromResult(r);
        }

        private async Task<Paragraph> GetParagraphNumbered(string text)
        {
            Paragraph p = new Paragraph();
            ParagraphProperties pp = new ParagraphProperties();
            pp.Append(await GetParagraphStyle("ListParagraph"));
            NumberingProperties np = new NumberingProperties();
            np.NumberingLevelReference = new NumberingLevelReference { Val = 0 };
            np.NumberingId = new NumberingId { Val = 22 };
            pp.Append(np);
            Indentation ind = new Indentation();
            ind.Hanging = new StringValue { Value = "720" };
            ind.Left = new StringValue { Value = "720" };
            pp.Append(ind);
            ContextualSpacing cs = new ContextualSpacing();
            pp.Append(cs);
            Justification js = new Justification();
            js.Val = JustificationValues.Both;
            pp.Append(js);
            ParagraphMarkRunProperties pmrp = new ParagraphMarkRunProperties();
            pmrp.Append(new RunFonts { Ascii = "Verdana", HighAnsi = "Verdana" });
            pmrp.Append(new FontSize { Val = "17" });
            pmrp.Append(new FontSizeComplexScript { Val = "17" });
            pmrp.Append(new Languages { Val = "en-SG" });
            pp.Append(pmrp);
            p.Append(pp);
            Run b1 = new Run();
            RunProperties b1props = new RunProperties();
            b1props.RunFonts = new RunFonts { Ascii = "Verdana", HighAnsi = "Verdana" };
            b1props.FontSize = new FontSize { Val = "17" };
            b1props.FontSizeComplexScript = new FontSizeComplexScript { Val = "17" };
            b1props.Languages = new Languages { Val = "en-SG" };
            b1.Append(b1props);
            Text t1 = new Text();
            t1.Text = text;
            b1.Append(t1);
            p.Append(b1);




            return await Task.FromResult(p);
        }

        private async Task<ParagraphStyleId> GetParagraphStyle(string val)
        {
            ParagraphStyleId psid = new ParagraphStyleId();
            psid.Val = val;
            return await Task.FromResult(psid);
        }

    }
}
