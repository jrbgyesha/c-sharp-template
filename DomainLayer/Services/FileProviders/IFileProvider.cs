﻿using DomainLayer.Services.CypherProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.File;

namespace DomainLayer.Services.FileProviders
{
    public interface IFileProvider
    {
        Task<string> FileUpload(Stream fileStream, string fileName, string path, bool useCypher = false);
        Task<CloudResult> FileDownload(string path, string fileName, bool useCypher = false);
    }
    public class UserFileProvider : IFileProvider
    {
        private readonly ICypherProvider cypherpvd;
        public UserFileProvider(ICypherProvider cypherpvd)
        {
            this.cypherpvd = cypherpvd;
        }
        public async Task<string> FileUpload(Stream fileStream, string fileName, string path, bool useCypher = false)
        {
            //Paths for Uploads are [clientupload] or [generated]
            try
            {
                CloudFileDirectory currentDir = GetDirectory(path);
                byte[] xbyte = await ReturnBytes(fileStream);
                if (useCypher)
                {
                    var keys = GetCypherKeys();
                    xbyte = cypherpvd.Encrypt(xbyte, keys.key, keys.iv);
                }
                //Create a reference to the filename that you will be uploading
                CloudFile cloudFile = currentDir.GetFileReference(fileName);

                //Upload the file to Azure.
                await cloudFile.UploadFromByteArrayAsync(xbyte, 0, xbyte.Length);
                fileStream.Dispose();
                return cloudFile.Name;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public async Task<CloudResult> FileDownload(string path, string fileName, bool useCypher = false)
        {

            CloudFileDirectory currentDir = GetDirectory(path);

            //Create a reference to the filename that you will be uploading
            CloudFile cloudFile = currentDir.GetFileReference(fileName);
            Stream fileStream = await cloudFile.OpenReadAsync();
            if (useCypher)
            {
                MemoryStream ms = new MemoryStream();
                fileStream.CopyTo(ms);
                var keys = GetCypherKeys();

                byte[] decypher = cypherpvd.Decrypt(ms.ToArray(), keys.key, keys.iv);
                fileStream = new MemoryStream(decypher);
            }
            return new CloudResult { stream = fileStream, contentType = cloudFile.Properties.ContentType };
            //return File(fileStream, cloudFile.Properties.ContentType, "test.msg");
        }

        private async Task<byte[]> ReturnBytes(Stream fileStream)
        {
            MemoryStream ms = new MemoryStream();
            fileStream.Position = 0;
            await fileStream.CopyToAsync(ms);
            return ms.ToArray();
        }
        private CloudFileDirectory GetDirectory(string path)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=sgtaxstorage;AccountKey=DZY9fJ5KG0kyARn4XL4Ve464JLgsTfo+/MQjuW3ISeqb9tzZTD27zl/HBYUwhuPl+4+enw9llijwsd8mmitzxg==;EndpointSuffix=core.windows.net");
            // Create a CloudFileClient object for credentialed access to Azure Files.
            CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
            CloudFileShare share = fileClient.GetShareReference("NewShare");
            CloudFileDirectory rootDir = share.GetRootDirectoryReference();
            CloudFileDirectory currentDir = rootDir.GetDirectoryReference(path);
            currentDir.CreateIfNotExists();
            return currentDir;
        }

        private CypherKey GetCypherKeys()
        {
            return new CypherKey
            {
                key = new byte[16] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
                iv = new byte[16] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
            };
        }
    }

    public class CypherKey
    {
        public byte[] key { get; set; }
        public byte[] iv { get; set; }

    }

    public class CloudResult
    {
        public Stream stream { get; set; }
        public string contentType { get; set; }
    }
}
