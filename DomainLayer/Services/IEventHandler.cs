﻿using DomainLayer.Events;
using DomainLayer.Models;
using DomainLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Services
{
    public interface IEventHandler<TEvent>
    {
        Task Handle(TEvent e);
    }

    public class NotifyUserOfUpdate : IEventHandler<UserUpdated>
    {
        private readonly IRepositoryService<User> userRepo;
        public NotifyUserOfUpdate(IRepositoryService<User> userRepo)
        {
            this.userRepo = userRepo;
        }
        public async Task Handle(UserUpdated e)
        {
            //Tell User Of Update Example
            var u = await userRepo.All();
            
        }
    }

}
