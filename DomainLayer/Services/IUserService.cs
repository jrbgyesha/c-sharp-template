﻿using DomainLayer.Models;
using DomainLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(string oid);


    }
    public class UserService : IUserService
    {
        private readonly IRepositoryService<User> userRepo;

        public UserService(IRepositoryService<User> userRepo)
        {
            if (userRepo == null) throw new ArgumentNullException("repository");
            this.userRepo = userRepo;
        }

        public async Task<User> GetUser(string oid)
        {
            var users = await userRepo.All();
            return users.Where(x=> x.OID == oid).FirstOrDefault();
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            var users = await userRepo.All();
            return users.Where(x => !x.IsDeleted);
        }
    }
}

