﻿using DomainLayer.Events;
using DomainLayer.Models;
using DomainLayer.Repositories;
using DomainLayer.Services.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Services
{
    public interface ICommandService<TCommand>
    {
        Task Execute(TCommand command);
    }

    public class InsertUserPropertyService : ICommandService<InsertUserProperty>
    {
        private readonly IRepositoryService<UserProperty> userpropRepo;
        public InsertUserPropertyService(IRepositoryService<UserProperty> userpropRepo)
        {
            if (userpropRepo == null) throw new ArgumentNullException("user prop Repo missing");
            this.userpropRepo = userpropRepo;
        }

        public async Task Execute(InsertUserProperty command)
        {
            await this.userpropRepo.AddSingle(new Models.UserProperty
            {
                Key = command.Key,
                Value = command.Value,
                Type = command.Type,
                UserID = command.UserID
            });
        }
    }
    public class UpdateUserPropertyService : ICommandService<UpdateUserProperty>
    {
        private readonly IRepositoryService<UserProperty> userpropRepo;
        public UpdateUserPropertyService(IRepositoryService<UserProperty> userpropRepo)
        {
            if (userpropRepo == null) throw new ArgumentNullException("user prop Repo missing");
            this.userpropRepo = userpropRepo;
        }
        public async Task Execute(UpdateUserProperty command)
        {
            var userProp = await userpropRepo.Single(command.UserPropertyID);
            userProp.Value = command.Value;
            await this.userpropRepo.UpdateSingle(userProp);
        }
    }

    public class RegisterUserService : ICommandService<List<RegisterUser>>
    {
        private readonly IRepositoryService<UserProperty> userpropRepo;
        public RegisterUserService(IRepositoryService<UserProperty> userpropRepo)
        {
            if (userpropRepo == null) throw new ArgumentNullException("User Property Repo missing");
            this.userpropRepo = userpropRepo;
        }
        public async Task Execute(List<RegisterUser> command)
        {
            var props = await this.userpropRepo.All();
            List<UserProperty> additions = new List<UserProperty>();
            List<UserProperty> updates = new List<UserProperty>();
            foreach (var c in command)
            {
                if (props.Any(x => x.UserID == c.UserID && x.Key == c.Key))
                {
                    var p = props.Where(x => x.UserID == c.UserID && x.Key == c.Key).First();
                    p.Value = c.Value;
                    updates.Add(p);
                }
                else
                    additions.Add(new UserProperty { Key = c.Key, Type = PropertyDataType._string, Value = c.Value, UserID = c.UserID });
            };
            if (additions.Count > 0)
                await userpropRepo.AddMany(additions);
            if (updates.Count > 0)
                await userpropRepo.UpdateMany(updates);
        }
    }

    public class AddClaimService : ICommandService<List<AddClaim>>
    {
        private readonly IRepositoryService<Claim> claimRepo;
        public AddClaimService(IRepositoryService<Claim> claimRepo)
        {
            if (claimRepo == null) throw new ArgumentNullException("Claim Repo missing");
            this.claimRepo = claimRepo;
        }
        public async Task Execute(List<AddClaim> command)
        {
            var claims = await this.claimRepo.All();
            List<Claim> additions = new List<Claim>();
            List<Claim> updates = new List<Claim>();
            foreach (var c in command)
            {
                if (claims.Any(x => x.UserID == c.UserID && x.Key == c.Key && x.Type == c.Type))
                {
                    var _claim = claims.Where(x => x.UserID == c.UserID && x.Key == c.Key && x.Type == c.Type).First();
                    _claim.Value = c.Value;
                    updates.Add(_claim);
                }
                else
                    additions.Add(new Claim { Key = c.Key, Type = c.Type, Value = c.Value, UserID = c.UserID });
            };
            if (additions.Count > 0)
                await claimRepo.AddMany(additions);
            if (updates.Count > 0)
                await claimRepo.UpdateMany(updates);
        }
    }
    public class RemoveClaimService : ICommandService<RemoveClaim>
    {
        private readonly IRepositoryService<Claim> claimRepo;
        public RemoveClaimService(IRepositoryService<Claim> claimRepo)
        {
            if (claimRepo == null) throw new ArgumentNullException(nameof(claimRepo));
            this.claimRepo = claimRepo;
        }
        public async Task Execute(RemoveClaim command)
        {
            var claims = await claimRepo.All();

            var removals = claims.Where(x => x.Key == command.Key && x.Value == command.Value && x.Type == command.Type && x.UserID == command.UserID).ToList();
            foreach (var r in removals)
            {
                await claimRepo.Remove(r);
            }
        }
    }

    public class AddLogService : ICommandService<AddLog>
    {
        private readonly IRepositoryService<Log> logRepo;
        public AddLogService(IRepositoryService<Log> logRepo)
        {
            if (logRepo == null) throw new ArgumentNullException(nameof(logRepo));
            this.logRepo = logRepo;
        }
        public async Task Execute(AddLog command)
        {
            await logRepo.AddSingle(new Log { AccessDate = DateTime.Now, OID = command.OID, PageName = command.PageName, Properties = command.Props });
        }
    }

}
