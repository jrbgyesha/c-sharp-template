﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Services.Conditionals
{
    public interface IConditionalService
    {
        Task<string> GetConditional();
    }


    public class ConditionalA : IConditionalService
    {
        public Task<string> GetConditional()
        {
            return Task.FromResult("TestA");
        }
    }
    public class ConditionalB : IConditionalService
    {
        public Task<string> GetConditional()
        {
            return Task.FromResult("TestB");
        }
    }
}
