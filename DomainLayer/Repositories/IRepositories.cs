﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Repositories
{
    public interface IRepositoryService<TCommand>
    {
        Task<IEnumerable<TCommand>> All();
        Task<TCommand> Single(int id);
        Task AddSingle(TCommand item);
        Task AddMany(List<TCommand> items);
        Task UpdateSingle(TCommand item);
        Task UpdateMany(List<TCommand> items);
        Task Remove(TCommand item);
    }
}
