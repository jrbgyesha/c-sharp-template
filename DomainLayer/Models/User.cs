﻿using System.Text.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DomainLayer.Models
{
    public class User
    {

        public int UserID { get; set; }
        public string OID { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<UserAudit> UserAudits { get; set; }
        public virtual ICollection<UserProperty> UserProperties { get; set; }
        public virtual ICollection<Claim> Claims { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
        public string GetUserProperty(UserPropertyKey key)
        {
            var result = this.UserProperties.Where(x => x.Key == key).FirstOrDefault();
            return result.Value ?? "";
        }
    }

    public class UserProperty
    {
        public int UserPropertyID { get; set; }
        public UserPropertyKey Key { get; set; }
        public string Value { get; set; }
        public PropertyDataType Type { get; set; }
        public int UserID { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }

    }

    public enum UserPropertyKey
    {
        firstname = 1,
        lastname = 2,
        email = 3,
        country = 4,
        function = 5,
        department = 6,
        serviceLine = 7,
        photo = 8,
        employeeID = 9,
        jobTitle = 10,
        careerLevel = 11,
    }

    public enum PropertyDataType
    {
        _string = 1,
        _double = 2,
        _int = 3,
        _date = 4,
        _bytes = 5
    }

    public class UserAudit
    {
        public int UserAuditID { get; set; }
        public UserAuditKey Key { get; set; }
        public string Value { get; set; }
        public DateTime Date { get; set; }
        public int UserID { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }

    }

    public enum UserAuditKey
    {
        CreatedProfile = 1,
        ModifiedProfile = 2,
        SubscribedToEvent = 3,
        UnSubscribedToEvent = 4,
        ViewedReport = 5,
        SignedAcknowledgement = 6
    }

    public class Claim
    {
        public int ClaimID { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public ClaimType Type { get; set; }
        public int UserID { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }
    }

    public enum ClaimType
    {
        role = 1,
        action = 2,
        graph = 3,
        registration = 4

    }
}

