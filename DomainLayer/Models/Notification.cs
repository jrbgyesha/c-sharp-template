﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainLayer.Models
{
    public class Notification
    {
        public int NotificationID { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationMessage { get; set; }
        public DateTime? NotificationActiveDate { get; set; }
        public DateTime? NotificationExpireDate { get; set; }
        public bool NotificationSeen { get; set; }
        public bool NotificationDeleted { get; set; }

        public int UserID { get; set; }


        public virtual User User { get; set; }
    }
}
