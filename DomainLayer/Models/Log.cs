﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainLayer.Models
{
    public class Log
    {

        public int LogID { get; set; }
        public string OID { get; set; }
        public string PageName { get; set; }
        public string Properties { get; set; }
        public DateTime AccessDate { get; set; }

    }
    public class LoggerProp
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
