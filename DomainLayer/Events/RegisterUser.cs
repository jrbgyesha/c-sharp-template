﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainLayer.Events
{
    public class RegisterUser
    {
        public UserPropertyKey Key { get; set; }
        public string Value { get; set; }
        public int UserID { get; set; }

    }
}
