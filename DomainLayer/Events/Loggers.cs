﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainLayer.Events
{
    public class AddLog
    {
        public string OID { get; set; }
        public string PageName { get; set; }
        public string Props { get; set; }
    }
}
