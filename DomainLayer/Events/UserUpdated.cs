﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainLayer.Events
{
    public class UserUpdated
    {
        public int UserID { get; set; }
        public string Message { get; set; }
    }
}
