﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainLayer.Events
{
    public class AddClaim
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public ClaimType Type { get; set; }
        public int UserID { get; set; }
    }
    public class RemoveClaim
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public ClaimType Type { get; set; }
        public int UserID { get; set; }
    }
}
