﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainLayer.Events
{
    public class InsertUserProperty
    {
        public UserPropertyKey Key { get; set; }
        public string Value { get; set; }
        public PropertyDataType Type { get; set; }
        public int UserID { get; set; }
        public InsertUserProperty(UserPropertyKey Key, string Value, PropertyDataType Type, int UserID)
        {
            this.Key = Key;
            this.Value = Value;
            this.Type = Type;
            this.UserID = UserID;
        }

    }

    public class UpdateUserProperty
    {
        public string Value { get; set; }
        public int UserPropertyID { get; set; }
        public UpdateUserProperty(int UserPropertyID, string Value)
        {
            this.UserPropertyID = UserPropertyID;
            this.Value = Value;
        }
    }
}
