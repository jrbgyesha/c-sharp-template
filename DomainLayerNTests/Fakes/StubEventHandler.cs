﻿using DomainLayer.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayerNTests.Fakes
{
    public class StubEventHandler<TEvent> : IEventHandler<TEvent>
    {
        public async Task Handle(TEvent e)
        {
            await Task.Run(() =>
            {
            });
        }
    }
}
