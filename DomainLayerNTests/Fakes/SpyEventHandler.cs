﻿using DomainLayer.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayerNTests.Fakes
{
    public class SpyEventHandler<TEvent> : IEventHandler<TEvent>
    {
        public List<TEvent> HandledEvents { get; private set; } = new List<TEvent>();

        public TEvent HandledEvent
        {
            get
            {
                CollectionAssert.IsNotEmpty(this.HandledEvents);
                return this.HandledEvents[0];
            }
        }

        public async Task Handle(TEvent e)
        {
            await Task.Run(() =>
            {
                Assert.IsNotNull(e);
                this.HandledEvents.Add(e);
            });
        }
    }
}
