using DomainLayer.Events;
using DomainLayer.Models;
using DomainLayer.Repositories;
using DomainLayer.Services;
using DomainLayerNTests.Fakes;
using InMemoryDataLayer;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DomainLayerNTests
{
    [TestFixture]
    public class UserServiceTests
    {
        private DbContextOptions<InMemDataContext> options;
        [SetUp]
        public void Setup()
        {
            options = new DbContextOptionsBuilder<InMemDataContext>()
                .UseInMemoryDatabase(databaseName: "DataBase")
                .Options;
            using (var context = new InMemDataContext(options))
            {
                context.Users.Add(new User { OID = "12373482107", IsDeleted = false });
                context.Users.Add(new User {  OID = "12373482108", IsDeleted = true });
                context.UserProperties.Add(new UserProperty { Key = UserPropertyKey.careerLevel, Type = PropertyDataType._string, UserID = 1,  Value = "Singapore" });
                context.SaveChanges();
            }
        }

        [Test]
        public async Task UserRepository_ValidIncrementOfUser()
        {
            using (var context = new InMemDataContext(options))
            {
                IRepositoryService<User> userRepo = new InMemUserRepository(context);
                var users = await userRepo.All();
                Assert.IsNotNull(users.Where(x=> x.UserID == 1).FirstOrDefault());
                Assert.IsNotNull(users.Where(x => x.UserID == 2).FirstOrDefault());
            }
        }
        [Test]
        public async Task UserRepository_UserConstructsWithClaimListInstantiated()
        {
            using (var context = new InMemDataContext(options))
            {
                IRepositoryService<User> userRepo = new InMemUserRepository(context);
                var users = await userRepo.All();
                foreach (var u in users)
                {
                    Assert.IsNotNull(u.Claims);
                }
            }
        }
        [Test]
        public async Task UserRepository_UserConstructsWithUserPropertiesListInstantiated()
        {
            using (var context = new InMemDataContext(options))
            {
                IRepositoryService<User> userRepo = new InMemUserRepository(context);
                var users = await userRepo.All();
                foreach (var u in users)
                {
                    Assert.IsNotNull(u.UserProperties);
                }
            }
        }
        [Test]
        public async Task UserRepository_UserConstructsWithUserAuditListInstantiated()
        {
            using (var context = new InMemDataContext(options))
            {
                IRepositoryService<User> userRepo = new InMemUserRepository(context);
                var users = await userRepo.All();
                foreach (var u in users)
                {
                    Assert.IsNotNull(u.UserAudits);
                }
            }
        }
    }
}