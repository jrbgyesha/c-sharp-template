﻿using DomainLayer;
using DomainLayer.Models;
using DomainLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataLayer
{
    public class SqlUserRepository : IRepositoryService<User>
    {
        private readonly SQLDataContext context;

        public SqlUserRepository(SQLDataContext context)
        {
            if (context == null) throw new ArgumentNullException("context not set");
            this.context = context;
        }

        public async Task AddMany(List<User> items)
        {
            await context.Users.AddRangeAsync(items);
            await context.SaveChangesAsync();
        }

        public async Task AddSingle(User item)
        {
            await context.Users.AddAsync(item);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<User>> All()
        {
            return await context.Users
                .Include(x => x.Claims)
                .Include(x => x.UserAudits)
                .Include(x => x.UserProperties)
                .ToListAsync();
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await Task.FromResult(this.context.Users
                .Include(x => x.Claims)
                .Include(x => x.UserAudits)
                .Include(x => x.UserProperties));

        }

        public async Task Remove(User item)
        {
            context.Users.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task<User> Single(int id)
        {
            return await context.Users
                .Include(x => x.Claims)
                .Include(x => x.UserAudits)
                .Include(x => x.UserProperties)
                .Where(x => x.UserID == id).FirstOrDefaultAsync();
        }

        public async Task UpdateMany(List<User> items)
        {
            context.Users.UpdateRange(items);
            await context.SaveChangesAsync();
        }

        public async Task UpdateSingle(User item)
        {
            context.Users.Update(item);
            await context.SaveChangesAsync();
        }
    }
}

