﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace SQLDataLayer
{
    public class SQLDataContext : DbContext
    {
        private readonly string connectionString;

        public SQLDataContext(DbContextOptions<SQLDataContext> options) : base(options)
        {
        }

        public DbSet<Log> Logs { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<UserProperty> UserProperties { get; set; }
        public DbSet<UserAudit> UserAudits { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
