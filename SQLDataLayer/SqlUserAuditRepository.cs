﻿using DomainLayer.Models;
using DomainLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataLayer
{
    public class SqlUserAuditRepository : IRepositoryService<UserAudit>
    {
        private readonly SQLDataContext context;

        public SqlUserAuditRepository(SQLDataContext context)
        {
            if (context == null) throw new ArgumentNullException("context not set");
            this.context = context;
        }

        public async Task AddMany(List<UserAudit> items)
        {
            await context.UserAudits.AddRangeAsync(items);
            await context.SaveChangesAsync();
        }

        public async Task AddSingle(UserAudit item)
        {
            await context.UserAudits.AddAsync(item);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserAudit>> All()
        {
            return await context.UserAudits.ToListAsync();
        }

        public async Task<IEnumerable<UserAudit>> GetUserAudits()
        {
            return await Task.FromResult(this.context.UserAudits);
        }

        public async Task Remove(UserAudit item)
        {
            context.UserAudits.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task<UserAudit> Single(int id)
        {
            return await context.UserAudits.Where(x => x.UserAuditID == id).FirstOrDefaultAsync();
        }

        public async Task UpdateMany(List<UserAudit> items)
        {
            context.UserAudits.UpdateRange(items);
            await context.SaveChangesAsync();
        }

        public async Task UpdateSingle(UserAudit item)
        {
            context.UserAudits.Update(item);
            await context.SaveChangesAsync();
        }
    }
}
