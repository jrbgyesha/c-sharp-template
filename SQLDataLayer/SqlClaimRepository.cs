﻿using DomainLayer;
using DomainLayer.Models;
using DomainLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataLayer
{
    public class SqlClaimRepository : IRepositoryService<Claim>
    {
        private readonly SQLDataContext context;

        public SqlClaimRepository(SQLDataContext context)
        {
            if (context == null) throw new ArgumentNullException("context not set");
            this.context = context;
        }

        public async Task AddMany(List<Claim> items)
        {
            await context.Claims.AddRangeAsync(items);
            await context.SaveChangesAsync();
        }

        public async Task AddSingle(Claim item)
        {
            await context.Claims.AddAsync(item);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Claim>> All()
        {
            return await context.Claims.ToListAsync();
        }

        public async Task<IEnumerable<Claim>> GetClaims()
        {
            return await Task.FromResult(this.context.Claims);
        }

        public async Task Remove(Claim item)
        {
            context.Claims.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task<Claim> Single(int id)
        {
            return await context.Claims.Where(x => x.ClaimID == id).FirstOrDefaultAsync();
        }

        public async Task UpdateMany(List<Claim> items)
        {
            context.Claims.UpdateRange(items);
            await context.SaveChangesAsync();
        }

        public async Task UpdateSingle(Claim item)
        {
            context.Claims.Update(item);
            await context.SaveChangesAsync();
        }
    }
}
