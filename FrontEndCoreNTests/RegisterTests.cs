using FrontEndCore.Helpers.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace FrontEndCoreNTests
{
    [TestFixture]
    public class RegisterTests
    {
        private ActionContext _actionContext;

        [SetUp]
        public void Setup()
        {
            _actionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext(),
                RouteData = new RouteData(),
                ActionDescriptor = new ActionDescriptor()
            };
        }
        [Test]
        public void RegisterFilter_ReturnsContextProfileWhenClaimFalse()
        {
            var authContext = new AuthorizationFilterContext(_actionContext, new List<IFilterMetadata>());
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim("registered", "flase"));
            ClaimsIdentity identity = new ClaimsIdentity(claims, "testing");
            authContext.HttpContext.User.AddIdentity(identity);
            var attribute = new RegisterClaimFilter(new Claim("registered", "true"));

            // When
            attribute.OnAuthorization(authContext);
            var result = ((RedirectResult)authContext.Result).Url;

            Assert.AreEqual("/Home/Profile", result);
        }
        [Test]
        public void RegisterFilter_ReturnsContextProfileWhenNoClaims()
        {
            var authContext = new AuthorizationFilterContext(_actionContext, new List<IFilterMetadata>());
            var attribute = new RegisterClaimFilter(new Claim("registered", "true"));

            // When
            attribute.OnAuthorization(authContext);
            var result = ((RedirectResult)authContext.Result).Url;

            Assert.AreEqual("/Home/Profile", result);
        }
        [Test]
        public void RegisterFilter_ReturnsContextNormalWhenClaimExistsForRegistration()
        {
            var authContext = new AuthorizationFilterContext(_actionContext, new List<IFilterMetadata>());
            var expectation = authContext.Result;
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim("registered", "true"));
            ClaimsIdentity identity = new ClaimsIdentity(claims, "testing");
            authContext.HttpContext.User.AddIdentity(identity);
            var attribute = new RegisterClaimFilter(new Claim("registered", "true"));

            // When
            attribute.OnAuthorization(authContext);
            var result = authContext.Result;

            Assert.AreEqual(expectation, result);
        }
    }
}