﻿using System;
using DomainLayer.Models;
using DomainLayer.Repositories;
using FrontEndCore.GraphServices;
using FrontEndCore.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.TokenCacheProviders.InMemory;
using Microsoft.Identity.Web.UI;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using SQLDataLayer;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace FrontEndCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Add Standard Services
            services.ConfigureStandardServices(Configuration);

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
            })
            .AddMicrosoftIdentityWebApp(Configuration.GetSection("AzureAd"))
            .EnableTokenAcquisitionToCallDownstreamApi(new string[] { "user.read" })
            .AddInMemoryTokenCaches();


            // Add AAD Identity Authorization
            services.AddControllersWithViews();
            services.AddDbContext<SQLDataContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DatabaseContext"), (sqlServerOptions) => {
                    sqlServerOptions.EnableRetryOnFailure(
                            maxRetryCount: 5,
                            maxRetryDelay: TimeSpan.FromSeconds(5),
                            errorNumbersToAdd: null);
                });
            });


            services.AddScoped<IRepositoryService<User>, SqlUserRepository>();
            services.AddScoped<IRepositoryService<UserAudit>, SqlUserAuditRepository>();
            services.AddScoped<IRepositoryService<UserProperty>, SqlUserPropertyRepository>();
            services.AddScoped<IRepositoryService<Claim>, SqlClaimRepository>();
            services.AddScoped<IRepositoryService<Log>, SqlLogRepository>();

            // Add Microsoft Graph Services
            services.AddGraphService(Configuration);
            services.AddScoped<IGraphAuthService, GraphAuthService>();
            services.AddScoped<IGraphConverterService, GraphConverterService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.EnvironmentName == "Development")
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
                //app.UseHsts();
            }

            ConfigureMiddleware(app, loggerFactory);

            app.UseHttpsRedirection();
            //EJS Support
            app.UseStaticFiles(new StaticFileOptions { 
                FileProvider = new PhysicalFileProvider(env.WebRootPath + @"\js"),
                RequestPath = "/js",
                ContentTypeProvider = new FileExtensionContentTypeProvider
                {
                    Mappings =
                    {
                        [".ejs"] = "text/html"
                    }
                }
            });
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name:"apis",
                    pattern:"api/[controller]"
                    );
                endpoints.MapControllerRoute(
                    name:"default",
                    pattern: "{controller=Home}/{action=Index}/{id?}"
                    );
                endpoints.MapRazorPages();
                endpoints.MapHub<ExampleHub>("/exampleHub");
            });
        }

        private static void ConfigureMiddleware(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.CreateLogger("Middleware");

            app.Use(async (context, next) =>
            {
                var middleware = new LoggingMiddleware(logger);

                await middleware.Invoke(context, next);
            });
        }
    }
}