﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Graph = Microsoft.Graph;

namespace FrontEndCore.GraphServices
{
    public interface IGraphConverterService
    {
        Dictionary<string, string> Convert(Graph.User u);
    }
    public class GraphConverterService : IGraphConverterService
    {

        public Dictionary<string, string> Convert(Graph.User u)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            result.Add("surname", u?.Surname ?? "");
            result.Add("givenname", u?.GivenName ?? "");
            result.Add("jobtitle", u?.JobTitle ?? "");
            result.Add("department", u?.Department ?? "");
            result.Add("country", u?.Country ?? "");
            result.Add("email", u?.Mail ?? "");
            result.Add("employeeid", u?.EmployeeId ?? "");
            return result;

        }
    }
}
