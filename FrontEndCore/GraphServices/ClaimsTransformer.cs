﻿using dm =DomainLayer.Models;
using DomainLayer.Repositories;
using Microsoft.AspNetCore.Authentication;
using SQLDataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DomainLayer.Events;

namespace FrontEndCore.GraphServices
{
    public class ClaimsTransformer : IClaimsTransformation
    {
        private readonly string IdentityName = "GenericSystemName";
        private readonly string RegisterName = "registered";
        private readonly string RegisterFalse = "false";
        private readonly IRepositoryService<dm.Claim> claimRepo;
        private readonly IRepositoryService<dm.User> userRepo;
        public ClaimsTransformer(IRepositoryService<dm.Claim> claimRepo, IRepositoryService<dm.User> userRepo)
        {
            this.claimRepo = claimRepo;
            this.userRepo = userRepo;
        }
        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            var oid = principal.Claims?
                .FirstOrDefault(x => x.Type.Equals("http://schemas.microsoft.com/identity/claims/objectidentifier", StringComparison.OrdinalIgnoreCase))?.Value;
            var users = await userRepo.All();
            ClaimsPrincipal transformed = await GetTransformedPrincipal(principal, oid, users.Where(x=> x.OID == oid).FirstOrDefault());
            transformed.AddIdentities(principal.Identities.Where(x => x.Name != IdentityName));
            return transformed;
        }
        private async Task<ClaimsPrincipal> GetTransformedPrincipal(ClaimsPrincipal principal, string oid, dm.User _user)
        {
            if (_user != null)
                return await AddClaimsForExistingUser(principal, _user);
            else
                return await AddClaimsForNewUser(principal, oid);
        }
        private async Task<ClaimsPrincipal> AddClaimsForNewUser(ClaimsPrincipal principal, string oid)
        {
            var transformed = new ClaimsPrincipal();
            List<Claim> claims = new List<Claim>();
            await AddNewUserToDatabase(oid);
            AddClaimIfNotExist(principal, claims, new dm.Claim { Key = RegisterName, Value = RegisterFalse });
            transformed.AddIdentity(new ClaimsIdentity(claims, IdentityName));
            return transformed;
        }

        private async Task AddNewUserToDatabase(string oid)
        {
            dm.User u = new dm.User { OID = oid, IsDeleted = false };
            u.Claims = new List<dm.Claim>() { new dm.Claim { Key = RegisterName, Value = RegisterFalse, Type = dm.ClaimType.registration } };
            await userRepo.AddSingle(u);
        }

        private async Task<ClaimsPrincipal> AddClaimsForExistingUser(ClaimsPrincipal principal, dm.User _user)
        {
            var transformed = new ClaimsPrincipal();
            List<Claim> claims = new List<Claim>();
            var dbClaims = await claimRepo.All();
            dbClaims.Where(x => x.UserID == _user.UserID).ToList().ForEach(x => AddClaimIfNotExist(principal, claims, x));
            transformed.AddIdentity(new ClaimsIdentity(claims, IdentityName));
            return await Task.FromResult(transformed);
        }

        private static void AddClaimIfNotExist(ClaimsPrincipal principal, List<Claim> claims, dm.Claim c)
        {
            if (!principal.HasClaim(c.Key.ToString(), c.Value))
                claims.Add(new Claim(c.Key.ToString(), c.Value));
        }
    }
}

