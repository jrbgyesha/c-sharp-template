﻿using DomainLayer.Events;
using DomainLayer.Services;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using Microsoft.Identity.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace FrontEndCore.GraphServices
{
    public interface IGraphAuthService
    {
        Task<User> GetGraphUser(string[] scopes);
        Task<User> CheckUser(string[] scopes, string email);
        Task<string> GetUserPhoto(string[] scopes);
        Task<Dictionary<string, string>> ConvertUser(User u);
        Task<bool> SendEmail(string[] scopes, string subject, List<string> tos, List<string> ccs, string body, List<Attachment> files);
    }


    public class MockAuthService : IGraphAuthService
    {
        private readonly IGraphConverterService converter;
        public MockAuthService(IGraphConverterService converter)
        {
            this.converter = converter;
        }
        public Task<Dictionary<string, string>> ConvertUser(User u)
        {
            return Task.FromResult(converter.Convert(u));
        }

        public Task<User> GetGraphUser(string[] scopes)
        {
            Microsoft.Graph.User me = new User()
            {
                GivenName = "Tyler",
                Surname = "Furrer",
                JobTitle = "Senior Manager",
                Country = "Singapore",
                CompanyName = "Deloitte & Touche LLP",
                Department = "Tax -Digital Transformation",
                Mail = "tfurrer@deloitte.com",
                OnPremisesExtensionAttributes = new OnPremisesExtensionAttributes
                {
                    ExtensionAttribute8 = "7"
                }


            };
            return Task.FromResult(me);
        }

        public Task<User> CheckUser(string[] scopes, string email)
        {
            Microsoft.Graph.User user = new User()
            {
                GivenName = "Johnny",
                Surname = "Fredrick",
                JobTitle = "Senior Manager",
                Country = "Singapore",
                CompanyName = "Deloitte & Touche LLP",
                Department = "Tax -Digital Transformation",
                Mail = "jfredrick@deloitte.com",
                OnPremisesExtensionAttributes = new OnPremisesExtensionAttributes
                {
                    ExtensionAttribute8 = "9"
                }


            };
            return Task.FromResult(user);

        }

        public Task<string> GetUserPhoto(string[] scopes)
        {
            return Task.FromResult("/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADwAPADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD1n/AU5elLg8cdhQwY7drhfm54zkelZG48UuKUDgcUoHNAhVGBThSjmloAMUYHSlGR/CaOccZI9CM0AMkDBcj8R61zHjLxXD4a0zzMq1zJ/qo88nH9K6OabYuF2t2KscA18zfEDxFJqXie8yxEcEjRxhjyMHv+NIaMbW9cubu5meSZnkkkLyMScsxrEj+0XUoSNC2TwAOtPtbaS/uQozyep7V2umaXFaoFCguOC2OtDaiVGDkzGtvD0rR4lBBBzjPerv8AYSpGQq7j2wK6eODIAxxViO0zxjj6VjKozqjROTXRFbbtXgetQnRV3Lwe+Tiu6FhzlcD6jrUMthu3Efl71PtGU6KOGl0ESKGVe2T/AJ71lz6fLA7KBz9OuK9J8hUz8ufUdqoT6dEdxGTnJ61SqGcqKPPf3iEtIHJ9QOlM82Mvt4Ldzj+tdRd6Yu1htAHpXOXtkYZCwPy55raMkzCUGjo/BniB9B1iKVWPlFvnGBke4z/KvpzSNXtNZ06O8s5kmhZeWQ8g+47V8h20ikcn5h0OetelfDfxJ/Z+oBDMI9zYcE4Vx2J9D2/Kgk+gc5HHSioYpVkjWQOrbhkbafmmTceaYeTS55xmkyfSkMCcDHP1pMd807H1owe1ADcAdevrTcCn4pOn5UDKx4PTsKUD86COR9BTqZI1VCsSqgFjkn1qUU0exp4x0oELj24pwoxS470AFKeOe9KBxWfrurw6Dol1qU43JBGWC5xuPYfiaAPM/iV4p1bw3rUS2V0qtcx5EDoCAB/ERnOf0rxG4R77UHdt0k0z5ZmPJJ61ratrF1rWq3OqXmHurlsgDhUUenoB0p2jWJ3m5fG48KKTdi4q7saem6WtpCuACx6nHNbtvbdOKSGLkZFa9tCSFwPqcVzTmd1OFhtvaZHAxitCOzKjkYx056VdtbcbsDnHp0rQ8gDgYHpWLZ0JGR9kAHzE4PfHSmPYqv3TgseprcMJIHzKcdsUx4AXO4LjrxSuM5aezKtlSc+nrVSaEH7sZXjB5rrZrSN+Qe3cVly2RyQKakS0jkLmA5YH86wNQsQ6kYPPTHau5u7TGeARjmsK6gGDxxW0JGE4nnwURTNGVOR371btJ3iuRJGcOvIINT63ZYcOOCOpHFZkDkkByAw6Mex/wrpWqOKSs7H0x8PPEw1jRUglkUTRDAB7j612mR+FfPXw1117DXo4HTfFIcMD1Ga+gCPmwOg6U0QyXvwaVenNQMxBxk/4VKhoETgUGkB7EUuKRQlNwDTsZ9qKBlXgkfQUopMc59hTgOKokUCnDHfpSAdhTwKBC4p+KQCnDrSAMVxvxNu0tPBd3mNGeT5FLDOzPfHr6V2fBFcF8XvL/wCEJ2MWBa6jK46H2NDBHzwAFypPzdOewrp9KhAt0UA8YzXKCUm+PoD6V2umAmBCc8jJrOeiN6S1NqCIHax49Kv27FhjOefpiqcb4XHGe1W7SF3IOcDP+TXKzuibNvOIuAcY645qx9pDYAByw7nmqkdscZ7n171L5LLjgDP51BpuXhIwA+fGOoxnNKbhCPmGPQgdKrADZzkHqaY6OpVudvT1pDsTSyqwwGXA5AqnIw6A9+lJKhK8Hn2qlKZFz19j3osJkNx8xI/Sse4hyD/LFabSHr+YqvMBs3d+9WjKRx2tW29MYHA/SuMjjKuUPBBIBz0Poa9F1GMNuOO3SuDuImW5m2j72SRXXTehxVVqdL4EuDD4itsgnDDt0wRxX0/kHpjHWvlbwjcNDrFvK4/iXLZ9/wCdfUqNlFPGSAePpWlzGRJjAOBzTk4Az1pqk89/enD1oJJl60/FMGTxUlBSG4xRS0euPSkMqHt9BSg8UuOn0FOAqiRVwR0pw/lSKMdKfgUAIOtOFKKUDNAha4b4rWn2jwZNOxIS1cS47E9K7qud8ewG48Aa5GOv2RmH4c0hnymATfgZ5Z8DFegWyi1t1XOTjk1wel/6Rq1mcZJIJzXdzyeWcohd15VfU+9Z1Dopaaj5r77KhcR78D1xn6etbGi67Y3MixORDkBlLtgN7Z7GsvT/AA/PdzCe5dZ2fsTgIPb0rRufCF5HHugEc6E5IbAI98dKwdjdSktTq7a6tdxjaVPMHPJxn6e3vV4QpLlFILDncD0rzU2OrW+UW32KrZVCMY9QK3tIv7uF2YFs9FSQ9AB0PsCalxRrGZ0slpIH2gdPfrSxRFSYXTiTofftWhFemSDLbW47jpxwaztUunFkJIwPMX50wevHSosaqRXklt9zQHh1xznv71Wk2BzGQemRxWJqmpeTctcR48yTnPZDjn8e1Ya63fCVsXDsF5KOm8fietUomUqqR0F5CE+ZTk1nPPgKByDnvWLea5qrSs0M9tKmeipjI/z78VJHqUd0g8xTDP8A3W6N9DVqJlzpjdSLJCzgZFcfOyvKe+etdpdHzbNvda4RTmdgexNbQMKpf0UBb+P5S4DgEV9SWePsNvgEYiXg9RxXzj4S0yTU9ct7eHG9nAwTgH15r6TiXbBGuT8qgHNWYMkB564pytxTOg6Uq+tMRZUmphUKZNTAHimCCjHtS0CgZWPUfQU4dOKbj+Q/lTqZIop4HJpAM04daAFAxThQKKAFxxWL4vmWDwXrcjHAFlKMgZ6rjpW3XIfEu9Fp4KuoQgea8/cRg9iRyfyo2BJt2R83eGLYtqVuxU/KmeewxXewxLgyNy2eK53wxbACaYnJXEY9q6ngQ8H5ugFYVGddKOg2TxBb6R8vDzE4Cjnn0AHJPtVVvHeul3SOEQIhKlW2hwR1P/1utEFgY7sXSgGYn5WxyKmutHl1DUftSoYWkx5pjHBP94DsfpUx5eppNVPsk9nqOrX9ut5cTTrE0phMkyApvHUZHT8cVI91NbXbRzxiOZOSByGHt+FaDTPZ6ZFpFte+VaxjlI4sFm7sTyST61h3kkJVIoS7iBNis5yTntUtK+hUVJL3tzrLTVQ0QYY2gYFZOp6wAmxcH0B7U3SQv2chsZIOc9qwdTAkuZBkgEHpUJam09IXL9vcRyo223aUDliF4H1NXrX+wgp+320kTnvwRj3Gc1JYBvMhgnsJ5NIUBwLRwHkP+33I9hXMeLdJtLfWLt7W0l+yXLrLCfJYPF0ymD0PHXkYNaRipHNOTir2udVeWWiX8GyGKErnOE4P5enrXLX2ipEx+ysdoH3GGfyqr/Z11p1glwswMjOWNkX3YTtgj7rVo2F8LqFWYkuODu6kds+4otysSakr2KewiB0YHgdM1xLqEuncf3jXocybixI65rzq7LJc4PABOR6mtYGNQ9P+DFk8uozXZHywxn35JxXtfbArzv4P2UcHhWacY8ySYKeewGf516KOnrVswe4dqcOKT8KcvLc0gRZiFTCo4xUlUhh1pQP5UlKOlMZWxz+ApR7UY5H0FKBQSKKkApoHNOoAUUvagUtABXE/EaUJaaepXPzyMPYha7auN+I9n5+jWlwc7YbgI/8AutxUz+FmtBpVFc8Z8Oj/AIlW8jG6Zzz35rdRDIuPxqjbWR02W8tAQ8KT7oJP76H/AA6Vs2i5GMdTXPM7acehYgiCj7p6ZxVp3Z4dqKQWHb9aswW4ZeW4PTNSmFSwxk+1ZXN+U5+SLcsggTHy4aQ/w/T3rLaFYiq4xt5NdbeQiO38tAAp5OK5O9y8oRTySRzVRd2S1Ys2Tl1kxwgHJFY95kXR9j610OmwbIyoJLMMY7ZrJ1m0aO7MgBANC+IqpH3CXThJbqriR0iZvlcdVPofateQC5TbcqcsPvBjyKg8PskqPDIoKOOc1pvYFCAkn7vshGfpzRzGUY3RkXOnQgfINoJz9fWs4WiQzhlGCetb7202QCPlA4IPNU57fuR064ouKUSi47YwQa891CAnWZIlHPm4FehSA+YB2rm7C3Sa81K7ZQyCTart2+lbQdkc8o80kj034W3BWK8sDgqI1lVgPfBr0boK4L4cWuP7Tu9veOBT26bmH6iu9q09DnqJKbSDtTo+TSDpinxA56UyC1GOKkIpEHFOqkMTil6ZoxxSUwK/f8BTxTOuPoKeKYh4x1pQM0g6U4UAKOlAooFADgKwvGMHneEtQ4BKKJBn/ZINbvbNQX9ot/p9xaOcLPG0ZPpkUmNOzTPAmabzsTcgcphcfKTnHvWtbMBGRjmq2rW1zaXQtrqMxTQsUKnuOxHqD1p9q+QCcgd/auWS0PRjL3rm1bHemS30rUjVTIcdMcVk2uAoKnHPSteDH38ZJ7Vg9zquVNRjK2zyEYABriLSOSeZpD2OBXe6tOrIIieMEHHrXByXMtlcLGECY6uTwcelVAzknudhpemyyW+6ONjk43gcD2rK8R6c0I5UgkZ5rS0jxLsthCJPLLfeXOKwvEmvLcXCxIWYZ+dweF9BTSdwlO6sQ+Hv3glTndGeee1dOvzpgjGPeuc8OqovJJV3CJgEz6mutltwidBz3Heon8QoKyKkjMikd+mazLorg7cHPBAq/O3ydcAVi3Eo3fLjmnEU3ZGbO5UvjsDioLe3h03S7m2XDSK6sc8ksR3pbtmYMEOHPAPoateH9NWbV7OyCmTzLhWck5yAdxP6V0JXsjmUrXketaBpiaPotrYoQzKu+Rv70jcsf6fhWp2po5YsO5zSnqK2OG9xR0qeJcnp+FQDrVuEcCkMnAO3ANFO7UVYCUnf8KKM/wAqYFbv+Ap6mmnt9BSigCQdKX0pB1p9ABS9DSd6WgBe1LSDNKOtAHH/ABItlk8Li42KZIbiM7schTxjPpzXmlu204/CvY/Ftob3wnqUCjLeSXX6rz/SvGISCob1GRWNVHRQeht2pG3rjHFaaXBjjyCCx4ANYlrJuRRnHP51p7A52k89q43ud6loU57hpJiAckDBNUXh+1IR5e9AcZq1JcwQswdguD2Paqy6zbxBUijOee+KqKZV7olfTFVYVKAgjA+XkVm32mL9oAdQFQkgDoP8a211+3KLlhGyYG1lzmqs+r20kpYHAJ/iFO7JcHuVrKT7PJg8LjgV0kdyJbbbnlOhzxWDG0MvzEruz29KvndHGHQkADB+lQ0HP0Yl04OT07VhTyYLEfStGd23MS3UcA9qxp3y+B0zWlNGNWRBtaedI1DF2b5FUZZj6AdzXongnw7c2Mj6pqERglZDHbwN95QfvO3oT0A9K5jwNZ/a/GEU38FlA82f9o/Kv8zXqy5xg10pdTiqTfwkw6UHOaQfgKUAUzIelWogcdcVCi8VYTrTQyUDjrQTS5pr9ODVgHfNJnmimqdy59aAIz1/AUoHFBHI+gooAeOtPqMfSnigB3alApB0pRQAvQUopKWgAZQ6lGGVYYYeo714VqmnPpOsXWnuCPIkOzPdDyp/L+Ve7GvOvifbxI+m3arid98TEd1ABGfoaiaui6btI4+zYeaRnoa6BFSWBlJK/Lgkda5WCUq4JNbVtcs8R7kjFcVRanfB6WOb1DRmhuG2TTNFng7smoE0iK6+87q2fvFzn611RKsWGBz7UNYwuNyoQxHQU41DeLS3Oe/4R65WRFW5lcE8Hhs++ap3WkFC+68lZlPPzD+VdA1syyCPewUDBANP/suPYM7hu5+ar5i3OLRykKXyPthnDZOPmXkV29ujx6RH9okEkhHJxiqH2aOA5VMN1qeS5KwYyAB0yKzlK5zytuUL1zHxyC1ZMh+Vj0q3ezGWY85qPTdOOs6xaaZ5hjS4k2SOOoUDJx74FbU46HLUkdz8O9PNtoc2pSIRJfyZTPXyk4H5nJrs1GfYelRxxRwwpDCgSGNQkaD+FRwBVgDArc473dwqVQMdKjXk1Og4pDJUGBUyCowABUqjFUkMd9aTA79aU0lUAh5/CmJkADvjpT2HBGccdfSkAH4gYzQAw9fwFFIf6Cl7UAKvNPFM5p1Ah460tIPpS0AANOptLQMdXnnxQb95pCe8px+Ar0L2ryj4hXy32q2oRspEGVB+I5/E1M/hLpfGjk2XymUk/K3Q1ftJGOADznFRSQiWDbjnHrVSCVoZNjnBHANcrV0dl+Vm3lWO/jjv61chZnJQLnPJrMglEmACB6+9WracRyEs3y4rLlZtGSLMttiUHafmII470yaYhQo7Z49amknZuBktxnHYVSu5U+6vHqfenYptFaUszgjoKqXlxiPbnLEU95sQk5y3esq4nG48+wFXGJzzkMYksFX7xrc8HoB4w0xf7pkP47DWTBAVG5uXPX2FavhqeOy8S295OcQW6sZCOqqw27voM8+1axephNe62esKP15qTvxTACDxgjHUGngetanKKOv86sx81VHXnNWITxQMsrjFSZwOKjUcU/H6VSAX2zSfjRR/SmAlFBpO/wCFAxp6/gKU0Hr+Ao/WgQd6eOlNFOFAx4paaDSg0CA8ClpM0ZoEZ2t3v2SwZVP72b5F9h3P5V5R4lY/26lu3SOBMfjmu4v521PU3cE+Up2IPYf41yfjGAJq9lcAfLPblSR6qf8A69TUXum1H40Zsa/KvHaqNzbqzkY+hrTt1AAJqC+hIkUjsc1ypnbJXMwGW3GWVnjHBI6irEd/GVyGBAHBqa3k2MN4IA6irf8AZ1hdr5zwgMT1Q7T+lNmdpLYgh1NFXIkAJ6+tV7jUE7MC3TcfSrcnhm1J+Se4HsearS6HaQbi8sjgdmPQUaA5SMia7LkpHlj7VNYWhkl8yTkjotSPFFGNsSAKKtWcZKccAn86bemhKXckmjCLgDgjms6ykdPFekonMczvDKv95Stat3x8p9Mcdqq6Ham48ZaS3O2382d/YBP8TTp7iqfCemeGpy+l/ZHJaaxc27EnqnVD/wB88fhWz2rldJnFtry7jiO+TyW9pF+ZPz5FdTW5yMXGOhyDU0WO4qHj86mjPvQkJlxelO7VDu+lPDVYrj6QnB60A5FNNIoUmkoyM03PpTGKev4Ckzig9fwFNPrSEPzT8iodwAzQzgKWJAHcnoKAJt1BeqH20SHbApk/2jwv/wBelkaOGPzbt9/og4B9gKdhE8t5Gi7uW5wMevp9azdWu5orHDsEeY7VjXqo7kmpbNzPKb24ZI8DbBGxwFHt7+9VdTj87UVU8rHGOAe55qkgKdhb4CZ4zWd4v09pfD8V1GFLWNzucsf+WZGGx79DXRRKFwfQVV1O1bUPDGowCURGcEB8E4+oHPtx60TV42KjKzucLBGAAByOxp88CvGQ3TqDVLRLl5LGISACRPkYZzhhx+VbbRb05rztmeje6OcaMoxRx+NPhkMbjOSAcA/1q7NEvmbWHA6Y7VH9kZlxkE9QQasS3LpvR5JJlyvUVjXFwZnIBJHrU5sn3AYYAnv3pkluUUluABSEzOdSSBj8BWlbxGOE7hliOe1R20HmS5wQAep61ozII0Ixlcce1FxIy7jpsA6VueErHFtquqEc7Ps0LEe+Xx+grmL65EeQDyc4rsPChf8A4QASMjIXlbaWP3xnqPb/AArSkru5lWelibajgBlBUEHB9RXRp9ugi8wZuohyyMQJVHsejfjXNq3Ndfpl1H/ZnmyuN0S7SAOTjpx3rptc5ZDbe6hukLQsDj7ykYZPYjtVuNsD+VYt7JaXMyzxpJbTg/LKh5GexHp7U1NWlhk8q7j3N0EkQxu+q/4VNrMR0O6nA9xWfBdxTJuilDjvjqPqOoqwsvFNMRbD4AxSl6gDg+lGSO9MZNnv60CoEk3Dee/8qlVv5UwFJ5H0FQzTxwRlpHVR23HrWXcarPPxap5MZA/eyj5j9B2qIp5H74RyTSt0dzyfzpWKLwvJZ8i2j2qOsso/ktJHYG4kLXMsk2OzcAfhWS+q3MGEFoigHqxJJNSRa3dyDaNiZ67RjH+NOwjeEIj5A+UdKpX+UTLD5jwc/oBUI1CWRB++Jx2J5qBN894hcYC8qvt6/WmgsTT2gNvtYktt6n1otA76ak2DubKkn2q467j6VNHCY9LVTtGCeB9aoCgM+QSelQ6rdNYeELq5EgRgjANtJ5PHAHc5wPfFTTn90FHc8VR8UGRPB/7qYQvuRhIxAAwff8qJbAed2bPZaiba4j8t5oll2Eg7TjGDjvxmuhgfIx1rlr6CUT2F+J7aaGeMhZISdzkEjJz27D6Vt2sxdVAJFcFSNpHZSleJauI9zZPXHNRJtXO4ZqzKQAGAPSouCd2AM9h0qLmouUIBGeuaqTRq3TJ9/WreVycdh1xTdpJHy5+lFxNkNvAFHYY6cVBqMuxOT27VfdSiZx9KwL92lfA6ZoQmY1wTIW469zXpHh+Lb4DWHzHk8uRh84xt77V/2R2/GuBe3P3Qu5mO3aK9E8MCefwvdmeVmZpmaNGGBGoAAQY7DGfxroorc5qzKKE5FbED7YpSP7oFYy96vbmRIiCCjsFPsa1MmXr9R9nRgcHgP/jT4BFdv5M+AJRs3Y+5IBwfxqLVSVt0HqKfb7WIV+BNGGB9GFMFsVpImt7jy59yTJ0cd/oRzVmK9uowGO24X24b8+hrQuI1u7cJJ/rV5V/esdUVXIYlBnkg4wfWiwGtbajBOdqPh+8bjDfl/hVxZsZ5rnGBkDJMqyMhAJ74PQg0+O5ngbZHL5oH/LKY4b/gLf40rWFY6JW+Uc9qmU5/AVi2+oRTNs+ZJMf6pxhh/j+FX1mBB5zRcVjNtoGlkEsrFgMEZPB/CtVfmAzjNUhKOBgAADAqeFxnjjNWUJNAr5BFZE1uY2JUHOeK33UlSarTW4lTjrTaAzIQpOZFDH9Kv28e3LkElu57Cs6QPA5BBxVyC5EgGetJAXweD1FXWKLpi/MWZhnNUImGeSQferV7PGlhHErrvbjiqEzLzubJ4HOM1jeP7i2uPBctpl0IaMbCm5mBOAye4Izntg1tpA00gPIjX7vHLH19h71Zl0W1vtNmtbhQVdeinBXByCD1znmhiPLNUmheztZCscKW8QjwpIRzuADI33Wz3XqDUkO+BiGHTtV7xp4d1L+xIhC5nsYbpZ3CxjzFXuFHucHis4TLcwRyKQQwDAiuOurNM6KBuxATw8AdO/UVG8JQD39R1punyf6O+W6c89qto24EhwAMDpn61znUZ7O0cgHkuYNu5pQeFPpjqa04rdQOcnPfFTxJnbuIyW6denWpeMsG7Ak4NBJj6ifLjK7qyI7YuGYjLEd6u3R+0XL7eF7H1qWJPMeOGGJpJpPlRF5JP+e9MbRz2oKITCjoxSSZY8jPBPfjntVjwdr+oTtd6VbQvaxwTZe8LEmYEnaCDxwOw696seLobXRhDFqE2GABmMeG2FuF2g/eYAFh7ioNI1dbrU9ifPA7pFFcRo0cUrqOoQ8hyv3uw7da7qUGoanFVkpS0OsuY1wtyihQx2yKOgPY/Q/zqazQzK0OfmHzL+FSxoWl2Iu7I2kEcMPQ1K+lXFsyzQKzxjsOWT/GhEkWqy74UP0P0qwkZ/sxXUfNE+4f1qhqaSqQ+1vLf1XGDWrZMFt9p+64zzVAXFPmRq444yKzrlT5xcY54I7Grcb7E2jp29qqt80+B0JwaAIjGYpEkC/Lgqyn09KiuYVIyR06GtIoCuD0xVOcbQV6jsaAM2YMi7SPNi7Buq/Q9RVi01RoflkLSwjqSP3kf1/vL+tNbBXI6HqKozIyNvUkMOQRSaA6V1IOT0p0LEMM+nSrckWR0HSqrIV6DFUBpRHcMVG6GM57ZqGCQrwWz61d4dcHtTQFdoo51wQOag+xiFtw/lVlkMZyv5VJG4YYPWgRRL4bPTHc1pQpZyhJCoLqOM1DJbCQccVUZJbduhxQDNtlQj5RgVFgq2RVa2vQww5x2zV4YbpTEV5kk8stG4GTzn+D6VweveFriK4+06fCokkk3Tw7sK2f409ySSf5V6NtPrgVBNbLNCYd20j7jent9KmUVJWY4txd0eVwK0FxIlwhR4zskiP/ACzPfNS2kxIZy21QeA3b39q7PU/DkWq2zMx8q6iGBIOoI6Bv7yencV57dtJaXckVxA0U8OA6qeD249RXDUpuD8jrp1FJG4btZdrFDkenUL/nmmvehraRmJyRtIHUf/rrKs5JQp8w78Nwo6cHqaluEeUsi4G7hyT6/wBfaoNUU7ma43II1Zt8gRI0TJYnsB3Nek+FvDZ0q0WW6w19Ko81uuwf3AfT19az/B+kqiNq9yA8jZS3Yjoo4L49T0/CuzVvkz6DNdVKnbVnLWq3fKjyP4li9TxVbfYoonElofkdFYsFb59inncqEnjnmofDVrBrP9l3FhN/oNiJHVEU+WxPyoSGyyvySQfTNdR470e51bSY7qxS3F7FcBoppmKmE5A3KRzkenQjIqbw9olroGmpY2i/IrGSSQgAyyH7znHr29Bit27Kxh1L9tAFcggcHk1blnEMTHNR5wxqleuWIQd6lKwzNuyZmUFm25yAScZrTtk/cBc8gcCqUigOg960V+WMEelMZEwKEoe4yKZEuZNx+tEzFvLbupwfcVKowM+tADZWxwOKqSAsCTyKtMNxyetRSYB9zTArxQElu4NVbqErG3qozWxBFtXkcdqraioW2lY/wqSaAP/Z");
        }

        public Task<bool> SendEmail(string[] scopes, string subject, List<string> tos, List<string> ccs, string body, List<Attachment> files)
        {
            var message = new Message
            {
                Subject = subject,
                Body = new ItemBody { ContentType = BodyType.Html, Content = body },
            };
            List<Recipient> _tos = new List<Recipient>();
            foreach (var to in tos)
            {
                _tos.Add(new Recipient { EmailAddress = new EmailAddress { Address = to } });
            }
            message.ToRecipients = _tos;

            List<Recipient> _ccs = new List<Recipient>();
            foreach (var cc in ccs)
            {
                _ccs.Add(new Recipient { EmailAddress = new EmailAddress { Address = cc } });
            }
            message.CcRecipients = _ccs;
            message.Attachments = new MessageAttachmentsCollectionPage();
            foreach (var f in files)
            {
                try
                {
                    message.Attachments.Add(f);
                }
                catch (Exception ex)
                {
                    var xx = 1;
                }
            }
            return Task.FromResult(true);
        }
    }

    public class GraphAuthService : IGraphAuthService
    {
        readonly ITokenAcquisition tokenAcquisition;
        readonly WebOptions webOptions;
        readonly ICommandService<AddLog> logSrv;
        private readonly IGraphConverterService converter;
        public GraphAuthService(ITokenAcquisition tokenAcquisition, IGraphConverterService converter,
                              IOptions<WebOptions> webOptionValue, ICommandService<AddLog> logSrv)
        {
            this.tokenAcquisition = tokenAcquisition;
            this.webOptions = webOptionValue.Value;
            this.converter = converter;
            this.logSrv = logSrv;
        }
        public Task<Dictionary<string, string>> ConvertUser(User u)
        {
            return Task.FromResult(converter.Convert(u));
        }


        private GraphServiceClient GetGraphServiceClient(string[] scopes)
        {
            return GraphServiceClientFactory.GetAuthenticatedGraphClient(async () =>
            {
                string result = await tokenAcquisition.GetAccessTokenForUserAsync(scopes);
                return result;
            }, webOptions.GraphApiUrl);
        }

        public async Task<User> GetGraphUser(string[] scopes)
        {
            GraphServiceClient graphClient = GetGraphServiceClient(scopes);
            return await graphClient.Me.Request().GetAsync();
        }

        public async Task<User> CheckUser(string[] scopes, string email)
        {
            GraphServiceClient graphClient = GetGraphServiceClient(scopes);
            return await graphClient.Users[email].Request().GetAsync();
        }

        public async Task<string> GetUserPhoto(string[] scopes)
        {
            try
            {
                GraphServiceClient graphClient = GetGraphServiceClient(scopes);
                using (var photoStream = await graphClient.Me.Photo.Content.Request().GetAsync())
                {
                    byte[] photoByte = ((MemoryStream)photoStream).ToArray();
                    return Convert.ToBase64String(photoByte);
                }
            }
            catch {
                return "";
            }
        }

        public async Task<bool> SendEmail(string[] scopes, string subject, List<string> tos, List<string> ccs, string body, List<Attachment> files)
        {
            try
            {
                GraphServiceClient graphClient = GetGraphServiceClient(scopes);

                var message = new Message
                {
                    Subject = subject,
                    Body = new ItemBody { ContentType = BodyType.Html, Content = body },
                };
                List<Recipient> _tos = new List<Recipient>();
                foreach (var to in tos)
                {
                    _tos.Add(new Recipient { EmailAddress = new EmailAddress { Address = to } });
                }
                message.ToRecipients = _tos;

                List<Recipient> _ccs = new List<Recipient>();
                foreach (var cc in ccs)
                {
                    _ccs.Add(new Recipient { EmailAddress = new EmailAddress { Address = cc } });
                }
                message.CcRecipients = _ccs;

                message.Attachments = new MessageAttachmentsCollectionPage();
                foreach (var f in files)
                {
                    message.Attachments.Add(f);
                }

                await graphClient.Me
                    .SendMail(message, true)
                    .Request()
                    .PostAsync();
                return true;
            }
            catch (Exception ex)
            {
                await logSrv.Execute(new AddLog
                {
                    OID = "",
                    PageName = "Send Mail",
                    Props = JsonConvert.SerializeObject(ex)
                });
                return false;
            }

        }
    }
}
