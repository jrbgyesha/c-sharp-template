﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Security.Claims;

namespace FrontEndCore.GraphServices
{
    public class MockAuthHandler : AuthenticationHandler<MockAuthOptions>
    {
        public MockAuthHandler(IOptionsMonitor<MockAuthOptions> options,
            ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var authenticationTicket = new AuthenticationTicket(
                new ClaimsPrincipal(Options.Identity),
                new AuthenticationProperties(),
                "Test Scheme");

            return Task.FromResult(AuthenticateResult.Success(authenticationTicket));
        }
    }
    public static class MockAuthExtensions
    {
        public static AuthenticationBuilder AddMockAuth(this AuthenticationBuilder builder, Action<MockAuthOptions> configureOptions)
        {
            return builder.AddScheme<MockAuthOptions, MockAuthHandler>("Test Scheme", "Test Auth", configureOptions);
        }
    }
    public class MockAuthOptions : AuthenticationSchemeOptions
    {
        public virtual ClaimsIdentity Identity { get; } = new ClaimsIdentity(new Claim[]
            {
                new Claim("http://schemas.microsoft.com/identity/claims/objectidentifier", "23305374-74c2-4cd4-b8b5-5999902f8e09"),
                new Claim("preferred_username","tfurrer@deloitte.com"),
                new Claim("name","Furrer, Tyler Jay")
            }, "test");
    }
}
