﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
        end = new Date().getTime();
    }
}

function NavHome() {
    window.location.replace(window.location.protocol + '//' + window.location.host + "/Home");
}
function NavAdmin() {
    window.location.replace(window.location.protocol + '//' + window.location.host + "/Admin");
}

const validfileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'msg', 'pptx', 'ppt', 'xlsx', 'xlsm', 'xls', 'xlsb', 'docx', 'docm', 'doc', 'pdf', 'txt', 'csv'];
function ValidatedFileExtension(fileName) {
    if ($.inArray(fileName.split('.').pop().toLowerCase(), validfileExtension) == -1) {
        return false;
    }
    return true;
}

function jqueryHasAttribute(element, attribute) {
    var attr = $(element).attr(attribute);
    return typeof attr !== typeof undefined && attr !== false;
}