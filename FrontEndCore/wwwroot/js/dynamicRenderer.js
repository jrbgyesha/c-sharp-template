function InitializeDynamicRenderer() {
  //Click the partner tab onchange/click for top nav bar
  $('.nav-tabs-top a.nav-link-dttl-form').on('click', function () {
    var isDisabled = $(this).hasClass("disabled");
    if (!isDisabled) {
      $('.nav-tabs-bottom a.nav-link-dttl-form.active').removeClass('active');
      $('.nav-tabs-bottom a.nav-link-dttl-form[href="' + $(this).attr('href') + '"]').addClass('active');
    }
  })

  //Click the partner tab onchange/click for bottom nav bar
  $('.nav-tabs-bottom a.nav-link-dttl-form').on('click', function () {
    var isDisabled = $(this).hasClass("disabled");
    if (!isDisabled) {
      $('.nav-tabs-top a.nav-link-dttl-form.active').removeClass('active')
      $('.nav-tabs-top a.nav-link-dttl-form[href="' + $(this).attr('href') + '"]').addClass('active');
    }
  })

  //Enable bootstrap tooltip
  $('[data-toggle="tooltip"]').tooltip()

  //Enable back to top button ----BUGGGY on THIS Tempalte
  // $(window).scroll(function () {
  //     if ($(this).scrollTop() > 100) {
  //         $('#backToTop').show("slide", { direction: "right" });
  //     } else {
  //         $('#backToTop').hide("slide", { direction: "right" });
  //     }
  // });
  // $('#backToTop').click(function () {
  //     $("html, body").animate({ scrollTop: 0 }, 600);
  //     return false;
  // });


//   Enable back to top button //FIXED
$(window).scroll(function () {
  if ($(this).scrollTop() > 100) {
      $('#backToTop').slideDown();
  } else {
      $('#backToTop').slideUp();
  }
});

$('#backToTop').click(function () {
  $("html, body").animate({ scrollTop: 0 }, 600);
  return false;
});

  //Enable toolbar hide/show toggle
  $('.dynamicToolbarSwitch').change(function () {
    $('.dynamicToolbar').collapse('toggle')
  })

  showClearButton();
  additionalClassForSelectpricker();
}


function initializeDatepickers() {
  var element = $('[data-toggle="datepicker"]');
  element.each(function () {
    var dateProp = JSON.parse($(this).attr('data-dateProp'));

    var dateFormat = $(this).attr('data-dateFormat')

    // Merged Object using object.Assign, spread syntax (...) not supported by Edge
    var obj1 = dateProp;
    var obj2 = { onSelect: function (dateText,) { $(this).val(dateText); } };
    var obj = Object.assign(obj1, obj2);
    $(this).datepicker(obj);

    $(this).change(function () {
      if (this.value) {
        if (moment(this.value).format(dateFormat) === "Invalid date") {
          //When it can't reformat because it's already in the correct format 
          this.value = moment(this.value, dateFormat).format(dateFormat);
          $(this).attr('value', moment(this.value));
        }
        else {
          this.value = moment(this.value).format(dateFormat);
          $(this).attr('value', moment(this.value).format(dateFormat));
        }
      }
      else {
        $(this).attr('value', this.value);
      }
    });
  })
}

function initializeDataTables() {
  $('.divDataTable').each(function () {
    var divDataTable = $(this);

    var dataTableProp = JSON.parse(divDataTable.attr('data-table-prop'));
    var table = $(this).find('table');

    var url = dataTableProp.url;
    var data = dataTableProp.data;
    var columns = dataTableProp.columns;
    var order = dataTableProp.order;
    var timeSinceIndexes = dataTableProp.timeSinceIndexes;
    var customDom = dataTableProp.customDom;
    var buttons = dataTableProp.buttons;
    var _columns = [];
    var _columnDefs = [];

    console.log(dataTableProp);

    if (dataTableProp) {
      // if(url && url.includes('#formId')){
      //     var formId = $('#formId');
      //     if(formId.length && $(formId).val()){
      //       var formIdRegEx = new RegExp('#formId', 'g')
      //       url = url.replace(formIdRegEx, $(formId).val());
      //     }else{
      //       return;
      //     }
      // }
      console.log(url);
      var dataTable = new TableBuilder(table);
      dataTable.customDom = customDom;

      dataTable.Destroy();
      console.log(url);
      console.log(url, _columns, "", _columnDefs, order, true, buttons);
      if (url) {
        columns.forEach((column, index) => {
          if (column.columnDefs) {

            var defaultcontent =
              `<button type="submit" method="${column.columnDefs.method}" 
              formaction="${column.columnDefs.api}" 
              class="btn btn-outline-secondary ${column.columnDefs.class ? column.columnDefs.class : ""}" 
              ${column.columnDefs.onLoadMessage ? `onload-message="${column.columnDefs.onLoadMessage}"` : ""}
              ${column.columnDefs.onSuccessfulMessage ? `onsuccess-message="${column.columnDefs.onSuccessfulMessage}"` : ""}
              ${column.columnDefs.onFailedMessage ? `onfail-message="${column.columnDefs.onFailedMessage}"` : ""}
              onclick="summaryActiveButtonClick(this)">
                <i class="${column.columnDefs.icon}" aria-hidden="true"></i>
              </button>`

            _columnDefs.push(
              {
                title: column.title,
                targets: index,
                data: column.id,
                // eslint-disable-next-line no-unused-vars
                render: function (data, type, row, meta) {
                  // console.log(data);
                  if ((column.enabledReference && data === column.enabledReference) || column.enabledReference === undefined) {
                    return defaultcontent;
                  } else {
                    return "";
                  }
                }
              }
            );
          }
          else {
            if (timeSinceIndexes.includes(index)) {
              _columns.push({
                data: column.id,
                type: "datetime",
                title: column.title,
                // eslint-disable-next-line no-unused-vars
                render: function (data, type, row, meta) {
                  return timeSince(data);
                }
              });
            } else {
              _columns.push({ data: column.id, title: column.title });
            }
          }
        });
        dataTable.AjaxTable(url, _columns, "", _columnDefs, order, true, buttons)
      }

      if (data) {
        // eslint-disable-next-line no-unused-vars
        columns.forEach((column, index) => {
          _columns.push({ data: column.id, title: column.title });
        });
        dataTable.BasicTable(data, _columns, order);
      }
    }
  });
}

function ToggleBottomTab() {
  $('.bottomNavContainer').collapse('toggle');
}

async function InitializeBootstrapSelect2(selectElement) {
  return new Promise((resolve, reject) => {
    // var t0 = performance.now();
    var isParamSelectElement = selectElement ? true : false;
    selectElement = selectElement ? selectElement : $('select.dropdown');
    if (selectElement.length === 0) resolve();
    selectElement.selectpicker({ title: 'Select..' });
    var cacheService = new cachePromiseService($.ajax)

    if (!isParamSelectElement) {
      // var startGettingUniqueValues = performance.now();
      var dataSelectArray = [];

      var parentInputGroup = null;
      var selectLoader = null;
      var selectLoaderError = null;
      var inputGroupPrepend = null;
      var inputGroupAppend = null;
      var apiSelectCounter = 0;

      $(selectElement).each(function () {
        var parentElementContainer = $(this).closest('.elementColContainer');
        parentInputGroup = parentElementContainer.find('.input-group');
        selectLoader = parentElementContainer.find('.selectLoader');
        selectLoaderError = parentElementContainer.find('.selectLoader-error');
        inputGroupPrepend = parentElementContainer.find('div.input-group-prepend');
        inputGroupAppend = parentElementContainer.find('div.input-group-append');

        selectLoader.show();
        selectLoaderError.hide();
        inputGroupPrepend.hide();
        inputGroupAppend.hide();

        var dataSourceType = $(this).attr('data-source-type');
        var dataSource = $(this).attr('data-source');
        var dataDataValue = $(this).attr('data-data-value');
        var dataSourceDataKey = $(this).attr('data-source-data-key');
        var dataSourceDataValue = $(this).attr('data-source-data-value');
        if (dataSource) {
          if (dataSelectArray.find(_ =>
            _.dataSource === dataSource &&
            _.dataDataValue === dataDataValue &&
            _.dataSourceDataKey === dataSourceDataKey &&
            _.dataSourceDataValue === dataSourceDataValue
          ) === undefined) {
            if (dataSourceType === 'api') {
              apiSelectCounter++;
            }

            dataSelectArray.push({ dataSourceType, dataSource, dataDataValue, dataSourceDataKey, dataSourceDataValue });
          }
        } else {
          $(this).selectpicker();
          var isDisabled = jqueryHasAttribute(this, 'disabled');
          if (isDisabled) {
            $(this).removeAttr('disabled');
          }

          parentInputGroup.show();
          selectLoader.hide();
          selectLoaderError.hide();
          inputGroupPrepend.hide();
          inputGroupAppend.hide();
        }
      })

      //console.log(`getting unique values took ${performance.now() - startGettingUniqueValues}ms`);

      // var startAddingOptionsTotal = performance.now()
      var currentApiSelectCounter = 0;
      var isApiExists = dataSelectArray.find(_ => _.dataSourceType === 'api') !== undefined;
      // eslint-disable-next-line no-unused-vars
      dataSelectArray.forEach((_, index, array) => {
        var { dataSourceType, dataSource, dataDataValue, dataSourceDataKey, dataSourceDataValue } = _;

        if (dataSourceType === 'api') {
          dataDataValue = dataDataValue ? JSON.parse(dataDataValue) : [];
          // var startAddingOptions = performance.now();
          var exceptList = ['external']
          cacheService.GetServerPromise(dataSource, dataSource, window.sessionStorage, false, exceptList).then((results) => {
            var options = document.createDocumentFragment();
            results.forEach((data) => {
              var option = document.createElement('option');
              option.text = data[dataSourceDataValue];
              option.value = data[dataSourceDataKey];
              dataDataValue.forEach((_dataValue) => {
                option.setAttribute(`data-${_dataValue.key}`, data[_dataValue.value]);
              });
              options.append(option);
            });
            var dataValueSearch = _.dataDataValue ? `[data-data-value="${_.dataDataValue.replace(/"/gi, "\\\"")}"]` : "";
            var selectElements = $(`[data-source-type="${dataSourceType}"][data-source="${dataSource}"]${dataValueSearch}[data-source-data-key="${dataSourceDataKey}"][data-source-data-value="${dataSourceDataValue}"]`);
            $(selectElements).append(options);
            //console.log(`Adding options for ${dataSource} took ${performance.now() - startAddingOptions}ms and total is ${performance.now() - startAddingOptionsTotal}ms`);
            return selectElements;
          }).then((_selectElements) => {
            // var startSelectpicker = performance.now()
            var dataPlaceholder = $(_selectElements).attr('data-placeholder');
            var newValue = $(_selectElements).attr('defaultValue');
            // console.log("newvlaue================1111===============",newValue);

            var isDisabled = jqueryHasAttribute(_selectElements, 'disabled');

            if (dataPlaceholder) {
              $(_selectElements).selectpicker({ title: dataPlaceholder }).selectpicker('render');
            } else if (newValue) {
              $(_selectElements).selectpicker('val', newValue);
            } else {
              $(_selectElements).selectpicker({ title: 'Select..' }).selectpicker('render');
            }
            var initialValue = $(_selectElements).attr('initial-value');

            if (initialValue !== undefined && !onSubmit) {
              $(_selectElements).val(initialValue);
              //$(_selectElements).removeAttr('initial-value');
              // $(`#initial-value-holder-${$(this).attr("name")}`).remove();
            }

            $(_selectElements).selectpicker('refresh');

            if (isDisabled) {
              $(_selectElements).removeAttr('disabled');
            }

            var parentElementContainer = $(_selectElements).closest('.elementColContainer');
            parentInputGroup = parentElementContainer.find('.input-group');
            selectLoader = parentElementContainer.find('.selectLoader');
            selectLoaderError = parentElementContainer.find('.selectLoader-error');
            inputGroupPrepend = parentElementContainer.find('div.input-group-prepend');
            inputGroupAppend = parentElementContainer.find('div.input-group-append');

            selectLoader.hide();
            selectLoaderError.hide();
            parentInputGroup.show();
            inputGroupPrepend.show();
            inputGroupAppend.show();

            //console.log(`Adding selectpicker took ${performance.now() - startSelectpicker}`);
            // })
          }).then(() => {
            currentApiSelectCounter++;
            if (currentApiSelectCounter === apiSelectCounter) {
              // var startReValidation = performance.now()
              //validateForm();
              resolve();
              // console.log(`Bootstrap select with api took ${performance.now() - t0}ms to initialize `);
            }
          }).catch(error => {
            var dataValueSearch = _.dataDataValue ? `[data-data-value="${_.dataDataValue.replace(/"/gi, "\\\"")}"]` : "";
            var _selectElements = $(`[data-source-type="${dataSourceType}"][data-source="${dataSource}"]${dataValueSearch}[data-source-data-key="${dataSourceDataKey}"][data-source-data-value="${dataSourceDataValue}"]`);
            var parentElementContainer = $(_selectElements).closest('.elementColContainer');
            parentInputGroup = parentElementContainer.find('.input-group');
            selectLoader = parentElementContainer.find('.selectLoader');
            selectLoaderError = parentElementContainer.find('.selectLoader-error');
            inputGroupPrepend = parentElementContainer.find('div.input-group-prepend');
            inputGroupAppend = parentElementContainer.find('div.input-group-append');

            // selectLoaderError.find('.selectLoader-error-Messsage').html("This dropdown encountered an error while loading.");
            selectLoaderError.find('.selectLoader-error-Messsage').html("This dropdown requires VPN connection.");

            selectLoader.hide();
            selectLoaderError.show();
            parentInputGroup.hide();
            currentApiSelectCounter++;
            if (currentApiSelectCounter === apiSelectCounter) {
              // console.log("HERE");
              additionalClassForSelectpricker();
              reject(error.message ? error.message : "Error in retrieving api dropdown");
            }
          })
        } else if (dataSourceType === 'js') {
          try {
            var jsData = eval(dataSource);
            var options = document.createDocumentFragment();
            jsData.forEach((data) => {
              var option = document.createElement('option');
              option.text = data[dataSourceDataValue];
              option.value = data[dataSourceDataKey];
              options.append(option);
            })

            var dataSourceSearch = dataSource ? `[data-source="${dataSource.replace(/"/gi, "\\\"")}"]` : "";
            var selectElements = $(`[data-source-type="${dataSourceType}"]${dataSourceSearch}[data-source-data-key="${dataSourceDataKey}"][data-source-data-value="${dataSourceDataValue}"]`);
            $(selectElements).append(options);

            var dataPlaceholder = $(selectElements).attr('data-placeholder');
            var newValue = $(selectElements).attr('defaultValue');
            var isDisabled = jqueryHasAttribute(selectElements, 'disabled');

            if (dataPlaceholder) {
              $(selectElements).selectpicker({ title: dataPlaceholder }).selectpicker('render');
            } else if (newValue) {
              $(selectElements).selectpicker('val', newValue);
            } else {
              $(selectElements).selectpicker({ title: 'Select..' }).selectpicker('render');
            }

            if (isDisabled) {
              $(selectElements).removeAttr('disabled');
            }


            var parentElementContainer = $(selectElements).closest('.elementColContainer');
            parentInputGroup = parentElementContainer.find('.input-group');
            selectLoader = parentElementContainer.find('.selectLoader');
            selectLoaderError = parentElementContainer.find('.selectLoader-error');
            inputGroupPrepend = parentElementContainer.find('div.input-group-prepend');
            inputGroupAppend = parentElementContainer.find('div.input-group-append');

            selectLoader.hide();
            selectLoaderError.hide();
            parentInputGroup.show();
            inputGroupPrepend.show();
            inputGroupAppend.show();

            //validateForm();

            additionalClassForSelectpricker();
            if (!isApiExists) resolve();

            // console.log(`Bootstrap select with js took ${performance.now() - t0}ms to initialize `);

          } catch (error) {
            reject(error);
            console.log("at JS error", error);
            selectLoader.hide();
            selectLoaderError.show();
            parentInputGroup.hide();
          }
        }
      })
      if (!isApiExists) resolve();
      // eslint-disable-next-line no-empty
    } else {
      resolve();
    }
  });
}

function clearValueByID(id) {
  var inputToChange = $(`#${id}`);
  if(jqueryHasAttribute($(inputToChange), "wrappertype")){
      var elementWrapper = $(inputToChange).closest(".elementWrapper");
      var elementLsit = $(elementWrapper).find(`input[type="${$(inputToChange).attr("wrappertype")}"]`);
      if(elementLsit.length){
          $(elementLsit).each(function(){
              onRadioClear = true;
              $(inputToChange).val('');
              $(this).removeAttr('checked');
              $(this).prop('checked', false);
              $(this).trigger('click');
              $(this).prop('checked', false);
              // $(this).trigger('change');
          })
      }
  }else{
      inputToChange.val('').trigger('change');
  }
}

function showClearButton(){
  //Fix not showing the clearable tag
  var clearButton = $('.input-group-append.isClearable');
  if(clearButton.length > 0){
    $(clearButton).show();
  }
}

function additionalClassForSelectpricker() {
  //Aditional class for dropdown
  $('button.dropdown-toggle').addClass('border rounded');
  $('.input-group-append').find('[data-tag="clear"]').addClass('rounded-right');
}