class _cache {
    constructor(_data) {
        this.data = _data;
    }
    PutCache(name, data) {
        const _this = this;
        _this.data.push({ name, data });
    }
    PopCache(name) {
        const _this = this;
        _this.data = _this.data.filter(x => x.name !== name);
    }
    GetCache(name) {
        const _this = this;
        return _this.data.filter(x => x.name === name)[0];
    }
    CheckCache(name) {
        const _this = this;
        return _this.data.length > 0 && _this.data.filter(x => x.name === name).length > 0;
    }
    Data(name) {
        const _this = this;
        return _this.GetCache(name).data;
    }
    GetServerObject(name, path) {
        const _this = this;
        if (_this.CheckCache(name)) {
            return new Promise((resolve, reject) => { resolve(_this.GetCache(name).data); });
        }
        return new Promise((resolve, reject) => {
            $.ajax({
                url: path + '?version=' + (621355968e9 + (new Date()).getTime() * 1e4), type: 'GET'
            }).then(function (result) {
                _this.PutCache(name, result);
                resolve(result);
            });
        });
    }
}