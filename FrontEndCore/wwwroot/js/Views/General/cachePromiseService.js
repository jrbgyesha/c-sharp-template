/* eslint-disable no-empty */

class cachePromiseService{
    constructor(requestHandler, sessionPromise = []){
        this.requestHandler = requestHandler;
        this.sessionPromise = sessionPromise;
    }

    PutCache(name, promise) {
        this.sessionPromise.push({ name, promise });
    }

    PopCache(name) {
        this.sessionPromise = this.sessionPromise.filter(x => x.name !== name);
    }
    GetCachePromise(name) {
        var data = this.sessionPromise.find(x => x.name === name);
        return data === undefined ? null : data.promise;
    }
    CheckCache(name) {
        return this.sessionPromise.length > 0 && this.sessionPromise.filter(x => x.name === name).length > 0;
    }

    CheckSessionStorage(name, _sessionStorage){
        return _sessionStorage.getItem(name) !== null;
    }

    GetSessionStorage(name, _sessionStorage){
        return _sessionStorage.getItem(name);
    }

    RegisterToSessionStorage(name, value, _sessionStorage){
        if(typeof value !== 'string'){
            try{
                value = JSON.stringify(value);
            }catch(err){
            }
        }
        _sessionStorage.setItem(name, value);
    }

    async GetServerPromise(name, path, _sessionStorage = null, iLog = false, exceptList = []) {
        const _this = this;
        var getPromise = false;
        if(exceptList.length > 0){
            for(var except of exceptList){
                if(path.includes(except)){
                    getPromise = true;
                    break;
                }
            }
        }

        if(_sessionStorage !== null && _this.CheckSessionStorage(name, _sessionStorage) && !getPromise){
            if(iLog) console.log(`calling ${name} from SessionStorage.`);
            var result = _this.GetSessionStorage(name, _sessionStorage);
            try{
                return JSON.parse(result);
            }catch(err){
                return result;
            }
            
        } else if (_this.CheckCache(name)) {
            const promise = this.GetCachePromise(name);
            if(iLog) console.log(`calling ${name} from Cache.`);
            return await promise;
        }
        const promise = this.requestHandler({
            type: 'get',
            method: 'get',
            url: path,
        })

        this.PutCache(name, promise);
        if(_sessionStorage !== null){
            promise.then(response => {
                _this.RegisterToSessionStorage(name, response, _sessionStorage);
            })
        }
        if(iLog) console.log(`calling ${name} from Ajax.`);
        return await promise;
    }
}
