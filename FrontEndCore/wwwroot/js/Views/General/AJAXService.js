﻿//Ajax Service
/*
 Requires JQuery: https://jqueryui.com/
 */
class AJAXService {
    constructor(_url = '', _type = '', _data = new FormData, _run = false) {
        this.url = _url;
        this.type = _type;
        this.data = _data;
        this.promise = null;
        if (_run) {
            this.Call();
        }
    }

    Call(_url = this.url, _type = this.type, _data = this.data) {
        const _this = this;
        _this.promise = $.ajax({
            url: _url,
            type: _type,
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            data: _data
        });
    }
    Clean(result) {
        const _this = this;
        return JSON.parse(result);
    }
}
