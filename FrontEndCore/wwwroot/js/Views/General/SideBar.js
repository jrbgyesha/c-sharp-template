﻿//SideBar Links
//links should be an array of: { _order: 1, _id: 'linkSys', _type: 'box', _label: 'System', _class: 'linkGroup', _data: 'sys', _attrs: '', _animate = [''] }
class Sidebar {
    constructor(_container, _links = [], _cache, _run = true) {
        this.container = _container;
        this.links = _links;
        this.cache = _cache;
        String.prototype.interpolate = function (params) {
            const names = Object.keys(params);
            const vals = Object.values(params);
            return new Function(...names, `return \`${this}\`;`)(...vals);
        };
        if (_run) {
            this.Build();
        }
    }
    Build() {
        const _this = this; 
        console.log(_this.links);
        console.log(_this.cache.Data('sidebaritem'));
        $(_this.container).empty().append(_this.links.map(function (x) {
            return _this.cache.Data('sidebaritem').interpolate({ _id: x._id, _class: x._class, _data: x._data, _attrs: x.attrs, _icon: x._type, _label: x._label });
        }).join(''));
    }
    Clear() {
        const _this = this;
        $(_this.container).empty();
    }


}

