﻿//Message Service
/*
 Requires Noty: https://ned.im/noty/#/
 Requires Animate: https://daneden.github.io/animate.css/
 */
class Message {
    constructor(_layout = 'topRight') {
        this.Types = {
            warn: 'warning', error: 'error', success: 'success', info: 'info'
        };
        this.Durations = {
            long: 4500, moderate: 3000, short: 2500, blast: 1500
        };
        this.Delays = {
            long: 4500, moderate: 3000, short: 2500, blast: 1500, none: 0
        };
        this.Layout = _layout;
    }
    Build(_message, _type = 'info', _timeout = 2500, _delay = 0) {
        const _this = this;
        setTimeout(function () {

            const n = new Noty({
                layout: _this.Layout,
                type: _type.toLowerCase(),
                theme: 'mint',
                closeWith: ['click', 'button'],
                timeout: _timeout,
                text: _message,
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight' // Animate.css class names
                }
            }).show();
        }, _delay);
    }
}

const _message = new Message();