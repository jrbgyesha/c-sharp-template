﻿class atrapaValidator {
    constructor(_element,_elementValidation, _disableOnFail, ) {
        this.element = _element;
        this.elementValidation = _elementValidation;
        this.disableOnFail = _disableOnFail;
        this.self = this;

    }

    runValidation(_element = this.element, _elementValidation = this.elementValidation) {
        const self = this;
        $.when(this.check($(_element))).then(function (data) { self.validateAtrapa(data, _elementValidation); }).fail(function () { self.validateAtrapa(false, _elementValidation); });
    }
    check(_element = this.element) {
        return ajaxPromise('http://sgsin0857/UserCenter/api/Users/CheckNetworkUserByEmail?id=' + encodeURIComponent($(_element).val().toLowerCase()),
            'GET', null);
    }
    atrapaForm(_element = this.element, _doneFunction = this.atrapaFormDefault) {
        const self = this;
        $('form').one('submit', function (e) {
            $.when(self.check($(_element))).then(_doneFunction);
        });
    }
    validateAtrapa(_valid = false, _element = this.element, _msg = 'Please Input a Valid Email') {
        if (_valid) {
            $(_element).text('');
            $('#submit').removeAttr('disabled');
        } else {
            $(_element).text(_msg);
            $('#submit').attr('disabled', 'disabled');
        }
        return _valid;
    }
    atrapaFormDefault(data) {
        const self = this;
        if (data) {
            $('form').unbind('submit').submit();
            return true;
        }
        else {
            e.preventDefault();
            self.validateAtrapa(data);
            return false;
        }
    }
}