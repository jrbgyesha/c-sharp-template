﻿
const URLPath = window.location.origin === '[Server Path]' ? window.location.origin + '/[set Path Here]' : window.location.origin;
//const _loadAvatar = new AJAXService(URLPath + '/Home/GetUserImage', 'GET', null, true);
//_loadAvatar.promise.then(function (result) {
//    $('#avatar').attr('src', `data:image/png;base64,${_loadAvatar.Clean(result)}`);
//    $('#avatar').removeClass('hidden');
//});

// Standard Pre-Executions
function preExecute() {
    $('.date-field').each(function () {
        $(this).datetimepicker({ timepicker: false, format: 'd/m/Y' });
    });
    $('[data-toggle="tooltip"]').tooltip();
}


$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return decodeURI(results[1]) || 0;
    }
};
class PageInteractionProvider {
    constructor(_featherProvider, _tooltipProvider) {
        this.FeatherProvider = _featherProvider;
        this.TooltipProvider = _tooltipProvider;
    }
    ResetInteractions() {
        const _this = this;
        _this.FeatherProvider.ResetFeathers();
        _this.TooltipProvider.ResetTooltips();
    }
}
class FeatherProvider {
    constructor() { }
    ResetFeathers() {
        feather.replace();
    }
}
class TooltipProvider {
    constructor() { }
    ResetTooltips() {
        $('.tooltip').tooltip('hide');
        $('[data-toggle="tooltip"]').tooltip({
            trigger: "hover"
        });
    }
}

class EvidenceProvider {
    constructor(_element, _label) {
        this.element = _element;
        this.label = _label;
        this.LoadEvent();
    }
    LoadEvent() {
        const _this = this;
        $(_this.element).on('change', function () {
            try {
                const filename = $(_this.element)[0].files[0].name;
                $(_this.label).text(filename);
            }
            catch (error) {
                $(_this.label).val('Choose file...');
            }
        });
    }
}
const fileImageTypes = [
    { 'type': 'pdf', 'data': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAD00lEQVRoge2Y3U9TdxjHP6cUqn2BWcCBSlYor43RGDPDOkhj00SIJqJ/gcYrjCFKTLyYFy5LNDGZwgXXeqVeEQnZhQjNJGQYvTARusZIB7U0CKWyUWnLS8/ZxSJbltFCT39ysvC5/T3n+T6fnLffObCDtpDSLT73eJoUWf5WSLAkufLgp6+Hhnpy0U+fblEBT57J9J3Jbl/NRdg/WRwb270mSa2jbnfJN17v92r7pRUBMNpsa467d3erDfo3zz0eiurrWXzz5sao241aGV2uBsuG/S0t7G9pAZ3uxqjb/YOaXtsqAnDg1CkOtLaCTnddjcy2i0BuZDQhAuplNCMC6mQ0JQLZy2hOBLKT0aQIbF0m4wtRJEvBIEoqteG6pbqawtpaYm/fXv/F44k6Bwe7NqrdVpFgb++mayVJagO0J9I4OLjpWl9HRzLm9/+crkaz98hWESqyMDpK9NkzFFkWGQMIvLQ+jIwwcesWkl5PKh5nb2urqChA4Bn5/eVL9jidWJuaiI2Pi4pZR+ilJUkSiiwjSWk/RHOCMBGjzcZSIEDy3TsM5eWiYtYRJmKqrSUZCrEUCGB2OETFrCNOpKaGPJMJZBlzXZ2omHWEiegKCig6ehTDvn3kGY2iYv7OE9ZZUYj5fCTDYZLT08JiPiFMJDY+zmo0iqm6mtC9e6Ji1hEmEhkYoPDwYSovXyY6PMxHv19UFCBIZHVhgfmhIb5sa8NcV0exy8VUT0/aLbtahIi87+ujoLQUq9MJwFft7SRnZpi+f19EHCBgryUnk8z291Nx7hxIEql4nJW5OfY0NhJ+9IjluTmUtbW/iiWJgpISio4c4Ytjx0DFDiDnIuGHD0nFYixNTDDW3k48EECRZfKtViwNDcx7vejNZqzNzQAkgkFm+/spOX6cqqtXs87NmUjM5yPy5AnzT5+iMxhYiUSwNjdju3gRU00Nul27AJj3epns6iIRDFJ25gxGu50Pw8PM9PZSpSJftUgiFGKyu5vY69coioLF4aDh9m10BsN/1pe43VgcDsIPHvDbnTuk4nF0+flUnD+vag5VIh/9fvzXrmE5eJCys2eZ7eujsqNjQ4lPGMrKqOrspKqzk5VoFL3ZnPGYTKh6ak12d1PsclF64gTvHz/GdukSRrt9Sz0KiotVS4DKMyIvL/PHq1dEBgaouHCBvSdPqh4oW1SJ1N+8ycKLFxQeOoSxsjJXM2WFKhFDeTllp0/nahZV7PwO0ho7IlpjR0Rr/G9EMr5H4lNT+l+vXEl8jmHSzJCfqSaTyEgqkfhx8TP8u90EI9s9wA5b4U8DET1L0bec9gAAAABJRU5ErkJggg==' },
    { 'type': 'docx', 'data': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAACoUlEQVRoge2YTU8TURSGn2krsYpWJODwZazBUlNCMJpCIhCiG40bXBn/gStimuDGjTG6YEFi4sqf4M7EaOOe0gUhRgmdCFETsBTEmlAhhdIyLlpqP4Z20s5MAedZ3ZycOfd9556TOxkwMTExqSVCpQ92v5x9IMjCOHBaQz2liIHwaHbU8yo3aKm0msHiyew1Xhis2ADGis8gOwoj1Rg4EJgGao1poNbYiiK3Jx4i8ww4WerB2fcfSha22KyILheNF85XJbAcxQZUiFfDbjLFypd5Ghv0NaDUQlWL32M3ldKq1L4c+hk42gaGutuR/T7ePhnJxi53nEX2+5D9Pm72/uvvd0/vIvt9DHW366dWgZIGpudX2N5J0eduycYGcwQOeNqy62uXzrG9k2J6fkUHmftT0kA8kWRmYZUmhx2nmP6Ouu5pJZFMsbYezxpwig6az5xgZmGVeCKpv+ocys7A5FwYAK9LBGDQ08bHrz8JhML0u1uwWS30Z05oL9dIyhoIhJYB8HaJtDbW4xQdBKUIQSlCvf0YvReb8HaJeblGUnyRFRAIhZFl6HO3MJhpmaC0TOT3JpCeA69LRJbTuYUI0U2NJedT1kA0toW0FOVqZzM3ejsAmApFiMbiJJIphns6uNLZjLQUJRrb0lWsEqrugcm5MMfrbNwfdhOObvDj1x/iiSSfvq1xx+vEXmerSf+DSgOBuXRvn7LXMZXT50Epgs1qycsxGtUnsEdQiuSslxVzjKT4t8qtCVnLDXrcA1qW4/OLvjzNR/tb6DCgZGBDs+IW/d+P0j3wGHgO1FdT2CJY5AarY2tjcXG7mjrlqPjfqHPktabDrpbvb+6ZQ3ygMA3Umv/aQEwzFepZLwxUYUAYUyqoI+sIjBm4n4mJiYkK/gKMMsGwKgOREAAAAABJRU5ErkJggg==' }
];
function GetImageType(x) {
    const str = x.toLowerCase();
    if (str.includes('.pdf'))
        return fileImageTypes.filter(y => y.type === 'pdf')[0].data;
    if (str.includes('.docx'))
        return fileImageTypes.filter(y => y.type === 'docx')[0].data;
}
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}