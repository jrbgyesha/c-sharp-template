﻿//DataTable Object Services
/*
 Requires JQuery: https://jqueryui.com/
 Requires DataTables.Net: http://datatables.net/
 */
class TableBuilder {
    constructor(_container, _table = null) {
        this.table = _table;
        this.container = _container;
        this.customDom = "<'row'<'col-sm-4'l><'col-sm-4'p><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>";
    }
    Destroy() {
        const _this = this;
        if (typeof _this.table !== 'undefined' && _this.table !== null) {
            _this.table.destroy();
        }
    }
    Reset() {
        const _this = this;
        _this.table = _this.Destroy(_this.table);
        _this.table = $(_this.container).DataTable({});
    }
    BasicTable(_data, _cols, _order = [0, 'desc'], _ordering = true, _displayStart = 1, _createdRow = '', _useCustomDom = false) {
        const _this = this;
        _this.table = $(_this.container).DataTable({
            autoWidth: false,
            data: _data,
            dom: (_useCustomDom) ? _this.customDom : 'lrtip',
            columns: _cols,
            ordering: _ordering,
            order: _order,
            displayStart: _displayStart,
            createdRow: _createdRow
        });
    }
    StaticTable(_order = [0, 'desc'], _ordering = true, _displayStart = 1) {
        const _this = this;
        _this.table = $(_this.container).DataTable({
            autoWidth: false,
            ordering: _ordering,
            order: _order,
            displayStart: _displayStart
        });
    }
    HideCol(c) {
        const _this = this;
        _this.table.column(c).visible(false);
    }
}

const _dataTableService = new TableBuilder('#detail');