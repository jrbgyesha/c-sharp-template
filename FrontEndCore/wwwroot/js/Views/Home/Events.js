﻿
class _EventCreate {
    constructor(_container, _data, _path = '/Home/FormGet', _run = true) {
        this.container = _container;
        this.data = _data;
        this.path = _path;
        if (_run) {
            this.Build();
        }
    }
    Build() {
        const _this = this;
        console.log(_this);
        $(_this.container).empty();
        $('body').css('background-image', 'url()');

        const _ajaxService = new AJAXService(URLPath +_this.path, 'POST', _this.data, true);
        _ajaxService.promise.then(function (result) {
            result = _ajaxService.Clean(result);
            console.log(result);
            eval('_AssetsCore =' + result.Data.asset);
            $(_this.container).empty();

            $(_this.container).append(result.Data.htmlPage);
            eval(result.Data.javascript);
            _message.Build(result.Message, result.Status, _message.Durations.moderate);
        });
    }
}

