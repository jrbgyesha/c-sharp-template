class _EJSSampleUsage extends _EJSTemplate {
    constructor(_container, _data, _cache, _run = true) {
        super(_container, _data, _cache, false)

        var testPageData = new pageData("Test PageData");
        var testTab = new tab("testTab", true, "Test Tab", true);
        var testTab2 = new tab("testTab2", true, "Test Tab 2", true);

        var testSection = new section("testSection", 6, true, "Test Section (with span: 6)", true, false);
        var testSection2 = new section("testSection2", 12, true, "Test Section (with span 12)", true, false);

        var testGroup = new group("testGroup", 12, 1);
        var testGroup2 = new group("testGroup2", 12, 1);

        var testElementRow = new elementRow(1);
        var testElementRow2 = new elementRow(1);

        var datePickerTest = new datePicker("datePickerTest", 12, 'datePickerTest');
        datePickerTest.properties.label = "Label for datePickerTest";
        datePickerTest.properties.placeholder = "placeholder for datePickerTest";
        datePickerTest.properties.format = "YYYY-MM-DD";
        datePickerTest.properties.invalidTooltip = "Please choose a valid date.";
        datePickerTest.properties.isClearable = true;

        var revisedTaxYA = new constants('revisedTaxYA', 1, 0);
        revisedTaxYA.year = 2020;
        revisedTaxYA.data = [
            { label: '2019', value: '1' },
            { label: '2018', value: '2' },
            { label: '2017', value: '3' },
            { label: '2016 & before', value: '4' }
        ];

        var textboxTest = new textbox("textboxTest", 12, 'textboxTest');
        textboxTest.properties.label = "Label for textboxTest";
        textboxTest.properties.subLabel = "Sublabel for textboxTest";
        textboxTest.properties.isClearable = true;
        textboxTest.properties.placeholder = "placeholder for textboxTest"

        var dropdownTest = new dropdown("dropdownTest", 12, 'dropdownTest');
        dropdownTest.properties.label = "Label for dropdownTest";
        dropdownTest.properties.subLabel = "Sublabel for dropdownTest";
        dropdownTest.properties.isClearable = true;
        dropdownTest.properties.dataSourceCode = revisedTaxYA;
        dropdownTest.properties.dataKey = 'value';
        dropdownTest.properties.dataValue = 'label';
        dropdownTest.properties.dataLiveSearch = true;
        dropdownTest.properties.placeholder = "placeholder for dropdownTest"

        var dropdownTest2 = new dropdown("dropdownTest2", 12, 'dropdownTest2');
        dropdownTest2.properties.label = "Label for dropdownTest 2";
        dropdownTest2.properties.subLabel = "Sublabel for dropdownTest 2";
        dropdownTest2.properties.isClearable = false;
        dropdownTest2.properties.dataSource = "dummyData";
        dropdownTest2.properties.dataKey = 'ref';
        dropdownTest2.properties.dataValue = 'entity';
        dropdownTest2.properties.dataLiveSearch = true;
        dropdownTest2.properties.placeholder = "placeholder for dropdownTest 2"

        var labelTest = new label("test", 10, "test", "testLabel 123");
        labelTest.properties.isBold = true;
        labelTest.properties.subLabel = "test sub"

        var htmlTest = new html("test", 10, "test");
        htmlTest.properties.code = `<b> Hello World as a HTML </b>`

        var textAreaTest = new textarea("test", 10, "test");
        textAreaTest.properties.rows = 7
        textAreaTest.properties.label = "Test Label for TextArea"

        testElementRow.addElement(dropdownTest);
        testElementRow.addElement(dropdownTest2);
        testElementRow.addElement(datePickerTest);
        testElementRow.addElement(textboxTest);

        testElementRow2.addElement(labelTest);
        testElementRow2.addElement(htmlTest);
        testElementRow2.addElement(textAreaTest);

        testGroup.addElementRow(testElementRow);
        testGroup2.addElementRow(testElementRow2);

        testSection.addGroup(testGroup);
        testSection2.addGroup(testGroup2);

        testTab.addSection(testSection);
        testTab2.addSection(testSection2);

        testPageData.addTab(testTab);
        testPageData.addTab(testTab2);

        const commonToolbar = new toolbar("commonToolbar");

        const commonValidationSettingsToolbarButtonGroup = new toolbarButtonGroup("commonValidationSettingsToolbarButtonGroup");
        commonValidationSettingsToolbarButtonGroup.groupName = "Validation Settings";

        const commonValidateToolbarButton = new toolbarButton("commonValidateToolbarButton");
        commonValidateToolbarButton.label = "Validate Form";
        commonValidateToolbarButton.icon = "fas fa-clipboard-check";
        commonValidateToolbarButton.onclick = "validateFormBeforeSubmit()";
        commonValidateToolbarButton.dataRefAttributeReference = `'#formId[value]' && ('#formStatus[value]' == 'Draft' || '#formStatus[value]' == 'Rejected' ) ? true : false`; //Embeded default Id for a form
        commonValidateToolbarButton.dataRefIsVisibility = true;

        commonValidationSettingsToolbarButtonGroup.addToolbarButton(commonValidateToolbarButton);

        const commonTabSettingsToolbarButtonGroup = new toolbarButtonGroup("commonTabSettingsToolbarButtonGroup");
        commonTabSettingsToolbarButtonGroup.groupName = "Tab Settings";

        const commonBottomTabToolbarButtonToggle = new toolbarButtonToggle("commonBottomTabToolbarButtonToggle");
        commonBottomTabToolbarButtonToggle.label = "Bottom Tab";
        commonBottomTabToolbarButtonToggle.onChange = "ToggleBottomTab()";

        commonTabSettingsToolbarButtonGroup.addToolbarButton(commonBottomTabToolbarButtonToggle);

        const commonPDFToolbarButtonGroup = new toolbarButtonGroup("commonPDFToolbarButtonGroup");
        commonPDFToolbarButtonGroup.groupName = "PDF";

        const commonSavePDFToolbarButton = new toolbarButton("commonSavePDFToolbarButton");
        commonSavePDFToolbarButton.label = "Save PDF";
        commonSavePDFToolbarButton.icon = "far fa-file-pdf";
        commonSavePDFToolbarButton.onclick = "savePDF()";
        commonSavePDFToolbarButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonSavePDFToolbarButton.dataRefIsVisibility = true;

        const commonViewPDFToolbarButton = new toolbarButton("commonViewPDFToolbarButton");
        commonViewPDFToolbarButton.label = "View PDF";
        commonViewPDFToolbarButton.icon = "fas fa-search";
        commonViewPDFToolbarButton.onclick = "viewPDF()";

        commonPDFToolbarButtonGroup.addToolbarButton(commonSavePDFToolbarButton);
        //commonPDFToolbarButtonGroup.addToolbarButton(commonViewPDFToolbarButton);

        var toolbarButtonGroupTest = new toolbarButtonGroup("Test");
        toolbarButtonGroupTest.groupName = "Form Settings";
        toolbarButtonGroupTest.isAdmin = false;

        var commonGrantAccessButton = new toolbarButtonToggle("commonGrantAccessButton");
        commonGrantAccessButton.label = "Grant Access";
        commonGrantAccessButton.onChange = "GrantAccess()";
        commonGrantAccessButton.checked = false;
        commonGrantAccessButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonGrantAccessButton.dataRefIsVisibility = true;

        var commonReCreateFinalFolderToolbarButton = new toolbarButton("ReCreateFinalFolder");
        commonReCreateFinalFolderToolbarButton.properties.typeProp = "submit";
        commonReCreateFinalFolderToolbarButton.label = "Re-Create Files";
        commonReCreateFinalFolderToolbarButton.properties.method = "POST";
        commonReCreateFinalFolderToolbarButton.icon = "fas fa-file-signature";
        commonReCreateFinalFolderToolbarButton.properties.tooltipMessage = "Re-Create File(s) in Final Folder To IRAS."
        commonReCreateFinalFolderToolbarButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonReCreateFinalFolderToolbarButton.dataRefIsVisibility = true;
        commonReCreateFinalFolderToolbarButton.properties.submissionLink = `/api/action/recreatefinalfolder/Test`;
        commonReCreateFinalFolderToolbarButton.properties.dfRoute = '/test';

        var commonReCreateAndSendSftpToolbarButton = new toolbarButton("ReCreateAndSendSftp");
        commonReCreateAndSendSftpToolbarButton.properties.typeProp = "submit";
        commonReCreateAndSendSftpToolbarButton.label = "Re-Create & Send";
        commonReCreateAndSendSftpToolbarButton.properties.method = "POST";
        commonReCreateAndSendSftpToolbarButton.icon = "fas fa-file-export";
        commonReCreateAndSendSftpToolbarButton.properties.tooltipMessage = "Re-Create File(s) in Final Folder and send to IRAS SFTP."
        commonReCreateAndSendSftpToolbarButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonReCreateAndSendSftpToolbarButton.dataRefIsVisibility = true;
        commonReCreateAndSendSftpToolbarButton.properties.submissionLink = `/api/action/recreateandsendsftp/Test`;
        commonReCreateAndSendSftpToolbarButton.properties.dfRoute = '/test';

        toolbarButtonGroupTest.addToolbarButton(commonGrantAccessButton)
        toolbarButtonGroupTest.addToolbarButton(commonReCreateFinalFolderToolbarButton)
        toolbarButtonGroupTest.addToolbarButton(commonReCreateAndSendSftpToolbarButton)

        var toolbarButtonGroupTest2 = new toolbarButtonGroup("Test2");
        toolbarButtonGroupTest2.groupName = "Form Settings";

        var commonDeleteToolbarButton = new toolbarButton("commonDeleteToolbarButton");
        commonDeleteToolbarButton.properties.typeProp = "submit";
        commonDeleteToolbarButton.properties.method = "POST";
        commonDeleteToolbarButton.label = "Delete Form";
        commonDeleteToolbarButton.icon = "fas fa-trash-alt";
        commonDeleteToolbarButton.properties.tooltipMessage = "Delete Form."
        commonDeleteToolbarButton.dataRefAttribute = 'text';
        // commonDeleteToolbarButton.dataRefDisabledReference = `'#formId[value]' ? false : true`;
        // commonDeleteToolbarButton.dataRefDisabledList = [`#${commonDeleteToolbarButton.id}`];
        commonDeleteToolbarButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonDeleteToolbarButton.dataRefIsVisibility = true;
        commonDeleteToolbarButton.onLoadMessage = "Deleting, please wait..."
        commonDeleteToolbarButton.onSuccessfulMessage = "Successfully deleted."
        commonDeleteToolbarButton.onFailedMessage = "Error while deleting."

        var createUpdate = new toolbarButton("createUpdate", 3, "createUpdate");
        createUpdate.properties.typeProp = "submit";
        createUpdate.properties.method = "POST";
        createUpdate.dataRefAttribute = 'dynamicToolbarLabel';
        createUpdate.icon = "far fa-save";
        createUpdate.dataRefAttributeReference = `'#formId[value]' ? 'Update Draft' : 'Save as Draft'`;
        createUpdate.dataRefIsEval = true;
        createUpdate.properties.tooltipMessage = "Save as draft."
        createUpdate.onLoadMessage = "Saving, please wait..."
        createUpdate.onSuccessfulMessage = "Successfully saved."
        createUpdate.onFailedMessage = "Error while saving."

        var submit = new toolbarButton("submit", 3, "submit");
        submit.properties.typeProp = "submit";
        submit.label = "Submit";
        submit.properties.method = "POST";
        submit.icon = "fas fa-paper-plane"
        submit.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        submit.dataRefIsVisibility = true;
        submit.onLoadMessage = "Submitting, please wait..."
        submit.onSuccessfulMessage = "Successfully submitted."
        submit.onFailedMessage = "Error while submitting."

        toolbarButtonGroupTest2.addToolbarButton(commonDeleteToolbarButton);
        toolbarButtonGroupTest2.addToolbarButton(createUpdate);
        toolbarButtonGroupTest2.addToolbarButton(submit);

        var toolbarButtonGroupTest3 = new toolbarButtonGroup("Test3");
        toolbarButtonGroupTest3.groupName = "CTTracker Settings";

        var commonCTTrackerUpdateButton = new toolbarButton("commonCTTrackerUpdate");
        commonCTTrackerUpdateButton.properties.typeProp = "submit";
        commonCTTrackerUpdateButton.label = "Update CTTracker";
        commonCTTrackerUpdateButton.properties.method = "POST";
        commonCTTrackerUpdateButton.icon = "fas fa-space-shuttle";
        commonCTTrackerUpdateButton.properties.tooltipMessage = "Update status to 'Sent to IRAS' and update CTTracker."
        commonCTTrackerUpdateButton.onSuccessfulMessage = "CTTracker Update Successful."
        commonCTTrackerUpdateButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonCTTrackerUpdateButton.dataRefIsVisibility = true;

        toolbarButtonGroupTest3.addToolbarButton(commonCTTrackerUpdateButton);

        commonToolbar.addToolbarButtonGroup(toolbarButtonGroupTest);
        commonToolbar.addToolbarButtonGroup(toolbarButtonGroupTest2);
        commonToolbar.addToolbarButtonGroup(toolbarButtonGroupTest3);

        commonToolbar.addToolbarButtonGroup(commonValidationSettingsToolbarButtonGroup);
        commonToolbar.addToolbarButtonGroup(commonTabSettingsToolbarButtonGroup);
        commonToolbar.addToolbarButtonGroup(commonPDFToolbarButtonGroup);

        testPageData.toolbar = commonToolbar;

        this.pageData = testPageData;

        if (_run) this.BuildAndInitialize();
    }
}