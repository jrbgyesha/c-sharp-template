﻿class _EJSTemplate {
    constructor(_container, _data, _cache, _run = true) {
        this.container = _container;
        this.data = _data;
        this.cache = _cache;
        this.pageData = null;
        String.prototype.interpolate = function (params) {
            const names = Object.keys(params);
            const vals = Object.values(params);
            return new Function(...names, `return \`${this}\`;`)(...vals);
        };
        String.prototype.EJSRender = function ( data) {
            return ejs.render(this, data);
        }
        if (_run) {
            this.BuildAndInitialize();
        }
    }
 
    async BuildAndInitialize(){
        this.Build().then(() =>{
            this.Initialize();
        }).catch(err =>{
            //Show a noty -- for now console log
            console.log(err);
        });
    }
    
    async Build() {
        const _this = this;
        $(_this.container).empty();
        $('body').css('background-image', 'url()');

        return new Promise((resolve, reject) =>
        {
            if(_this.pageData === null){
                reject("No pageData found.")
                return;
            }
            
            try{
                $.when(
                    //#region Common Properties
                    _this.cache.GetServerObject('buttonProp', URLPath + '/js/Components/General/commonProperties/buttonProp.ejs'),
                    _this.cache.GetServerObject('dataReferenceProp', URLPath + '/js/Components/General/commonProperties/dataReferenceProp.ejs'),
                    _this.cache.GetServerObject('dropdownProp', URLPath + '/js/Components/General/commonProperties/dropdownProp.ejs'),
                    _this.cache.GetServerObject('elementProp', URLPath + '/js/Components/General/commonProperties/elementProp.ejs'),
                    _this.cache.GetServerObject('textboxProp', URLPath + '/js/Components/General/commonProperties/textboxProp.ejs'),
                    //#endregion
        
                    //#region Layout
                    _this.cache.GetServerObject('elementAppendCloseLayout', URLPath + '/js/Components/General/layout/elementAppendCloseLayout.ejs'),
                    _this.cache.GetServerObject('elementAppendLayout', URLPath + '/js/Components/General/layout/elementAppendLayout.ejs'),
                    _this.cache.GetServerObject('elementInvalidFeedback', URLPath + '/js/Components/General/layout/elementInvalidFeedback.ejs'),
                    _this.cache.GetServerObject('elementInvalidFeedbackLayout', URLPath + '/js/Components/General/layout/elementInvalidFeedbackLayout.ejs'),
                    _this.cache.GetServerObject('elementLabelLayout', URLPath + '/js/Components/General/layout/elementLabelLayout.ejs'),
                    _this.cache.GetServerObject('elementPrependLayout', URLPath + '/js/Components/General/layout/elementPrependLayout.ejs'),
                    _this.cache.GetServerObject('elementSubLabelLayout', URLPath + '/js/Components/General/layout/elementSubLabelLayout.ejs'),
                    _this.cache.GetServerObject('menu', URLPath + '/js/Components/General/layout/menu.ejs'),
                    //#endregion
        
                    //#region 
                    _this.cache.GetServerObject('elementRenderer', URLPath + '/js/Components/General/renderer/elementRenderer.ejs'),
                    _this.cache.GetServerObject('elementRowRenderer', URLPath + '/js/Components/General/renderer/elementRowRenderer.ejs'),
                    _this.cache.GetServerObject('elementSectionRenderer', URLPath + '/js/Components/General/renderer/elementSectionRenderer.ejs'),
                    _this.cache.GetServerObject('fromIRASFinalFilesRenderer', URLPath + '/js/Components/General/renderer/fromIRASFinalFilesRenderer.ejs'),
                    _this.cache.GetServerObject('navTabsRenderer', URLPath + '/js/Components/General/renderer/navTabsRenderer.ejs'),
                    _this.cache.GetServerObject('toIRASFinalFilesRenderer', URLPath + '/js/Components/General/renderer/toIRASFinalFilesRenderer.ejs'),
                    _this.cache.GetServerObject('toolbarRenderer', URLPath + '/js/Components/General/renderer/toolbarRenderer.ejs'),
                    //#endregion
        
                    //#region Toolbar
                    _this.cache.GetServerObject('toolbarButton', URLPath + '/js/Components/General/toolbar/toolbarButton.ejs'),
                    _this.cache.GetServerObject('toolbarButtonToggle', URLPath + '/js/Components/General/toolbar/toolbarButtonToggle.ejs'),
                    //#endregion
        
                    //#region General Componenets
                    _this.cache.GetServerObject('button', URLPath + '/js/Components/General/button.ejs'),
                    _this.cache.GetServerObject('datePicker', URLPath + '/js/Components/General/datePicker.ejs'),
                    _this.cache.GetServerObject('dropdown', URLPath + '/js/Components/General/dropdown.ejs'),
                    _this.cache.GetServerObject('html', URLPath + '/js/Components/General/html.ejs'),
                    _this.cache.GetServerObject('label', URLPath + '/js/Components/General/label.ejs'),
                    _this.cache.GetServerObject('textarea', URLPath + '/js/Components/General/textarea.ejs'),
                    _this.cache.GetServerObject('textbox', URLPath + '/js/Components/General/textbox.ejs'),
                    //#endregion           
                )
                .done(function (
                    buttonProp,
                    dataReferenceProp,
                    dropdownProp,
                    elementProp,
                    textboxProp,
                    
                    elementAppendCloseLayout,
                    elementAppendLayout,
                    elementInvalidFeedback,
                    elementInvalidFeedbackLayout,
                    elementLabelLayout,
                    elementPrependLayout,
                    elementSubLabelLayout,
                    menu,
        
                    elementRenderer,
                    elementRowRenderer,
                    elementSectionRenderer,
                    fromIRASFinalFilesRenderer,
                    navTabsRenderer,
                    toIRASFinalFilesRenderer,
                    toolbarRenderer,
        
                    EJStoolbarButton,
                    EJStoolbarButtonToggle,
        
                    EJSbutton,
                    EJSdatePicker,
                    EJSdropdown,
                    EJShtml,
                    EJSlabel,
                    EJStextarea,
                    EJStextbox
                    ) {
        
                    const dataSource = {
                        dummyData: [
                            { entity: 'Test Option 1', ref: '123', key: 'abc' },
                            { entity: 'Test Option 2', ref: '456', key: 'def' },
                            { entity: 'Test Option 3', ref: '789', key: 'ghi' }
                        ]
                    }
        
                    const EJSData = { 
                        prop: {
                            buttonProp,
                            dataReferenceProp,
                            dropdownProp,
                            elementProp,
                            textboxProp
                        },
        
                        layout: {
                            elementAppendCloseLayout,
                            elementAppendLayout,
                            elementInvalidFeedback,
                            elementInvalidFeedbackLayout,
                            elementLabelLayout,
                            elementPrependLayout,
                            elementSubLabelLayout,
                            menu
                        },
        
                        renderer: {
                            elementRenderer,
                            elementRowRenderer,
                            elementSectionRenderer,
                            fromIRASFinalFilesRenderer,
                            navTabsRenderer,
                            toIRASFinalFilesRenderer,
                            toolbarRenderer
                        },
        
                        toolbar: {
                            toolbarButton: EJStoolbarButton,
                            toolbarButtonToggle: EJStoolbarButtonToggle
                        },
            
                        component: {
                            button: EJSbutton,
                            datePicker: EJSdatePicker,
                            dropdown: EJSdropdown,
                            html: EJShtml,
                            label: EJSlabel,
                            textarea: EJStextarea,
                            textbox: EJStextbox
                        },
        
                        dataSource
                    }
        
                    Object.keys(EJSData).forEach(EJSKey => { 
                        var Data = EJSData[EJSKey];
                        Object.keys(Data).forEach(key =>{
                            _this.cache.PutCache(key, Data[key]); 
                        });
                    })

                    let renderedHtml = EJSData.renderer.elementRenderer.EJSRender({isMasterAdmin: false, pageData: _this.pageData, EJSHelper, EJSData})
        
                    $(_this.container).empty().append(renderedHtml);
                    resolve();
                });
            }catch(err){
                reject(err)
            }
        });
    }

    async Initialize(){
        initializeDatepickers();
        initializeDataTables();

        await InitializeBootstrapSelect2().catch((error) => {
            console.log("InitializeBootstrapSelect2", error);
        })

        InitializeDynamicRenderer();
    }
}