﻿class _Template {
    constructor(_container, _data, _cache, _run = true) {
        this.container = _container;
        this.data = _data;
        this.cache = _cache;
        String.prototype.interpolate = function (params) {
            const names = Object.keys(params);
            const vals = Object.values(params);
            return new Function(...names, `return \`${this}\`;`)(...vals);
        };
        if (_run) {
            this.Build();
        }
    }
    Build() {
        const _this = this;
        $(_this.container).empty();
        $('body').css('background-image', 'url()');


        $.when(
            _this.cache.GetServerObject('newform', URLPath + '/js/Components/Home/NewForm.html'),
            _this.cache.GetServerObject('selectbox', URLPath + '/js/Components/Home/SelectBox.html'))
        .done(function (a1, a2) {
            console.log(a1);
            console.log(a2);
            _this.cache.PutCache('newform', a1);
            _this.cache.PutCache('selectbox', a2);
            const groups = [
                { id: 'test1', name: 'Test 1', feather: 'plus', options: '<option>Test1</option><option>Test2</option>' },
                { id: 'test2', name: 'Group 1', feather: 'user', options: '<option>Test3</option><option>Test4</option>' }
            ];
            const _fields = groups.map(x => {
                return a2.interpolate({ _id: x.id, _name: x.name, _feather: x.feather, _options: x.options });
            }).join('');

            $(_this.container).empty().append(a1.interpolate({ _fields }));
            feather.replace();
        });
    }
}