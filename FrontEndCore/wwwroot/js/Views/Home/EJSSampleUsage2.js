﻿class _EJSSampleUsage2 extends _EJSTemplate {
    constructor(_container, _data, _cache, _run = true) {
        super(_container, _data, _cache, false)

        //#region ConstantData
        var typeOfTaskList = new constants("typeOfTaskList", 1, 0);
        typeOfTaskList.data = [
            { typeOfTaskListCode: "01_00", taskName: "Tax Return Review", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "01_02", taskName: "Revised Tax Return Review", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "02_00", taskName: "Tax Clearance Return Review", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "02_01", taskName: "Revised Tax Clearance Return", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "03_00", taskName: "Tax Equalisation Review", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "04_00", taskName: "Tax Return Submission", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "05_00", taskName: "Tax Briefing Checklist Approval", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "06_00", taskName: "NOA Verification", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "07_00", taskName: "Compliance", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "08_00", taskName: "Advisory", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "09_00", taskName: "NIL Return", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "10_00", taskName: "Immigration", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "11_00", taskName: "Micellaneous", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "12_00", taskName: "Form IR8A or Appendices Review", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "13_00", taskName: "Engagement Letter", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "14_00", taskName: "Timesheet", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "15_00", taskName: "Billing - Draft", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "15_01", taskName: "Billing", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "16_00", taskName: "SAP - Draft", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "16_01", taskName: "SAP - Final", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "17_00", taskName: "Tax Briefing - Senior", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "17_00", taskName: "Tax Briefing - AM", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "17_00", taskName: "Tax Briefing - Manger", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "18_00", taskName: "Provisional Tax Computation", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { typeOfTaskListCode: "18_01", taskName: "Provisional Hypothetical Tax", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" }
        ];

        var taskPriorityList = new constants("taskPriorityList", 1, 0);
        taskPriorityList.data = [
            { taskPriorityListCode: "01_00", priority: "Direct Final", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "02_01", priority: "Finalised", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "02_02", priority: "Finalised VIP", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "03_00", priority: "Partner to sign", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "04_00", priority: "Rework", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "05_00", priority: "Urgent", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "05_01", priority: "Urgent Finalised", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "05_02", priority: "Urgent Partner to sign", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "05_03", priority: "Urgent Rework", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "05_04", priority: "Urgent VIP", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" },
            { taskPriorityListCode: "06_00", priority: "VIP", isDeleted: false, departmentScope: "ST,GES,GST", countryScope: "SG,MY" }
        ];

        var gesPartnerList = new constants("gesPartnerList", 1, 0);
        gesPartnerList.data = [
            { gesPartnerListCode: "01_00", name: "Lim, Jill", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "02_00", name: "Sia, Sabrina", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "03_00", name: "Lee, Joanne Li Ling", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "04_00", name: "Chao, Michele Wen Ji", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "05_00", name: "Alton, Lisa", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "06_00", name: "Park, Jinho", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "07_00", name: "SG ECM User 4", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "08_00", name: "SG Tax App Services", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "09_00", name: "Ee, Pei Shan", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "10_00", name: "Karl, Christina", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "11_00", name: "Bhandal, Sandip", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "12_00", name: "Fong, Lai Ee", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "13_00", name: "Lam, Donny", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "14_00", name: "Thai Phuong, Dion", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesPartnerListCode: "15_00", name: "Poh, Jocelyn", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" }
        ];

        var gesManagerList = new constants("gesManagerList", 1, 0);
        gesManagerList.data = [
            { gesManagerListCode: "01_00", name: "Chia, Wen Lin", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "02_00", name: "Kuah, Carson CH", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "03_00", name: "Poh, Jocelyn", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "04_00", name: "Fong, Lai Ee", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "05_00", name: "Lam, Donny", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "06_00", name: "Leung, Tak Chung", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "07_00", name: "Lee, Foong Yi", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "08_00", name: "Kwok, Patrick YC", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "09_00", name: "Ling, Carol HP", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "10_00", name: "Kua, Shu Hui", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "11_00", name: "Yeo, Terry DL", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "12_00", name: "Lee, Joanne Li Ling", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "13_00", name: "Cheong, Grace Pui Yeng", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "14_00", name: "Deshpande, Anupama", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "15_00", name: "SG EMC user 3", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "16_00", name: "Ong, Hui Hong", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "17_00", name: "Sujaeh, Nur-Washelah", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "18_00", name: "SG Tax App Services", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "19_00", name: "Cheng, Zi Hui", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "20_00", name: "Hong, Bettina", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "21_00", name: "Tew, Athena", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "22_00", name: "Teng, Jane Seow Fen", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "23_00", name: "Wyatt, Katie", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "24_00", name: "Novi, Novi", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "25_00", name: "Ou, Joy Liping", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "26_00", name: "Tan, Eng Sing", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "27_00", name: "Ronquillo, Mark Ramos", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "28_00", name: "Ee, Pei Shan", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "29_00", name: "Bhandal, Sandip", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "30_00", name: "Eber, Sheila", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "31_00", name: "Kovesnikova, Anastasija", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "32_00", name: "Cheah, Guak Ee", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "33_00", name: "Favalli, Marilisa", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "34_00", name: "Utomo, Nelly Yuliana", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "35_00", name: "Gomes, Andrew Januarius", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "36_00", name: "Chng, Eng Ching", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "37_00", name: "Gess, Dwayne", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "38_00", name: "Woo, Jaan Feng", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "39_00", name: "Hong, Bettina", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "40_00", name: "Tew, Athena", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesManagerListCode: "41_00", name: "Goh, Si Ting", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" }
        ]

        var gesSicList = new constants("gesSicList", 1, 0);
        gesSicList.data = [
            { gesSicListCode: "01_00", name: "Chan, Yi Wei", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "02_00", name: "Chow, Michelle", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "03_00", name: "Soh, Fiona Hui Shiang", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "04_00", name: "Fong, Jill Jing Ying", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "05_00", name: "He, Peiling", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "06_00", name: "Law, Rachelle Juan An", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "07_00", name: "Halim, Stephan", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "08_00", name: "Pang, Chloe Yee Tin", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "09_00", name: "Lim, Brianna Xin Yi", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "10_00", name: "Tan, Angeline", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "11_00", name: "Yati, Ida Inu", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "12_00", name: "SG Tax App Services", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "13_00", name: "Yeo, Elena Mei Lyn", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "14_00", name: "SG EMC user 1", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "15_00", name: "SG EMC user 2", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "16_00", name: "Poh, Lay Bee", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "17_00", name: "Soo, Hoi Veon", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "18_00", name: "Tan, Eng Sing", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "19_00", name: "Lau, Aaron Kai Wen", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "20_00", name: "Ng, Jacqueline Qian Su", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "21_00", name: "Manoharan, Sharmini Manan", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "22_00", name: "Boo, Eleen Wei Yi", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "23_00", name: "Cheong, Eugene Kar Wye", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "24_00", name: "Abdul Hamid, Abdul Hafiz", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "25_00", name: "Hassan, Nur Ashikin", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "26_00", name: "Lau, Serene Mui Hong", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "27_00", name: "Yee, Amanda", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "28_00", name: "Chen, Joyce Lin", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "29_00", name: "Goh, Tracy Huey Ling", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "30_00", name: "Tan, Wen Jie", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "31_00", name: "Gomes, Andrew Januarius", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "32_00", name: "Foo, Jeslyn Wei Ling", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "33_00", name: "Su, Tina Tsu Ting", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "34_00", name: "Ng, Ting Wei", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "35_00", name: "Tan, Ayla Xue Yi", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "36_00", name: "Lin, Arnold Michael", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "37_00", name: "Lee, Yishan", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "38_00", name: "Osman, Kasmilah", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "39_00", name: "Ng, Felicia Shu Min", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "40_00", name: "Ho, Samantha Clare Min Hui", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "41_00", name: "Phang, Zhi Deng", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "42_00", name: "Chua, Daphne Zi Xiang", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "43_00", name: "Garrido, Calvin Niel", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "44_00", name: "Beh, Traci Hui Ying", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "45_00", name: "Abelinde, Jenny Rose", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "46_00", name: "Austero, Lera Sheane", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "47_00", name: "Bagorio, Kenneth John", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "48_00", name: "Benipayo, Aileen Joy", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "49_00", name: "Cabrera, Lorenzo Victorio", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "50_00", name: "Carumay, Julie Anne", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "51_00", name: "Cayme, Jona Mari", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "52_00", name: "Fajardo, Jerico", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "53_00", name: "Garrido, Calvin Niel", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "54_00", name: "Gaynilo, Piabelle", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "55_00", name: "Lacap, Maria Deah", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "56_00", name: "Lazarte, Patricia Edna", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "57_00", name: "Legaspi, Trish Ann", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "58_00", name: "Mantaring, Patricia Mae", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "59_00", name: "Nicolas, Verna", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "60_00", name: "Pelaez, Angelica Marie", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "61_00", name: "Perez, Jhonelwin", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "62_00", name: "Perez, Mary Antonette", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "63_00", name: "Quitos, Rose Jean", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "64_00", name: "Ragas, Aira Christina", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "65_00", name: "Romeroso, Jabelle", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "66_00", name: "Santiano, Airah Khrizzia Ericka B", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "67_00", name: "Torres, Rhowen Anne", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "68_00", name: "Wata, Rencyl Ira", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "69_00", name: "Sng, Eline Yi Ling", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "70_00", name: "Chia, Xin Yi", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "71_00", name: "Romeroso, Jabelle", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesSicListCode: "72_00", name: "Navea, Pamela", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" }
        ]

        var gesAmList = new constants("gesAmList", 1, 0);
        gesAmList.data = [
            { gesAmListCode: "01_00", name: "Goh, Si Ting", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "02_00", name: "Kek, May Yee", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "03_00", name: "Ang, Dawn Hui Hui", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "04_00", name: "Sia, Charlene", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "05_00", name: "Lee, Jeremiah", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "06_00", name: "Ng, Mandy FX", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "07_00", name: "Quek, Lester Qinzhi", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "08_00", name: "Ou, Joy Liping", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "09_00", name: "SG Tax App Services", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "10_00", name: "Boon, Daisy Jie Er", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "11_00", name: "Kang, Pei Rong", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "12_00", name: "Tan, Eng Sing", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "13_00", name: "Poh, Delvina SP", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "14_00", name: "Fong, Jill Jing Ying", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "15_00", name: "Ang, Caroline", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "16_00", name: "Abdullah, Nooraleesa", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "17_00", name: "Tapales, June", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "18_00", name: "Trinidad, Remigia Guia", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "19_00", name: "Marbil, Isabel Maria", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "20_00", name: "Sy, Clarence", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "21_00", name: "Panistante, Ma. Jeszel Paragas", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "22_00", name: "Gonzales, Katherine Cleo", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "23_00", name: "Gomes, Andrew Januarius", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "24_00", name: "Natalia, Joice", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" },
            { gesAmListCode: "25_00", name: "SG EMC user 2", isDeleted: false, departmentScope: "BT,GES,GST", countryScope: "SG,MY" }
        ]

        const constantData = [
            typeOfTaskList,
            taskPriorityList,
            gesPartnerList,
            gesManagerList,
            gesSicList,
            gesAmList,
        ]

        //#endregion

        console.log(constantData);
        console.log(constantData.find(data => data.constantName === "typeOfTaskList"));

        var workflowManagerPageData = new pageData("Workflow Manager");
        var sectionTab = new tab("sectionTab", true, "");

        var submissionForm = new section("submissionForm", 12, true, "Submission Form", true);
        var assignApprovers = new section("assignApprovers", 12, true, "Assign Approvers");
        var buttons = new section("buttons", 12);

        // content for submissionForm
        var name = new label("name", 3, "", "Name :");
        var companyName = new label("companyName", 3, "", "Company Name :");
        var employeeName = new label("employeeName", 3, "", "Employee Name / Description :");
        var trackerId = new label("trackerId", 3, "", "Tracker ID :");
        var ya = new label("ya", 2, "", "Year of Assessment : ");
        var typeOfTask = new label("typeOfTask", 3, "", "Type Of Task:");
        var taskPriority = new label("taskPriority", 2, "", "Task Priority:");
        taskPriority.offsetSpan = 1;
        var dueDate = new label("dueDate", 3, "", "Due Date:");

        var nameValue = new textbox("nameValue", 9, "nameValue");
        nameValue.plaintext = true;
        nameValue.readonly = true;
        nameValue.properties.value = "Some name";
        var companyNameValue = new textbox("companyNameValue", 9, "companyNameValue");
        companyNameValue.plaintext = true;
        companyNameValue.readonly = true;
        companyNameValue.properties.value = "Some Company Name";
        var employeeNameValue = new textbox("employeeNameValue", 9, "employeeNameValue");
        employeeNameValue.plaintext = true;
        employeeNameValue.readonly = true;
        employeeNameValue.properties.value = "Some Employee Name / Description";
        var trackerIdValue = new textbox("trackerIdValue", 4, "trackerIdValue");
        trackerIdValue.plaintext = true;
        trackerIdValue.readonly = true;
        trackerIdValue.properties.value = "Some Tracker ID";
        var yaValue = new textbox("yaValue", 3, "yaValue");
        yaValue.plaintext = true;
        yaValue.readonly = true;
        yaValue.properties.value = "2018";

        var typeOfTaskValue = new dropdown("typeOfTaskValue", 3, "typeOfTaskValue");
        typeOfTaskValue.properties.dataSourceCode = constantData.find(data => data.constantName === "typeOfTaskList")
        typeOfTaskValue.properties.dataKey = 'taskName';
        typeOfTaskValue.properties.dataValue = 'taskName';
        typeOfTaskValue.properties.placeholder = "Please Select Type of Task";
        typeOfTaskValue.properties.required = true;
        typeOfTaskValue.properties.invalidTooltip = 'Type of Task is required.';
        typeOfTaskValue.properties.isClearable = true;

        var taskPriorityValue = new dropdown("taskPriorityValue", 3, "taskPriorityValue");
        taskPriorityValue.properties.dataSourceCode = constantData.find(data => data.constantName === "taskPriorityList")
        taskPriorityValue.properties.dataKey = 'priority';
        taskPriorityValue.properties.dataValue = 'priority';
        taskPriorityValue.properties.placeholder = "Please Select Type Priority";
        taskPriorityValue.properties.required = true;

        var dueDateValue = new datePicker("dueDateValue", 4, "dueDateValue");
        dueDateValue.properties.showDateIcon = true;
        dueDateValue.properties.isClearable = true;
        dueDateValue.properties.placeholder = "Please select a date."
        dueDateValue.properties.format = "dddd, DD MMMM YYYY"
        dueDateValue.properties.required = true;
        dueDateValue.properties.invalidFeedback = "Please choose a proper due date."


        // Creating elementRows
        var submissionFormRow1 = new elementRow();
        submissionFormRow1.properties.hrBelow = true;
        var submissionFormRow2 = new elementRow();
        submissionFormRow2.properties.hrBelow = true;
        var submissionFormRow3 = new elementRow();
        submissionFormRow3.properties.hrBelow = true;
        var submissionFormRow4 = new elementRow();
        submissionFormRow4.properties.hrBelow = true;
        var submissionFormRow5 = new elementRow();
        submissionFormRow5.properties.hrBelow = true;
        var submissionFormRow6 = new elementRow();

        //adding elements > row > section > tab > page
        submissionFormRow1.addElement(name);
        submissionFormRow1.addElement(nameValue);
        submissionFormRow2.addElement(companyName);
        submissionFormRow2.addElement(companyNameValue);
        submissionFormRow3.addElement(employeeName);
        submissionFormRow3.addElement(employeeNameValue);
        submissionFormRow4.addElement(trackerId);
        submissionFormRow4.addElement(trackerIdValue);
        submissionFormRow4.addElement(ya);
        submissionFormRow4.addElement(yaValue);
        submissionFormRow5.addElement(typeOfTask);
        submissionFormRow5.addElement(typeOfTaskValue);
        submissionFormRow5.addElement(taskPriority);
        submissionFormRow5.addElement(taskPriorityValue);
        submissionFormRow6.addElement(dueDate);
        submissionFormRow6.addElement(dueDateValue);


        submissionForm.addElementRow(submissionFormRow1);
        submissionForm.addElementRow(submissionFormRow2);
        submissionForm.addElementRow(submissionFormRow3);
        submissionForm.addElementRow(submissionFormRow4);
        submissionForm.addElementRow(submissionFormRow5);
        submissionForm.addElementRow(submissionFormRow6);




        // content for assignApprovers
        /*
        var staffInCharge = new label("staffInCharge", 2, "staffInCharge", "Staff In Charge :");
        var assistantManager = new label("assistantManager", 2, "assistantManager", "Assistant Manager :");
        var manager = new label("manager", 2, "manager", "Manager :");
        var partner = new label("partner", 2, "partner", "Partner :");
        var remarks = new label("remarks", 2, "remarks", "Remarks :");
        */

        var staffInChargeValue = new dropdown("staffInChargeValue", 12, "staffInChargeValue");
        staffInChargeValue.properties.label = "Staff In Charge :";
        staffInChargeValue.properties.subLabel = "Level 1 Approver (Please leave empty if you are a Staff in Charge)";
        staffInChargeValue.properties.dataSourceCode = constantData.find(data => data.constantName === "gesSicList")
        staffInChargeValue.properties.dataKey = 'name';
        staffInChargeValue.properties.dataValue = 'name';
        staffInChargeValue.properties.dataLiveSearch = true;
        staffInChargeValue.properties.placeholder = "Choose a Staff in Charge";


        var assistantManagerValue = new dropdown("assistantManagerValue", 12, "assistantManagerValue");
        assistantManagerValue.properties.label = "Assistant Manager :";
        assistantManagerValue.properties.subLabel = "Level 2 Approver (Please leave empty if you are an Assistant Manager)";
        assistantManagerValue.properties.dataSourceCode = constantData.find(data => data.constantName === "gesAmList")
        assistantManagerValue.properties.dataKey = 'name';
        assistantManagerValue.properties.dataValue = 'name';
        assistantManagerValue.properties.dataLiveSearch = true;
        assistantManagerValue.properties.placeholder = "Choose a Assistant Manager";

        var managerValue = new dropdown("managerValue", 12, "managerValue");
        managerValue.properties.label = "Manager :";
        managerValue.properties.subLabel = "Level 3 Approver";
        managerValue.properties.dataSourceCode = constantData.find(data => data.constantName === "gesManagerList")
        managerValue.properties.required = true;
        managerValue.properties.invalidFeedback = "Please choose the Manager.";
        managerValue.properties.dataKey = 'name';
        managerValue.properties.dataValue = 'name';
        managerValue.properties.dataLiveSearch = true;
        managerValue.properties.placeholder = "Choose a Manager";

        var partnerValue = new dropdown("partnerValue", 12, "partnerValue");
        partnerValue.properties.label = "Partner :";
        partnerValue.properties.subLabel = "Level 4 Approver (Please leave it blank if partner approval is not required)";
        partnerValue.properties.dataSourceCode = constantData.find(data => data.constantName === "gesPartnerList")
        partnerValue.properties.dataKey = 'name';
        partnerValue.properties.dataValue = 'name';
        partnerValue.properties.dataLiveSearch = true;
        partnerValue.properties.placeholder = "Choose a Partner";

        var remarksValue = new textarea("remarksValue", 12, "remarksValue");
        remarksValue.properties.label = "Remarks";


        // Creating elementRow
        var assignApproversRow1 = new elementRow();
        assignApproversRow1.properties.hrBelow = true;
        var assignApproversRow2 = new elementRow();
        assignApproversRow2.properties.hrBelow = true;
        var assignApproversRow3 = new elementRow();
        assignApproversRow3.properties.hrBelow = true;
        var assignApproversRow4 = new elementRow();
        assignApproversRow4.properties.hrBelow = true;
        var assignApproversRow5 = new elementRow();

        //adding elements > row > section > tab > page
        //assignApproversRow1.addElement(staffInCharge);
        assignApproversRow1.addElement(staffInChargeValue);
        //assignApproversRow2.addElement(assistantManager);
        assignApproversRow2.addElement(assistantManagerValue);
        assignApproversRow3.addElement(managerValue);
        assignApproversRow4.addElement(partnerValue);
        //assignApproversRow5.addElement(remarks);
        assignApproversRow5.addElement(remarksValue);

        assignApprovers.addElementRow(assignApproversRow1);
        assignApprovers.addElementRow(assignApproversRow2);
        assignApprovers.addElementRow(assignApproversRow3);
        assignApprovers.addElementRow(assignApproversRow4);
        assignApprovers.addElementRow(assignApproversRow5);

        // add in submit and cancel button
        var submit = new button("submit", 4, "submit");
        submit.properties.typeProp = "submit";
        submit.properties.label = "Submit";
        submit.offsetSpan = 2;
        submit.properties.submissionLink = '/api/Dynamic/Form/workflowManager';
        submit.properties.method = "POST";
        submit.dataRefAttribute = "display";
        submit.dataRefAttributeReference = `'#formId[value]' ? false : true`; //Embeded default Id for a form
        submit.dataRefIsVisibility = true;

        var update = new button("update", 4, "update");
        update.properties.typeProp = "submit";
        update.properties.label = "Update";
        update.offsetSpan = 2;
        update.properties.submissionLink = "/api/Dynamic/Form/workflowManager";
        update.properties.method = "PUT";
        update.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        update.dataRefIsVisibility = true;

        var cancel = new button("cancel", 4, "Cancel");
        cancel.properties.typeProp = "reset";
        cancel.offsetSpan = 2;
        cancel.properties.label = "Cancel";

        var buttonRow = new elementRow();
        buttonRow.addElement(submit);
        buttonRow.addElement(update);
        buttonRow.addElement(cancel);
        buttons.addElementRow(buttonRow);

        // Add all sections to the tab
        sectionTab.addSection(submissionForm);
        sectionTab.addSection(assignApprovers);
        sectionTab.addSection(buttons);
        workflowManagerPageData.addTab(sectionTab);

        workflowManagerPageData.pageName = 'workflowManager';
        workflowManagerPageData.icon = "fa fa-address-book";
        workflowManagerPageData.shortName = "Workflow Manager"
        workflowManagerPageData.order = 4;
        workflowManagerPageData.version = 1;
        workflowManagerPageData.subVersion = 4;
        workflowManagerPageData.year = 2019;
        workflowManagerPageData.country = ['SG', 'MY'];
        workflowManagerPageData.department = ['BT', 'GES'];
        workflowManagerPageData.enabled = true;
        workflowManagerPageData.pageDataType = "workflowManager";
        workflowManagerPageData.pageDataStatusList = ["New", "Draft"];

        const commonToolbar = new toolbar("commonToolbar");

        const commonValidationSettingsToolbarButtonGroup = new toolbarButtonGroup("commonValidationSettingsToolbarButtonGroup");
        commonValidationSettingsToolbarButtonGroup.groupName = "Validation Settings";

        const commonValidateToolbarButton = new toolbarButton("commonValidateToolbarButton");
        commonValidateToolbarButton.label = "Validate Form";
        commonValidateToolbarButton.icon = "fas fa-clipboard-check";
        commonValidateToolbarButton.onclick = "validateFormBeforeSubmit()";
        commonValidateToolbarButton.dataRefAttributeReference = `'#formId[value]' && ('#formStatus[value]' == 'Draft' || '#formStatus[value]' == 'Rejected' ) ? true : false`; //Embeded default Id for a form
        commonValidateToolbarButton.dataRefIsVisibility = true;

        commonValidationSettingsToolbarButtonGroup.addToolbarButton(commonValidateToolbarButton);

        const commonTabSettingsToolbarButtonGroup = new toolbarButtonGroup("commonTabSettingsToolbarButtonGroup");
        commonTabSettingsToolbarButtonGroup.groupName = "Tab Settings";

        const commonBottomTabToolbarButtonToggle = new toolbarButtonToggle("commonBottomTabToolbarButtonToggle");
        commonBottomTabToolbarButtonToggle.label = "Bottom Tab";
        commonBottomTabToolbarButtonToggle.onChange = "ToggleBottomTab()";

        commonTabSettingsToolbarButtonGroup.addToolbarButton(commonBottomTabToolbarButtonToggle);

        const commonPDFToolbarButtonGroup = new toolbarButtonGroup("commonPDFToolbarButtonGroup");
        commonPDFToolbarButtonGroup.groupName = "PDF";

        const commonSavePDFToolbarButton = new toolbarButton("commonSavePDFToolbarButton");
        commonSavePDFToolbarButton.label = "Save PDF";
        commonSavePDFToolbarButton.icon = "far fa-file-pdf";
        commonSavePDFToolbarButton.onclick = "savePDF()";
        commonSavePDFToolbarButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonSavePDFToolbarButton.dataRefIsVisibility = true;

        const commonViewPDFToolbarButton = new toolbarButton("commonViewPDFToolbarButton");
        commonViewPDFToolbarButton.label = "View PDF";
        commonViewPDFToolbarButton.icon = "fas fa-search";
        commonViewPDFToolbarButton.onclick = "viewPDF()";

        commonPDFToolbarButtonGroup.addToolbarButton(commonSavePDFToolbarButton);
        //commonPDFToolbarButtonGroup.addToolbarButton(commonViewPDFToolbarButton);

        var toolbarButtonGroupTest = new toolbarButtonGroup("Test");
        toolbarButtonGroupTest.groupName = "Form Settings";
        toolbarButtonGroupTest.isAdmin = false;

        var commonGrantAccessButton = new toolbarButtonToggle("commonGrantAccessButton");
        commonGrantAccessButton.label = "Grant Access";
        commonGrantAccessButton.onChange = "GrantAccess()";
        commonGrantAccessButton.checked = false;
        commonGrantAccessButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonGrantAccessButton.dataRefIsVisibility = true;

        var commonReCreateFinalFolderToolbarButton = new toolbarButton("ReCreateFinalFolder");
        commonReCreateFinalFolderToolbarButton.properties.typeProp = "submit";
        commonReCreateFinalFolderToolbarButton.label = "Re-Create Files";
        commonReCreateFinalFolderToolbarButton.properties.method = "POST";
        commonReCreateFinalFolderToolbarButton.icon = "fas fa-file-signature";
        commonReCreateFinalFolderToolbarButton.properties.tooltipMessage = "Re-Create File(s) in Final Folder To IRAS."
        commonReCreateFinalFolderToolbarButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonReCreateFinalFolderToolbarButton.dataRefIsVisibility = true;
        commonReCreateFinalFolderToolbarButton.properties.submissionLink = `/api/action/recreatefinalfolder/Test`;
        commonReCreateFinalFolderToolbarButton.properties.dfRoute = '/test';

        var commonReCreateAndSendSftpToolbarButton = new toolbarButton("ReCreateAndSendSftp");
        commonReCreateAndSendSftpToolbarButton.properties.typeProp = "submit";
        commonReCreateAndSendSftpToolbarButton.label = "Re-Create & Send";
        commonReCreateAndSendSftpToolbarButton.properties.method = "POST";
        commonReCreateAndSendSftpToolbarButton.icon = "fas fa-file-export";
        commonReCreateAndSendSftpToolbarButton.properties.tooltipMessage = "Re-Create File(s) in Final Folder and send to IRAS SFTP."
        commonReCreateAndSendSftpToolbarButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonReCreateAndSendSftpToolbarButton.dataRefIsVisibility = true;
        commonReCreateAndSendSftpToolbarButton.properties.submissionLink = `/api/action/recreateandsendsftp/Test`;
        commonReCreateAndSendSftpToolbarButton.properties.dfRoute = '/test';

        toolbarButtonGroupTest.addToolbarButton(commonGrantAccessButton)
        toolbarButtonGroupTest.addToolbarButton(commonReCreateFinalFolderToolbarButton)
        toolbarButtonGroupTest.addToolbarButton(commonReCreateAndSendSftpToolbarButton)

        var toolbarButtonGroupTest2 = new toolbarButtonGroup("Test2");
        toolbarButtonGroupTest2.groupName = "Form Settings";

        var commonDeleteToolbarButton = new toolbarButton("commonDeleteToolbarButton");
        commonDeleteToolbarButton.properties.typeProp = "submit";
        commonDeleteToolbarButton.properties.method = "POST";
        commonDeleteToolbarButton.label = "Delete Form";
        commonDeleteToolbarButton.icon = "fas fa-trash-alt";
        commonDeleteToolbarButton.properties.tooltipMessage = "Delete Form."
        commonDeleteToolbarButton.dataRefAttribute = 'text';
        // commonDeleteToolbarButton.dataRefDisabledReference = `'#formId[value]' ? false : true`;
        // commonDeleteToolbarButton.dataRefDisabledList = [`#${commonDeleteToolbarButton.id}`];
        commonDeleteToolbarButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonDeleteToolbarButton.dataRefIsVisibility = true;
        commonDeleteToolbarButton.onLoadMessage = "Deleting, please wait..."
        commonDeleteToolbarButton.onSuccessfulMessage = "Successfully deleted."
        commonDeleteToolbarButton.onFailedMessage = "Error while deleting."

        var createUpdate = new toolbarButton("createUpdate", 3, "createUpdate");
        createUpdate.properties.typeProp = "submit";
        createUpdate.properties.method = "POST";
        createUpdate.dataRefAttribute = 'dynamicToolbarLabel';
        createUpdate.icon = "far fa-save";
        createUpdate.dataRefAttributeReference = `'#formId[value]' ? 'Update Draft' : 'Save as Draft'`;
        createUpdate.dataRefIsEval = true;
        createUpdate.properties.tooltipMessage = "Save as draft."
        createUpdate.onLoadMessage = "Saving, please wait..."
        createUpdate.onSuccessfulMessage = "Successfully saved."
        createUpdate.onFailedMessage = "Error while saving."

        var submit = new toolbarButton("submit", 3, "submit");
        submit.properties.typeProp = "submit";
        submit.label = "Submit";
        submit.properties.method = "POST";
        submit.icon = "fas fa-paper-plane"
        submit.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        submit.dataRefIsVisibility = true;
        submit.onLoadMessage = "Submitting, please wait..."
        submit.onSuccessfulMessage = "Successfully submitted."
        submit.onFailedMessage = "Error while submitting."

        toolbarButtonGroupTest2.addToolbarButton(commonDeleteToolbarButton);
        toolbarButtonGroupTest2.addToolbarButton(createUpdate);
        toolbarButtonGroupTest2.addToolbarButton(submit);

        var toolbarButtonGroupTest3 = new toolbarButtonGroup("Test3");
        toolbarButtonGroupTest3.groupName = "CTTracker Settings";

        var commonCTTrackerUpdateButton = new toolbarButton("commonCTTrackerUpdate");
        commonCTTrackerUpdateButton.properties.typeProp = "submit";
        commonCTTrackerUpdateButton.label = "Update CTTracker";
        commonCTTrackerUpdateButton.properties.method = "POST";
        commonCTTrackerUpdateButton.icon = "fas fa-space-shuttle";
        commonCTTrackerUpdateButton.properties.tooltipMessage = "Update status to 'Sent to IRAS' and update CTTracker."
        commonCTTrackerUpdateButton.onSuccessfulMessage = "CTTracker Update Successful."
        commonCTTrackerUpdateButton.dataRefAttributeReference = `'#formId[value]' ? true : false`; //Embeded default Id for a form
        commonCTTrackerUpdateButton.dataRefIsVisibility = true;

        toolbarButtonGroupTest3.addToolbarButton(commonCTTrackerUpdateButton);

        commonToolbar.addToolbarButtonGroup(toolbarButtonGroupTest);
        commonToolbar.addToolbarButtonGroup(toolbarButtonGroupTest2);
        commonToolbar.addToolbarButtonGroup(toolbarButtonGroupTest3);

        commonToolbar.addToolbarButtonGroup(commonValidationSettingsToolbarButtonGroup);
        commonToolbar.addToolbarButtonGroup(commonTabSettingsToolbarButtonGroup);
        commonToolbar.addToolbarButtonGroup(commonPDFToolbarButtonGroup);

        workflowManagerPageData.toolbar = commonToolbar;

        console.log(workflowManagerPageData);

        this.pageData = workflowManagerPageData;
        if (_run) this.BuildAndInitialize();
    }
}