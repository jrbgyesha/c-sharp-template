﻿class HomeLanding
{
    constructor(_container,_data, _title, _lead = '', _run = true) {
        this.container = _container;
        this.data = _data;
        this.title = _title;
        this.lead = _lead;
        this.staggersAnimation = null;
        if (_run) {
            this.Build();
        }
    }
    Build() {
        const _this = this;
        $(_this.container).append(_this.ReturnElement());
        _this.BuildLoader();
        _this.InitiateLoader();
    }
    ReturnElement() {
        const _this = this;
        return `<div id="animatePane" name="animatePane" class="container animated fadeInUpBig" style="color:rgb(255, 255, 255); background-color:rgba(0, 0, 0,.85)">
                    <div class="jumbotron jumbotron-fluid" style="color:rgb(255, 255, 255); background-color:rgba(0, 0, 0,0)">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                <h1 class="display-4">${_this.title}</h1>
                                <p class="lead">${_this.lead}</p>
                                <div id="postLeadGroup"></div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                <div id="loaderGroup"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
    }
    Clear() {
        const _this = this;
        $(_this.container).empty();
    }
    BuildButton(_label, _class = 'btn-primary', _guid = 'btn' + parseInt(Math.random()*10000000)) {
        const _this = this;
        $('#postLeadGroup').append(`<button id="${_guid}" class="btn ${_class}">${_label}</button>`);
        return _guid;
    }
    BuildButtonEvent(_id, _eventFunc, _eventType = 'click') {
        const _this = this;
        $(_id).on(_eventType, _eventFunc);
    }
    BuildLoader() {
        $('#loaderGroup').append('<div class="stagger-visualizer"></div>');
    }
    InitiateLoader() {
        const _this = this;
        const staggerVisualizerEl = document.querySelector('.stagger-visualizer');
        const fragment = document.createDocumentFragment();
        const grid = [17, 17];
        const col = grid[0];
        const row = grid[1];
        const numberOfElements = col * row;

        for (let i = 0; i < numberOfElements; i++) {
            fragment.appendChild(document.createElement('div'));
        }

        staggerVisualizerEl.appendChild(fragment);

        _this.staggersAnimation = anime.timeline({
            targets: '.stagger-visualizer div',
            easing: 'easeInOutSine',
            delay: anime.stagger(50),
            loop: true,
            autoplay: false
        })
            .add({
                translateX: [
                    { value: anime.stagger('-.1rem', { grid: grid, from: 'center', axis: 'x' }) },
                    { value: anime.stagger('.1rem', { grid: grid, from: 'center', axis: 'x' }) }
                ],
                translateY: [
                    { value: anime.stagger('-.1rem', { grid: grid, from: 'center', axis: 'y' }) },
                    { value: anime.stagger('.1rem', { grid: grid, from: 'center', axis: 'y' }) }
                ],
                duration: 1000,
                scale: .5,
                delay: anime.stagger(100, { grid: grid, from: 'center' })
            })
            .add({
                translateX: () => anime.random(-10, 10),
                translateY: () => anime.random(-10, 10),
                delay: anime.stagger(8, { from: 'last' })
            })
            .add({
                translateX: anime.stagger('.25rem', { grid: grid, from: 'center', axis: 'x' }),
                translateY: anime.stagger('.25rem', { grid: grid, from: 'center', axis: 'y' }),
                rotate: 0,
                scaleX: 2.5,
                scaleY: .25,
                delay: anime.stagger(4, { from: 'center' })
            })
            .add({
                rotate: anime.stagger([90, 0], { grid: grid, from: 'center' }),
                delay: anime.stagger(50, { grid: grid, from: 'center' })
            })
            .add({
                translateX: 0,
                translateY: 0,
                scale: .5,
                scaleX: 1,
                rotate: 180,
                duration: 1000,
                delay: anime.stagger(100, { grid: grid, from: 'center' })
            })
            .add({
                scaleY: 1,
                scale: 1,
                delay: anime.stagger(20, { grid: grid, from: 'center' })
            });

        _this.staggersAnimation.play();
    }
    StopLoader() {
        const _this = this;
        _this.staggersAnimation.stop();
    }
}