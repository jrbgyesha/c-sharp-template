﻿class _EventManager {
    constructor(_container, _data, _run = true) {
        this.container = _container;
        this.data = _data;
        this.table = null;
        if (_run) {
            this.Build();
        }
    }

    Build() {
        const _this = this;
        $(_this.container).empty();
        let a1 = _this.cache.GetCache('laoder').data;
        $(_this.container).append(a1);
        const _ajaxService = new AJAXService(URLPath+'/Admin/EventsGet', 'GET', null, true);
        _ajaxService.promise.then(function (result) {
            $('#loader').removeClass('fadeIn').addClass('fadeOut');
            $(_this.container).empty();
            _this.data = _ajaxService.Clean(result);
            console.log(_this.data);
            _message.Build('Create & Manage Event Types', _message.Types.info, _message.Durations.moderate);
            $(_this.container).append(`
                                        <style>.green{ color: green;} .red{ color: red;}</style>
                                        <div>
                                        <h1>Event Types</h1>
                                        <button id="createNew" class="btn btn-primary btn-sm">Create</button>
                                    </div>
                                     <div class="row mt-2" style="width:100%">
                                        <div class="col-lg-12">
                                        <table id="events_tbl" name="events_tbl" class="table display">
                                            <thead>
                                                <tr>
                                                    <th>Event Name</th>
                                                    <th>Created By</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                ${_this.data.map(_this.BuildRow).join('')}
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>`);

            _this.table = new TableBuilder('#events_tbl');
            _this.table.StaticTable(1,true,15);
            feather.replace();
            _this.AddEvents();
        });

    }
    BuildRow({ EventCoreName, EventCoreID, ShowsOnDropDown }) {
        const _state = ShowsOnDropDown ? 'green' : 'red';
        return `
                <tr id="event${EventCoreID}" data-hook="${EventCoreID}">
                    <td>${EventCoreName}</td>
                    <td>Tyler Furrer</td>
                    <td>
                        <span id="activeState${EventCoreID}" data-feather="check-circle" class="mr-2 activate ${_state}" data-hook="${EventCoreID}"></span>
                        <span id="editEvent${EventCoreID}" class="edit" data-feather="edit" data-hook="${EventCoreID}"></span>
                        <span id="deleteEvent${EventCoreID}" class="delete ml-5" data-feather="trash-2" data-hook="${EventCoreID}"></span>
                    </td>
                </tr>
`;

    }
    AddEvents() {
        const _this = this;
        $('#createNew').on('click',
            function () {
                $('body').css('background-image', 'url()');
                $(_this.container).empty(); 
                //can pass the data here
                const e = new _EventCreator('#content', null);
            }
        );
        $('.activate').on('click', function () {
            const eventActivate = $(this).attr('data-hook');
            const _form = new FormData();
            _form.append("Id", eventActivate);
            _form.append("Level", "user");
            const _state = $(this).hasClass('red');
            if (_state) {
                $(this).removeClass('red');
                $(this).addClass('green');

            } else {
                $(this).addClass('red');
                $(this).removeClass('green');
            }
            _form.append("State", _state);
            const _ajaxService = new AJAXService(URLPath + '/Admin/EventToggleActive', 'POST', _form, true);
            _ajaxService.promise.then(function (result) {
                result = _ajaxService.Clean(result);
                console.log(result);
                _message.Build(result.Message, result.Status, _message.Durations.moderate);
            });

        });
        $('.delete').on('click', function () {
            const eventActivate = $(this).attr('data-hook');
            const _form = new FormData();
            _form.append("Id", eventActivate);
            const _ajaxService = new AJAXService(URLPath + '/Admin/EventDelete', 'POST', _form, true);
            _ajaxService.promise.then(function (result) {
                result = _ajaxService.Clean(result);
                console.log(result);
                _message.Build(result.Message, result.Status, _message.Durations.moderate);
                $('#event' + eventActivate).remove();
            });

        });
        $('.edit').on('click', function () {
            const eventSelection = _this.data.filter(x => x.EventCoreID == $(this).attr('data-hook'))[0];
            const e = new _EventCreator('#content', eventSelection);
            EventID_Detail = parseInt($(this).attr('data-hook'));
        });

    }
}