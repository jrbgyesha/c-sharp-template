﻿class _EventCreator
{
    constructor(_container, _data, _run = true) {
        this.container = _container;
        this.data = _data;
        this.HTML = null;
        this.Javascript = null;
        this.HTMLAdmin = null;
        this.JavascriptAdmin = null;
        this.Assets = null;
        if (_run) {
            this.Build();
        }
    }
    Build() {
        const _this = this;
        $(this.container).empty();
        $(this.container).append(`
                <h1>Create KPI Type</h1><hr>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button class="btn btn-secondary mr-1" type="button" data-toggle="collapse" data-target="#Info" aria-expanded="true" aria-controls="info">
                            Info
                            </button>
                            <button class="btn btn-secondary mr-1" type="button" data-toggle="collapse" data-target="#MetaData" aria-expanded="true" aria-controls="metaData">
                            Meta Data
                            </button>
                            <button class="btn btn-secondary mr-1" type="button" data-toggle="collapse" data-target="#Asset" aria-expanded="true" aria-controls="asset">
                            Assets
                            </button>
                            <button class="btn btn-secondary mr-1" type="button" data-toggle="collapse" data-target="#UserPage" aria-expanded="true" aria-controls="userPage">
                            User Page
                            </button>
                            <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#AdminPage" aria-expanded="true" aria-controls="adminPage">
                            Admin Page
                            </button>
                            <button id="saveCode" type="button" class="btn btn-primary btn-sm  ml-3">Save Code</button>
                        </div>
                    </div>
                    <div class="col-lg-12 mt-1">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button id="showCode" class="btn btn-small btn-secondary mr-1">
                            Show Code
                            </button>
                            <button id="showRender" class="btn btn-small  btn-secondary mr-1">
                            Show Render
                            </button>
                            <button id="showCodeRender" class="btn btn-small  btn-secondary mr-1">
                            Reset View
                            </button>
                        </div>
                    </div>
                </div>
                <div class="collapse show" id="Info">
                  <div class="card card-body">
                        <p>Information on how to use this page</p>
                        <ul>
                            <li>There will always be an asset "EventID_Detail" that can be used to call ajax for event data.</li>
                            <li>Admin/GroupFieldsGet - Provides all of the fields for the eventID</li>
                            <li>Admin/GroupGet - Provides all details for the Event</li>
                            <li>Assets will always be placed in an object "_AssetsCore"</li>
                        </ul>
                  </div>
                </div>
                <div class="collapse show" id="MetaData">
                  <div class="card card-body">
                        <div class="form-group">
                            <label for="xxTypexx" class="control-label col-lg-4">Type</label>
                            <div class="col-lg-8">
                                <input type="text" id="xxTypexx" class="form-control" value="${_this.data != null ?_this.data.EventCoreName :""}"/>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label for="xxFeatherxx" class="control-label col-lg-4">Feather</label>
                            <div class="col-lg-8">
                                <input type="text" id="xxFeatherxx" class="form-control" value="${_this.data != null ?_this.data.FeatherIcon: ""}" />
                            </div>
                        </div>  
                        <div class="form-group">
                            <label for="xxTitlexx" class="control-label col-lg-4">Page Title</label>
                            <div class="col-lg-8">
                                <input type="text" id="xxTitlexx" class="form-control" value="${_this.data != null ?_this.data.PageTitle: ""}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="xxMessagexx" class="control-label col-lg-4">Intro Message</label>
                            <div class="col-lg-8">
                                <input type="text" id="xxMessagexx" class="form-control" value="${_this.data != null ?_this.data.IntroMessage:""}" />
                            </div>
                        </div>
                  </div>
                </div>
                <div class="collapse show" id="Asset">
                  <div class="card card-body">
                        <label>JSON Assets <button id="executeAsset" class="btn btn-sm btn-primary">Load Asset</button></label>
                        <div id="CoreAsset" style="height:250px;width:100%;">

                        </div>
                  </div>
                </div>
                <div class="collapse" id="UserPage">
                  <div class="card card-body">
                <div class="row" style="width:100%">
                    <div class="col-lg-6 p-2 codeview">
                        
                        <label>HTML to Render</label>
                        <div id="panelHTML" class="codePanel" style="height:250px; width:500px">

                        </div>
                        <label> JS to Render <button id="executeJS" class="btn btn-sm btn-primary">Execute JS</button></label>
                        <div id="panelJava" class="codePanel" style="height:250px; width:500px">

                        </div>
                        
                    </div>
                    <div id="render" class="col-lg-5 ml-2 renderview">
                    </div>
                    
                </div>
                  </div>
                </div>
                <div class="collapse" id="AdminPage">
                  <div class="card card-body">
                <div class="row" style="width:100%">
                    <div class="col-lg-6 p-2 codeview">
                        
                        <label>HTML to Render</label>
                        <div id="panelHTMLAdmin" class="codePanel" style="height:250px; width:500px">

                        </div>
                        <label> JS to Render <button id="executeJSAdmin" class="btn btn-sm btn-primary">Execute JS</button></label>
                        <div id="panelJavaAdmin" class="codePanel" style="height:250px; width:500px">
                        </div>
                    </div>
                    <div id="renderAdmin" class="col-lg-5 ml-2 renderview">
                    </div>
                    
                </div>
                  </div>
                </div>

        `);
        //Assets
        _this.Assets = ace.edit("CoreAsset");
        _this.Assets.setTheme("ace/theme/twilight");
        _this.Assets.session.setMode("ace/mode/javascript");
        if (_this.data != null && _this.data.EventAsset[0].JsonAsset !== "") {
            _this.Assets.setValue(_this.data.EventAsset[0].JsonAsset);
            eval('_AssetsCore =' + _this.data.EventAsset[0].JsonAsset);
        }

        $('#executeAsset').on('click', function (e) {
            eval('_AssetsCore =' + _this.Assets.getValue());
        });

        //Users
        _this.HTML = ace.edit("panelHTML");
        _this.HTML.setTheme("ace/theme/twilight");
        _this.HTML.session.setMode("ace/mode/html");
        if (_this.data != null && _this.data.EventHTML[0].Code !== "") {
            _this.HTML.setValue(_this.data.EventHTML[0].Code);
            const r = _this.HTML.getValue();
            $('#render').append(r);

        }
        _this.HTML.session.on('change', function (delta) {
            // delta.start, delta.end, delta.lines, delta.action
            $('#render').empty();
            const r = _this.HTML.getValue();
            $('#render').append(r);
        });

        _this.Javascript = ace.edit("panelJava");
        _this.Javascript.setTheme("ace/theme/twilight");
        _this.Javascript.session.setMode("ace/mode/javascript");
        if (_this.data != null && _this.data.EventJavascript[0].Code !== "") {
            _this.Javascript.setValue(_this.data.EventJavascript[0].Code);
            //eval(_this.Javascript.getValue());
        }

        $('#executeJS').on('click', function (e) {
            eval(_this.Javascript.getValue());
        });

        //Admin
        _this.HTMLAdmin = ace.edit("panelHTMLAdmin");
        _this.HTMLAdmin.setTheme("ace/theme/twilight");
        _this.HTMLAdmin.session.setMode("ace/mode/html");
        if (_this.data != null && _this.data.EventAdminHTML[0].Code !== "") {
            _this.HTMLAdmin.setValue(_this.data.EventAdminHTML[0].Code);
            const r = _this.HTMLAdmin.getValue();
            $('#renderAdmin').append(r);

        }
        _this.HTMLAdmin.session.on('change', function (delta) {
            // delta.start, delta.end, delta.lines, delta.action
            $('#renderAdmin').empty();
            const r = _this.HTMLAdmin.getValue();
            $('#renderAdmin').append(r);
        });

        _this.JavascriptAdmin = ace.edit("panelJavaAdmin");
        _this.JavascriptAdmin.setTheme("ace/theme/twilight");
        _this.JavascriptAdmin.session.setMode("ace/mode/javascript");
        if (_this.data != null && _this.data.EventAdminJavascript[0].Code !== "") {
            _this.JavascriptAdmin.setValue(_this.data.EventAdminJavascript[0].Code);
            //eval(_this.JavascriptAdmin.getValue());
        }

        $('#executeJSAdmin').on('click', function (e) {
            eval(_this.JavascriptAdmin.getValue());
        });


        $('#saveCode').on('click', function (e) {

            const _form = new FormData();
            if (_this.data != null && _this.data.EventCoreID != "") {
                _form.append("Id", _this.data.EventCoreID);
            }
            _form.append("Type", $('#xxTypexx').val());
            _form.append("Feather", $('#xxFeatherxx').val());
            _form.append("Title", $('#xxTitlexx').val());
            _form.append("Message", $('#xxMessagexx').val());
            _form.append("HTML", encodeURIComponent(_this.HTML.getValue()));
            _form.append("Java", encodeURIComponent(_this.Javascript.getValue()));
            _form.append("HTMLAdmin", encodeURIComponent(_this.HTMLAdmin.getValue()));
            _form.append("JavaAdmin", encodeURIComponent(_this.JavascriptAdmin.getValue()));
            _form.append("CoreAsset", encodeURIComponent(_this.Assets.getValue()));
            const _ajaxService = new AJAXService(URLPath+'/Admin/EventSave', 'POST', _form, true);
            _ajaxService.promise.then(function (result) {
                const data = _ajaxService.Clean(result);
                console.log(data);
                _message.Build(data.Message, _message.Types.info, _message.Durations.moderate);
                const e = new _EventManager('#content', null);
            });

        });
        $('#showCode').on('click', function () {
            $('.codeview').removeClass('col-lg-6');
            $('.codeview').removeClass('hidden');
            $('.renderview').addClass('hidden');
            $('.codeview').addClass('col-lg-12');
            $('.codePanel').css({ 'height': '250px', 'width': '100%' });
        });
        $('#showRender').on('click', function () {
            $('.renderview').removeClass('col-lg-5');
            $('.renderview').removeClass('hidden');
            $('.codeview').addClass('hidden');
            $('.renderview').addClass('col-lg-12');
            $('.renderPanel').css({ 'height': '250px', 'width': '100%' });
        });
        $('#showCodeRender').on('click', function () {
            $('.codeview').removeClass('col-lg-12');
            $('.renderview').removeClass('col-lg-12');

            $('.codeview').addClass('col-lg-6');
            $('.renderview').addClass('col-lg-5');

            $('.renderview').removeClass('hidden');
            $('.codeview').removeClass('hidden');
            $('.codePanel').css({ 'height': '250px', 'width': '500px' });
            $('.renderPanel').css({ 'height': '250px', 'width': '500px' });
        });
    }


}