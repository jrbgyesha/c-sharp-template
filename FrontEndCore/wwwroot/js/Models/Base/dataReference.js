class dataReference {
    constructor(){
        this.dataReference = undefined;
        this.dataRefAttribute = undefined;
        this.dataRefAttributeReference = undefined;
        this.dataRefIsFormula = undefined;
        this.dataRefIsEval = undefined;
        this.dataRefIsVisibility = undefined;
        this.dataRefIsRowVisibility = undefined;
        this.dataRefSelect = undefined;
        this.dataRefRefreshOnPopulate = true;
        this.dataRefDisabledList = [];
        this.dataRefReverseDisabledList = [];
        this.dataRefDisabledReference = null
    }
}