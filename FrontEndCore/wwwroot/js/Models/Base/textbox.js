class textbox extends element {
    constructor(id, colSpan, name = "") {
        super(id, colSpan, name);
        this.type = "textbox";
        this.properties = Object.assign({
            value: "",
            label: "",
            labelHtml: "",
            dataSource: "",
            isDataSourceAPI: false,
            isCustomValidator: false,
            dataKey: null,
            dataValue: null,
            placeholder: "",
            required: false,
            invalidFeedback: "",
            invalidTooltip: "",
            subLabel: "",
            iconPrepend: null,
            iconAppend: null,
            isClearable: false,
            isHidden: false,
            isEmail: false,
            isPassword: false,
            wordCount: false,
            minLength: null,
            maxLength: null,
        },super.properties);
        this.plaintext = false,
        this.readonly = false; //exposing to extend property
    }
}
