﻿class element extends dataReference {
    constructor(id, colSpan = 12, name = "") {
        super();
        this.id = id;
        this.name = name;
        this.type = undefined;
        this.colSpan = colSpan;
        this.properties = {};
        this.dataIsClonable = false;
        this.offsetSpan = 0;
        this.isUserData = false;
        this.userProp = 'displayName'
        this.elementUserProp = 'value'
        this.referedClassElements = undefined;
        this.referedClassSum = undefined;
        //this.validatorProp = undefined; //new elementValidatorProp(undefined, undefined); //undefined for now.
        this.class = ""
    }
}
