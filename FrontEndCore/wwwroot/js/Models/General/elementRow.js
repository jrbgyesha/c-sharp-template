﻿class elementRow extends dataReference {
    constructor(order = 1) {
        super();
        this.elements = [];
        this.properties = {
            hrAbove: false,
            hrBelow: false,
            dotted: false,
        }
        this.order = order;
    }

    changeProperty(propertyName, newValue, elementID, elementType) {
        _changeProperty(this.elements, propertyName, newValue, elementID, elementType)
    }

    removeElement(elementID) {
        _removeElement(this.elements, elementID);
    }

    addElement(element) {
        var totalColSpan = this.elements.reduce((a, b) => a.colSpan + b.colSpan, 0)
        if (totalColSpan + element.colSpan > 12) {
            throw new Error(`Unable to add element with colSpan: ${element.colSpan}. Maximum is 12 and current total span is ${totalColSpan}`);
        }
        this.elements.push(Object.assign({}, element));
    }
}