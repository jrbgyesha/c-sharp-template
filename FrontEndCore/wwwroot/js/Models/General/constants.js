class constants {
    constructor(constantName, version, subVersion, isProd = false) {
        this.constantName = constantName;
        this.version = version;
        this.subVersion = subVersion;
        this.data = null;
        this.isProd = isProd;
        this.year = null;
    }
}