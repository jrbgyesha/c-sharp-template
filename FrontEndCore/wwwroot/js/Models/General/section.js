﻿class section extends dataReference {
    constructor(id, colSpan = 12, isSection = false, title = "", isCollapsable = false, isCollapsed = false) {
        super();
        this.id = id;
        this.colSpan = colSpan;
        this.isSection = isSection;
        this.title = title;
        this.titleHtml = '';
        this.isCollapsable = isCollapsable;
        this.isCollapsed = isCollapsed;
        this.groups = [];
        this.offsetSpan = 0;
        this.isHidden = false;
    }

    changeProperty(propertyName, newValue, elementID = undefined, elementType = undefined) {
        _changeProperty(this.groups, propertyName, newValue, elementID, elementType)
    }

    removeElement(elementID) {
        _removeElement(this.groups, elementID);
    }

    addGroup(group) {
        this.groups.push(group);
    }

    addElementRow(elementRow, groupId = `${this.id}DefaultGroup`) {
       var defaultGroupIndex = this.groups.findIndex((g) => g.id === groupId);

       if (defaultGroupIndex !== -1) {
           this.groups[defaultGroupIndex].addElementRow(elementRow);
       } else {
           var newDefaultGroup = new group(groupId, 12, elementRow.order);
           newDefaultGroup.addElementRow(elementRow);
           this.groups.push(newDefaultGroup);
       }
    }
}