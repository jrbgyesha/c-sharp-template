class pageData {
    constructor(title = "") {
        this.pageName = null;
        this.version = null;
        this.subVersion = null;
        this.year = null;
        this.country = [];
        this.department = [];
        this.title = title;
        this.icon = "";
        this.shortName = "";
        this.order = 0; //indicate same numner for autogrouping
        this.groupName = ""; //Indicate if there is grouping
        this.groupIcon = ""; //Indicate if there is grouping
        this.tabs = [];
        this.enabled = true;
        this.pageDataType = "";
        this.pageDataTypeDescription = "";
        this.pageDataStatusList = [];
        this.isProd = false;
        this.isHidden = false;
        // this.envMode = envMode.LOCAL; // No evnMode in .netCore
        this.validationList = []; //must be of type elementValidatorProp
        this.sectionTop = [];
        this.sectionBottom = [];
        this.toolbar = null;
        this.withRevised = false;
    }

    attachSectionTop(section) {
        this.sectionTop.push(section);
    }

    attachSectionBottom(section) {
        this.sectionBottom.push(section);
    }

    changeProperty(propertyName, newValue, elementID = undefined, elementType = undefined) {
        _changeProperty(this.tabs, propertyName, newValue, elementID, elementType)
    }

    removeElement(elementID) {
        _removeElement(this.tabs, elementID);
    }

    addTab(tab) {
        this.tabs.push(tab);
    }
}