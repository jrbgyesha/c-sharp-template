class button extends element {
    constructor(id, colSpan, name = "") {
        super(id, colSpan, name);

        this.type = "button";
        this.validationList = [];
        this.properties = Object.assign({
            iconPrepend: null,
            target: null,
            iconAppend: null,
            disabled: false,
            size: "",
            primaryOrSecondary: "primary",
            href: "#",
            typeProp: "button",
            submissionLink: "",
            label: "",
            labelHtml: "",
            method: "",
            dfRoute: "",
            waitOrchestrator: false,
            reLoadPage: true,
            tooltipPlacement: "top",
            tooltipMessage: null
        }, super.properties);
        this.confirmationMessage = null,
        this.confirmationMessageReference = null,
        this.onLoadMessage = null;
        this.onSuccessfulMessage = null;
        this.onFailedMessage = null;
    }

    addValidator(_elementValidatorProp) {
        // console.log("elementMode+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",_elementValidatorProp);
        
        // if (_elementValidatorProp instanceof elementValidatorProp) {
        //     this.validationList.push(_elementValidatorProp);
        // } else {
        //     throw new Error(`Unable to add Validator of type ${_elementValidatorProp.constructor.name || typeof _elementValidatorProp}`);
        // }
    }

    addValidatorSet(_elementValidatorSet){
        // var _elementValidatorList = Array.isArray(_elementValidatorSet) ? _elementValidatorSet : Object.keys(_elementValidatorSet).map(key => _elementValidatorSet[key]);
        // for(var _elementValidatorProp of _elementValidatorList){
        //     this.addValidator(_elementValidatorProp);
        // }
    }
}
