﻿class tab extends dataReference {
    constructor(id, showTab = false, tabDisplay = "", enableTab = true) {
        super();
        this.id = id;
        this.showTab = showTab;
        this.tabDisplay = tabDisplay;
        this.enableTab = enableTab;
        this.sections = [];
        this.class = '';
    }

    changeProperty(propertyName, newValue, elementID = undefined, elementType = undefined) {
        _changeProperty(this.sections, propertyName, newValue, elementID, elementType)
    }

    removeElement(elementID) {
        _removeElement(this.sections, elementID);
    }

    addSection(section, position = null) {
        this.sections.push(section);
        // this.sections.splice(position !== null ? position : this.sections.length, 0, section);
    }

    removeSection(currentSection) {
        this.sections = this.sections.filter(_section => _section.id !== currentSection.id);
    }
}