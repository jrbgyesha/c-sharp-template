﻿class group extends dataReference {
    constructor(id, colSpan = 12, order = 1) {
        super();
        this.id = id;
        this.colSpan = colSpan;
        this.elementRows = [];
        this.order = order;
    }

    changeProperty(propertyName, newValue, elementID, elementType) {
        _changeProperty(this.elementRows, propertyName, newValue, elementID, elementType)
    }

    removeElement(elementID) {
        _removeElement(this.elementRows, elementID);
    }

    addElementRow(elementRow) {
        this.elementRows.push(elementRow);
    }
}