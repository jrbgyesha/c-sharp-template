//for more details: http://developer.snapappointments.com/bootstrap-select/examples/
//You can grab more properties from the site and add it to below properties if needed.
class dropdown extends textbox {
    constructor(id, colSpan, name = "") {
        super(id, colSpan, name);

        this.type = "dropdown";
        this.properties = Object.assign({
            dataSource: "",
            dataSourceCode: [],
            isDataSourceAPI: false,
            isDataSourceJavascript: false,
            dataLiveSearch: false,
            dataMaxOptions: null,
            dataSelectedTextFormat: 'count > 5',
            multiple: false,
            dataKey: null,
            dataValue: null,
            dataDataValue: [],
            defultOnHidden: false,
            defaultValue: ""
        },super.properties);
        this.convertToNonEditableReference = null;
    }
}
