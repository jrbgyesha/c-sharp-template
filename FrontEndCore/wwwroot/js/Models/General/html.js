class html extends element {
    constructor(id, colSpan, name = "") {
        super(id, colSpan, name);
        this.type = "html";
        this.properties = Object.assign({
            code: ""
        }, super.properties);
    }
}