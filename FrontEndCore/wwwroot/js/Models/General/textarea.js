class textarea extends textbox {
    constructor(id, colSpan, name = "") {
        super(id, colSpan, name);

        this.type = "textarea";
        this.properties = Object.assign({
            rows: 5,
            label: ""
        },super.properties);
    }
}
