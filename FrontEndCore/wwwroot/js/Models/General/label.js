class label extends element {
    constructor(id, colSpan, name = "", value = null) {
        super(id, colSpan, name);

        this.type = "label";
        this.properties = Object.assign({
            isBold: false,
            value,
            labelHtml: "",
            subLabel: ""
        },super.properties);
    }
}