class toolbarButton extends button {
    constructor(id, colSpan = 12, name = ""){
        super(id, colSpan, name);
        this.type = 'toolbarButton'
        this.id = id;
        this.icon = "far fa-file-image";
        this.onclick = null;
        this.label = "Label"
        this.api = null;
        this.isAdmin = false;
    }
};