
class toolbar {
    constructor(id){
        this.id = id;
        this.toolbarButtonGroups = [];
    }

    addToolbarButtonGroup(toolbarButtonGroup, inFront = false){
        if(inFront){
            this.toolbarButtonGroups.unshift(toolbarButtonGroup);
        }else{
            this.toolbarButtonGroups.push(toolbarButtonGroup);
        }
    }
}