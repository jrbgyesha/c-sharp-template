class toolbarButtonToggle {
    constructor(id){
        this.type = 'toolbarButtonToggle'
        this.id = id;
        this.onChange = null;
        this.label = "Label"
        this.dataOnstyle = "dark"
        this.checked = true;
        this.isAdmin = false;
        this.dataRefAttributeReference = undefined;
        this.dataRefIsVisibility = undefined;
    }
}