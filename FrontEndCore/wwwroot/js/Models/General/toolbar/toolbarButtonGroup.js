class toolbarButtonGroup {
    constructor(id){
        this.id = id;
        this.groupName = "Group Name"
        this.toolbarButtons = [];
        this.isAdmin = false;
    }
    addToolbarButton(toolbarButton){
        this.toolbarButtons.push(toolbarButton);
    }
}
