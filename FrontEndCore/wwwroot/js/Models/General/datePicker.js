class datePicker extends textbox {
    constructor(id, colSpan, name = "") {
        super(id, colSpan, name);

        this.type = "datePicker";
        this.properties = Object.assign({
            
            iconPrepend: 'fa fa-calendar',
            format: 'DD/MM/YYYY',
            dateProp: {
                language: 'en-SG',
                startDate: null,
                endDate: null,
                startView: 0,
                weekStart: 1,
                autoHide: true
            },
            isClearable: true,
            showDateIcon: false
        },super.properties)
    }
}