﻿using DomainLayer;
using DomainLayer.Models;
using FrontEndCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Graph = Microsoft.Graph;
using Microsoft.Identity.Web;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using FrontEndCore.GraphServices;
using Microsoft.Extensions.Options;
using SQLDataLayer;
using FrontEndCore.Models.ViewModels;
using Newtonsoft.Json;
using DomainLayer.Services;
using DomainLayer.Services.Conditionals;
using FrontEndCore.Helpers.Filters;
using FrontEndCore.API;
using DomainLayer.Events;
using infra = FrontEndCore.Infrastructure.Constants;

namespace FrontEndCore.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        private readonly IGraphAuthService graphAuth;
        private readonly IControllerProvider ctrPvd;
        private readonly ICommandService<List<RegisterUser>> regUser;
        private readonly ICommandService<List<AddClaim>> regClaim;

        public HomeController(IGraphAuthService graphAuth, IControllerProvider ctrPvd, ICommandService<List<RegisterUser>> regUser, ICommandService<List<AddClaim>> regClaim)
        {
            if (graphAuth == null) throw new ArgumentNullException(nameof(graphAuth));
            if (ctrPvd == null) throw new ArgumentNullException(nameof(ctrPvd));
            if (regUser == null) throw new ArgumentNullException(nameof(regUser));
            if (regClaim == null) throw new ArgumentNullException(nameof(regClaim));


            this.graphAuth = graphAuth;
            this.ctrPvd = ctrPvd;
            this.regUser = regUser;
            this.regClaim = regClaim;
        }
        [AuthorizeForScopes(Scopes = new[] { GraphServices.Constants.ScopeUserRead })]
        [RegisterRequire] //Example of the Claim Filter for if user has registered
        [Data] //Example of a Data Filter For Checking if ID Exists
        public async Task<IActionResult> Index()
        {
            var test = Request.Headers["id"];
            return View();
        }

        [AuthorizeForScopes(Scopes = new[] { GraphServices.Constants.ScopeUserRead })]
        public async Task<IActionResult> Profile()
        {
            var u = await ctrPvd.GetCurrentUser(User);
            try
            {
                // Initialize the GraphServiceClient. 
                var me = await graphAuth.GetGraphUser(new[] { GraphServices.Constants.ScopeUserRead });
                if (me == null)
                    await ctrPvd.LogError(u.OID, "Home/Profile", "Graph object 'me' Is Null", "Error in Graph Auth");
                ViewData["Photo"] = await graphAuth.GetUserPhoto(new[] { GraphServices.Constants.ScopeUserRead });

                UserDetailsVM u_vm = new UserDetailsVM(me, ViewData["Photo"].ToString());
                u_vm.Registered = u.Claims.Any(x => x.Key == "registered" && x.Value == "true" && x.Type == ClaimType.registration);
                return View(u_vm);
            }
            catch (Exception ex)
            {
                await ctrPvd.LogError(u.OID, "Home/Profile", JsonConvert.SerializeObject(ex), ex.Message);
                throw new Exception(ex.Message, ex.InnerException);
                //return RedirectToAction("Error");
            }
        }


        [AuthorizeForScopes(Scopes = new[] { GraphServices.Constants.ScopeUserRead })]
        public async Task<JsonResult> Register()
        {
            var u = await ctrPvd.GetCurrentUser(User);
            try
            {
                await ApplyRegistration(u);
                return Json(ctrPvd.BuildReturnObject(infra.NotySuccess, "Registration Successful", new { }));
            }
            catch (Exception ex)
            {
                return Json(ctrPvd.LogError(u.OID, "Home/Register", ex.StackTrace, "There was an error with your Registration."));
            }
        }
        [AuthorizeForScopes(Scopes = new[] { GraphServices.Constants.ScopeUserRead })]
        public async Task<IActionResult> AutoRegister()
        {
            var u = await ctrPvd.GetCurrentUser(User);
            try
            {
                await ApplyRegistration(u);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        [AuthorizeForScopes(Scopes = new[] { GraphServices.Constants.ScopeUserRead })]
        public async Task<IActionResult> SignalRChat()
        {
            ViewData["Title"] = "Chat";
            return View();
        }

        private async Task ApplyRegistration(User u)
        {
            var me = await graphAuth.GetGraphUser(new[] { GraphServices.Constants.ScopeUserRead });
            if (me == null)
                await ctrPvd.LogError(u.OID, "Home/Register", "Graph object 'me' Is Null", "Error in Graph Auth");

            string photo = await graphAuth.GetUserPhoto(new[] { GraphServices.Constants.ScopeUserRead });

            var convertedGraph = await graphAuth.ConvertUser(me);
            convertedGraph.Add("userphoto", photo);

            List<RegisterUser> registrations = new List<RegisterUser>();
            registrations.Add(new RegisterUser { Key = UserPropertyKey.lastname, Value = convertedGraph.ContainsKey("surname") ? convertedGraph["surname"] : "", UserID = u.UserID });
            registrations.Add(new RegisterUser { Key = UserPropertyKey.firstname, Value = convertedGraph.ContainsKey("givenname") ? convertedGraph["givenname"] : "", UserID = u.UserID });
            registrations.Add(new RegisterUser { Key = UserPropertyKey.jobTitle, Value = convertedGraph.ContainsKey("jobtitle") ? convertedGraph["jobtitle"] : "", UserID = u.UserID });
            registrations.Add(new RegisterUser { Key = UserPropertyKey.department, Value = convertedGraph.ContainsKey("department") ? convertedGraph["department"] : "", UserID = u.UserID });
            registrations.Add(new RegisterUser { Key = UserPropertyKey.country, Value = convertedGraph.ContainsKey("country") ? convertedGraph["country"] : "", UserID = u.UserID });
            registrations.Add(new RegisterUser { Key = UserPropertyKey.email, Value = convertedGraph.ContainsKey("email") ? convertedGraph["email"] : "", UserID = u.UserID });
            registrations.Add(new RegisterUser { Key = UserPropertyKey.employeeID, Value = convertedGraph.ContainsKey("employeeid") ? convertedGraph["employeeid"] : "", UserID = u.UserID });
            registrations.Add(new RegisterUser { Key = UserPropertyKey.photo, Value = convertedGraph.ContainsKey("userphoto") ? convertedGraph["userphoto"] : "", UserID = u.UserID });

            await regUser.Execute(registrations);

            List<AddClaim> claims = new List<AddClaim>();
            claims.Add(new AddClaim
            {
                Key = "registered",
                Value = "true",
                Type = ClaimType.registration,
                UserID = u.UserID
            });

            await regClaim.Execute(claims);
        }

        [AllowAnonymous]
        public async Task<IActionResult> NoInternetExplorer()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                await ctrPvd.LogError("", "Home/NoInternetExplorer", ex.Message, JsonConvert.SerializeObject(ex));
                return RedirectToAction("Error");
            }
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
