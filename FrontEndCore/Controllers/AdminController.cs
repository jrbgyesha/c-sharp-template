﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DomainLayer.Services.Conditionals;
using DomainLayer.Services;
using DomainLayer.Events;
using DomainLayer.Models;
using FrontEndCore.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Identity.Web;
using FrontEndCore.GraphServices;

namespace FrontEndCore.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly IControllerProvider ctrPvd;
        public AdminController(IControllerProvider ctrPvd)
        {
            if (ctrPvd == null) throw new ArgumentNullException(nameof(ctrPvd));

            this.ctrPvd = ctrPvd;
        }
        //Note that these scopes are not required for this section; howeve, have been added for convenience
        [AuthorizeForScopes(Scopes = new[] { GraphServices.Constants.ScopeUserRead, GraphServices.Constants.ScopeUserReadAll, GraphServices.Constants.ScopeMailSend })]
        public async Task<IActionResult> Index()
        {
            var u = await ctrPvd.GetCurrentUser(User);
            if (User.HasClaim("role", "super admin") || User.HasClaim("role", "admin"))
            {
                //To Add This claim to the user...add in teh dbo.Claims for your profile.
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
    }
}