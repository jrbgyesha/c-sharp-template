﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FrontEndCore.GraphServices;
using DomainLayer;
using InMemoryDataLayer;
using DomainLayer.Repositories;
using System;
using DomainLayer.Models;
using System.Linq;
using DomainLayer.Events;
using DomainLayer.Services;
using FrontEndCore.Hubs;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.StaticFiles;
using SQLDataLayer;

namespace FrontEndCore
{
    public class MockStartup
    {

        public MockStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //Add Standard Services
            services.ConfigureStandardServices(Configuration);
            services.AddDbContext<SQLDataContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DatabaseContext"), (sqlServerOptions) => {
                    sqlServerOptions.EnableRetryOnFailure(
                            maxRetryCount: 5,
                            maxRetryDelay: TimeSpan.FromSeconds(5),
                            errorNumbersToAdd: null);
                });

            });

            services.AddScoped<IRepositoryService<User>, SqlUserRepository>();
            services.AddScoped<IRepositoryService<UserAudit>, SqlUserAuditRepository>();
            services.AddScoped<IRepositoryService<UserProperty>, SqlUserPropertyRepository>();
            services.AddScoped<IRepositoryService<Claim>, SqlClaimRepository>();
            services.AddScoped<IRepositoryService<Log>, SqlLogRepository>();
            //services.AddDbContext<InMemDataContext>(options =>
            //{
            //    options.UseSqlServer(Configuration.GetConnectionString("TempDatabase"), (sqlServerOptions) =>
            //    {
            //        sqlServerOptions.EnableRetryOnFailure(
            //                maxRetryCount: 5,
            //                maxRetryDelay: TimeSpan.FromSeconds(5),
            //                errorNumbersToAdd: null);
            //    });
            //});

            //services.AddScoped<IRepositoryService<User>, InMemUserRepository>();
            //services.AddScoped<IRepositoryService<UserAudit>, InMemUserAuditRepository>();
            //services.AddScoped<IRepositoryService<UserProperty>, InMemUserPropertyRepository>();
            //services.AddScoped<IRepositoryService<Claim>, InMemClaimRepository>();
            //services.AddScoped<IRepositoryService<Log>, InMemLogRepository>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Test Scheme";
                options.DefaultChallengeScheme = "Test Scheme";
            }).AddMockAuth(o => { });

            // Add Graph
            services.AddScoped<IGraphAuthService, MockAuthService>();
            services.AddScoped<IGraphConverterService, GraphConverterService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseDeveloperExceptionPage();


            app.UseHttpsRedirection();
            //EJS Support
            app.UseStaticFiles(new StaticFileOptions { 
                FileProvider = new PhysicalFileProvider(env.WebRootPath + @"\js"),
                RequestPath = "/js",
                ContentTypeProvider = new FileExtensionContentTypeProvider
                {
                    Mappings =
                    {
                        [".ejs"] = "text/html"
                    }
                }
            });
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name: "apis",
                    pattern: "api/[controller]"
                    );
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}"
                    );
                endpoints.MapRazorPages();
                endpoints.MapHub<ExampleHub>("/exampleHub");
            });
            DataGenerator.Initialize(app);
        }
    }

    public class DataGenerator
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<SQLDataContext>();
                context.Database.EnsureCreated();
                // Look for any board games.
                if (context.Users.Any())
                {
                    return;   // Data was already seeded
                }

                //GetUser(context);

                
                //var users = context.Users.ToList();
            }
        }
        private static void GetUser(SQLDataContext context)
        {
            var user = new User { OID = "23305374-74c2-4cd4-b8b5-5999902f8e09", IsDeleted = false };
            context.Users.Add(user);
            context.SaveChanges();
            context.UserProperties.AddRange(
                new UserProperty { Key = UserPropertyKey.lastname, Type = PropertyDataType._string, Value = "Furrer", UserID = user.UserID },
                new UserProperty { Key = UserPropertyKey.firstname, Type = PropertyDataType._string, Value = "Tyler", UserID = user.UserID },
                new UserProperty { Key = UserPropertyKey.jobTitle, Type = PropertyDataType._string, Value = "Senior Manager", UserID = user.UserID },
                new UserProperty { Key = UserPropertyKey.department, Type = PropertyDataType._string, Value = "Tax -Digital Transformation", UserID = user.UserID },
                new UserProperty { Key = UserPropertyKey.country, Type = PropertyDataType._string, Value = "Singapore", UserID = user.UserID },
                new UserProperty { Key = UserPropertyKey.email, Type = PropertyDataType._string, Value = "tfurrer@deloitte.com", UserID = user.UserID }
                );

            context.Claims.AddRange(
                new Claim { Key = "registered", Type = ClaimType.registration, Value = "true", UserID = user.UserID },
                new Claim { Key = "country", Type = ClaimType.registration, Value = "Singapore", UserID = user.UserID },
                new Claim { Key = "function", Type = ClaimType.registration, Value = "tax", UserID = user.UserID },
                new Claim { Key = "designation", Type = ClaimType.registration, Value = "7", UserID = user.UserID },
                new Claim { Key = "role", Type = ClaimType.role, Value = "super admin", UserID = user.UserID }
                );
            context.UserAudits.AddRange(
                new UserAudit { Date = DateTime.Now, Key = UserAuditKey.CreatedProfile, Value = "", UserID = user.UserID }
                );
            context.SaveChanges();
        }
    }
}
