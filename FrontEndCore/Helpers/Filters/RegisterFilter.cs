﻿using DomainLayer.Models;
using DomainLayer.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FrontEndCore.Helpers.Filters
{

    public class RegisterRequireAttribute : TypeFilterAttribute
    {
        public RegisterRequireAttribute() : base(typeof(RegisterClaimFilter))
        {
            Arguments = new object[] { new System.Security.Claims.Claim("registered", "true") };
        }
    }

    public class RegisterClaimFilter : IAuthorizationFilter
    {
        readonly System.Security.Claims.Claim _claim;

        public RegisterClaimFilter(System.Security.Claims.Claim claim)
        {
            _claim = claim;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var hasClaim = context.HttpContext.User.Claims.Any(c => c.Type == _claim.Type && c.Value == _claim.Value);
            if (!hasClaim)
            {
                if(Environment.GetEnvironmentVariable("UseRegistrationPage") =="false")
                    context.Result = new RedirectResult("/Home/AutoRegister");
                else
                    context.Result = new RedirectResult("/Home/Profile");
            }
        }
    }
}
