﻿using DomainLayer.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using DomainLayer.Models;

namespace FrontEndCore.Helpers.Filters
{
    public class DataAttribute: ActionFilterAttribute
    {
        public DataAttribute()
        {
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            IUserService svc = context.HttpContext.RequestServices.GetService<IUserService>();
            var x = Task.Run<IEnumerable<User>>(async () => await svc.GetUsers());
            var result = x.Result;
            if (!context.HttpContext.Request.Headers.ContainsKey("id"))
                context.HttpContext.Request.Headers.Add("id","1");
            base.OnActionExecuting(context);
        }
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            base.OnResultExecuting(context);
        }
    }
}
