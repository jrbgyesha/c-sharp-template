﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DomainLayer.Events;
using DomainLayer.Models;
using DomainLayer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using infra = FrontEndCore.Infrastructure.Constants;

namespace FrontEndCore.API
{
    public interface IControllerProvider
    {
        object BuildReturnObject(string Status, string Message, object Data);
        Task<User> GetCurrentUser(ClaimsPrincipal user);
        Task<List<User>> AllUsers();
        Task LogEvent(AddLog log);
        Task<object> LogError(string oid, string pagename, string sysmessage, string message);
    }
    public class ControllerProvider : IControllerProvider
    {
        private readonly IUserService usersvc;
        private readonly ICommandService<AddLog> logger;
        public ControllerProvider(IUserService usersvc, ICommandService<AddLog> logger)
        {
            if (usersvc == null) throw new ArgumentNullException(nameof(usersvc));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            this.usersvc = usersvc;
            this.logger = logger;
        }

        public object BuildReturnObject(string Status, string Message, object Data)
        {
            return new { Status, Message, Data };

        }

        public async Task<User> GetCurrentUser(ClaimsPrincipal user)
        {
            var oid = user.Claims?.FirstOrDefault(x => x.Type.Equals("http://schemas.microsoft.com/identity/claims/objectidentifier", StringComparison.OrdinalIgnoreCase))?.Value;
            return await usersvc.GetUser(oid);
        }

        public async Task<List<User>> AllUsers()
        {
            var result = await usersvc.GetUsers();
            return result.ToList();
        }
        public async Task LogEvent(AddLog log)
        {
            await logger.Execute(log);
        }

        public async Task<object> LogError(string oid, string pagename, string sysmessage, string message)
        {

            var ticket = Guid.NewGuid().ToString();
            List<LoggerProp> logs = new List<LoggerProp> { new LoggerProp { Key = "ticket", Value = ticket },
                    new LoggerProp { Key = "message", Value = sysmessage} };
            await this.LogEvent(new AddLog { OID = oid, PageName = pagename, Props = JsonConvert.SerializeObject(logs) });
            return this.BuildReturnObject(infra.NotyError, message + " - Ticket: " + ticket, new { });

        }
    }
}
