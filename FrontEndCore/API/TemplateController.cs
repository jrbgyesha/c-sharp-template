﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FrontEndCore.API
{
    [Route("api/Template")]
    [ApiController]
    public class TemplateController : ControllerBase
    {
        private readonly IControllerProvider ctrPvd;
        public TemplateController(IControllerProvider ctrPvd)
        {
            if (ctrPvd == null) throw new ArgumentNullException(nameof(ctrPvd));
            this.ctrPvd = ctrPvd;
        }
        //options
        //[FromBody]
        //[FromForm]
        //List<IFormFile> for Files


        // GET: api/Template
        [HttpGet]
        public async Task<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Template/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<string> Get(int id)
        {
            return "value";
        }

        // POST: api/Template
        [HttpPost]
        public async Task Post([FromBody] string value)
        {
        }

        // PUT: api/Template/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
        }
    }
}
