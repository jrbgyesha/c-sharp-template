﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using infra = FrontEndCore.Infrastructure.Constants;

namespace FrontEndCore.API
{
    [Route("api/Component")]
    [ApiController]
    public class ComponentController : ControllerBase
    {
        private readonly IControllerProvider ctrPvd;
        public ComponentController(IControllerProvider ctrPvd)
        {
            if (ctrPvd == null) throw new ArgumentNullException(nameof(ctrPvd));
            this.ctrPvd = ctrPvd;

        }

        [HttpGet("SideBar", Name = "SideBar")]
        public async Task<object> SideBar([FromQuery] string role = "user")
        {
            List<object> sideBarData = new List<object>();
            if (role == "user")
            {
                if (User.HasClaim("registered", "true"))
                    sideBarData = await GetSideBarUser(await ctrPvd.GetCurrentUser(User));
                return ctrPvd.BuildReturnObject(infra.NotySuccess, "SideBar Captured", sideBarData);
            }
            else
            {
                if (User.HasClaim("role", "super admin") || User.HasClaim("role", "admin"))
                {
                    if (User.HasClaim("registered", "true"))
                        sideBarData = await GetSideBarAdmin(await ctrPvd.GetCurrentUser(User));
                    return ctrPvd.BuildReturnObject(infra.NotySuccess, "SideBar Captured", sideBarData);
                }
                return ctrPvd.BuildReturnObject(infra.NotyWarn, "Not Allowed", new object());
            }
        }


        private void AddSiderBarUser(List<object> sideBarData, int maxOrder, User u)
        {
            sideBarData.Add(new
            {
                _order = 1,
                _id = "sys_template",
                _type = "fas fa-user-astronaut fa-spin",
                _label = "Template",
                _class = "animated fadeIn",
                _data = "sys",
                _attrs = ""
            });

             sideBarData.Add (new {
                _order = 2,
                    _id = "sys_EJSSampleUsage",
                    _type = "fas fa-globe-americas fa-spin",
                    _label = "EJS Template Sample",
                    _class = "animated fadeIn",
                    _data = "sys",
                    _attrs = ""
            });

            sideBarData.Add (new {
                _order = 3,
                    _id = "sys_EJSSampleUsage2",
                    _type = "fas fa-cat",
                    _label = "EJS Template Sample 2",
                    _class = "animated fadeIn",
                    _data = "sys",
                    _attrs = ""
            });

        }
        private void AddSiderBarAdmins(List<object> sideBarData, int maxOrder, User u)
        {
            sideBarData.Add(new
            {
                _order = maxOrder++,
                _id = "spacer",
                _type = "",
                _label = "",
                _class = "animated fadeIn",
                _data = "sys",
                _attrs = ""
            });
            sideBarData.Add(new
            {
                _order = maxOrder++,
                _id = "reports",
                _type = "fas fa-chart-pie",
                _label = "Reports",
                _class = "animated fadeIn",
                _data = "sys",
                _attrs = ""
            });
            sideBarData.Add(new
            {
                _order = maxOrder++,
                _id = "users",
                _type = "fas fa-user",
                _label = "Users",
                _class = "animated fadeIn",
                _data = "sys",
                _attrs = ""
            });
            sideBarData.Add(new
            {
                _order = maxOrder++,
                _id = u.Claims.Any(x => x.Key == "role" && x.Value == "super admin") ? "adminevents" : "events",
                _type = "fab fa-codepen",
                _label = "Events",
                _class = "animated fadeIn",
                _data = "sys",
                _attrs = ""
            });
            if (u.Claims.Any(x => x.Key == "role" && x.Value == "super admin"))
            {
                sideBarData.Add(new
                {
                    _order = maxOrder++,
                    _id = "supergroup",
                    _type = "shuffle",
                    _label = "Super Admin",
                    _class = "animated fadeIn",
                    _data = "sys",
                    _attrs = ""
                });
            }
        }


        private async Task<List<object>> GetSideBarUser(User u)
        {
            List<object> sideBarData = new List<object>();
            AddSiderBarUser(sideBarData, 1, u);
            return await Task.FromResult(sideBarData);
        }
        private async Task<List<object>> GetSideBarAdmin(User u)
        {
            List<object> sideBarData = new List<object>();
            AddSiderBarAdmins(sideBarData, 1, u);
            return await Task.FromResult(sideBarData);
        }
    }
}