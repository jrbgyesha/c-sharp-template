﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace FrontEndCore.Hubs
{
    public static class UserHandler
    {
        public static HashSet<string> ConnectedIds = new HashSet<string>();
        public static HashSet<string> ConnectedNames = new HashSet<string>();
    }
    public class ExampleHub : Hub
    {
        public async Task Connect()
        {
            var name = Context.User.Identities.Where(x => x.AuthenticationType != "GenericSystemName").FirstOrDefault()?.Claims.Where(x => x.Type == "name").FirstOrDefault()?.Value ?? "";
            if (!UserHandler.ConnectedNames.Any(y => y == name))
            {
                UserHandler.ConnectedNames.Add(name);
                await Clients.All.SendAsync("Connections", name);
            }

        }
        public override Task OnConnectedAsync()
        {
            UserHandler.ConnectedIds.Add(Context.ConnectionId);

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            UserHandler.ConnectedIds.Remove(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}
