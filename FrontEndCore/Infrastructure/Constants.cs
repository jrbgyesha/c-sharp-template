﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndCore.Infrastructure
{
    public static class Constants
    {
        public const string ReportsFolder = "Reports\\";
        public const string NotyWarn = "warning";
        public const string NotySuccess = "success";
        public const string NotyInfo = "info";
        public const string NotyError = "error";
        public const string NotyIgnore = "ignore";



        public const string ScopeUserRead = "User.Read";
        public const string BearerAuthorizationScheme = "Bearer";
    }
}
