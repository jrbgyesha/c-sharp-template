﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndCore.Models.ViewModels
{
    public class UserDetailsVM
    {
        public string Phone { get; set; }
        public string Country { get; set; }
        public string Company { get; set; }
        public string Department { get; set; }
        public string JobTitle { get; set; }
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string eMail { get; set; }
        public string CareerLevel { get; set; }
        public string UserName { get; set; }
        public string LastSync { get; set; }
        public string Enabled { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string StreetAddress { get; set; }
        public string Postal { get; set; }
        public string Photo { get; set; }
        public bool Registered { get; set; } = false;

        public UserDetailsVM() { }
        public UserDetailsVM(Microsoft.Graph.User me, string photo)
        {

            this.Phone = "";// me.BusinessPhones != null ? me.BusinessPhones.FirstOrDefault()?.ToString() ?? "" : "";
            this.Country = me.Country ?? "";
            this.Company = me.CompanyName ?? "";
            this.Department = me.Department ?? "";
            this.JobTitle = me.JobTitle ?? "";
            this.EmployeeId = me.EmployeeId ?? "";
            this.FirstName = me.GivenName ?? "";
            this.LastName = me.Surname ?? "";
            this.eMail = me.Mail ?? "";
            this.CareerLevel = "1";//me.OnPremisesExtensionAttributes != null && me.OnPremisesExtensionAttributes.ExtensionAttribute8 != null ? me.OnPremisesExtensionAttributes.ExtensionAttribute8 : "1";
            this.UserName = me.OnPremisesSamAccountName ?? "";
            this.LastSync = ""; //me.OnPremisesLastSyncDateTime?.ToString() ?? "";
            this.Enabled = "true";//me.AccountEnabled?.ToString() ?? "";


            this.City = me.City ?? "";
            this.State = me.State ?? "";
            this.StreetAddress = me.StreetAddress ?? "";
            this.Postal = me.PostalCode ?? "";
            this.Photo = photo ?? "";
        }
    }

}