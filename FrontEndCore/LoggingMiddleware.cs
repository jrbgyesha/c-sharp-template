﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndCore
{
    public class LoggingMiddleware
    {
        private readonly ILogger logger;

        public LoggingMiddleware(ILogger logger)
        {
            this.logger = logger;
        }

        public async Task Invoke(HttpContext context, Func<Task> next)
        {
            this.logger.LogInformation("Request started");
            await next();
            this.logger.LogInformation("Request ended");
        }
    }
}
