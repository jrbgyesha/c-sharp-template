﻿using DomainLayer;
using DomainLayer.Events;
using DomainLayer.Services;
using DomainLayer.Services.Conditionals;
using DomainLayer.Services.CypherProviders;
using DomainLayer.Services.FileProviders;
using FrontEndCore.API;
using FrontEndCore.Controllers;
using FrontEndCore.GraphServices;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SQLDataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndCore
{
    public static class StartupExtensionLoader
    {
        public static void ConfigureStandardServices(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddOptions();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            //Standard Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IClaimsTransformation, ClaimsTransformer>();
            services.AddScoped<ICypherProvider, FileCyper>();
            services.AddScoped<IFileProvider, UserFileProvider>();
            services.AddScoped<IControllerProvider, ControllerProvider>();

            services.AddScoped<ICommandService<InsertUserProperty>, InsertUserPropertyService>();
            services.AddScoped<ICommandService<UpdateUserProperty>, UpdateUserPropertyService>();
            services.AddScoped<ICommandService<List<RegisterUser>>, RegisterUserService>();
            services.AddScoped<ICommandService<List<AddClaim>>, AddClaimService>();
            services.AddScoped<ICommandService<RemoveClaim>, RemoveClaimService>();
            services.AddScoped<ICommandService<AddLog>, AddLogService>();
            //Standard Services

            services.AddScoped<IEventHandler<UserUpdated>, NotifyUserOfUpdate>();

            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
                
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
            .ConfigureApiBehaviorOptions(options => {
                options.SuppressModelStateInvalidFilter = true;
            });
            services.Configure<ApiBehaviorOptions>(apiBehaviorOptions => {
                apiBehaviorOptions.InvalidModelStateResponseFactory = actionContext => {
                    var pd = new ProblemDetails();
                    pd.Type = apiBehaviorOptions.ClientErrorMapping[400].Link;
                    pd.Title = apiBehaviorOptions.ClientErrorMapping[400].Title;
                    pd.Status = 400;
                    pd.Extensions.Add("traceId", actionContext.HttpContext.TraceIdentifier);
                    return new BadRequestObjectResult(pd);
                };
            });

            services.AddSignalR();

            services.AddHttpContextAccessor();

            services.AddTransient<ConditionalA>();
            services.AddTransient<ConditionalB>();

            services.AddScoped(svp =>
            {
                var http = svp.GetRequiredService<IHttpContextAccessor>().HttpContext;
                if (http == null)
                    return null;
                var name = http.GetRouteData().Values["controller"].ToString();
                switch (name)
                {
                    case "Home":
                        return svp.GetService<ConditionalA>() as IConditionalService;
                    case "Admin":
                        return svp.GetService<ConditionalB>() as IConditionalService;
                    default:
                        return svp.GetService<ConditionalA>() as IConditionalService;
                }

            });
        }


    }


}
