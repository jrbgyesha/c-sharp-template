﻿using DomainLayer.Models;
using DomainLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryDataLayer
{

    public class InMemLogRepository : IRepositoryService<Log>
    {
        private readonly InMemDataContext context;

        public InMemLogRepository(InMemDataContext context)
        {
            if (context == null) throw new ArgumentNullException("context not set");
            this.context = context;
        }

        public async Task AddMany(List<Log> items)
        {
            await context.Logs.AddRangeAsync(items);
            await context.SaveChangesAsync();
        }

        public async Task AddSingle(Log item)
        {
            await context.Logs.AddAsync(item);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Log>> All()
        {
            return await context.Logs.ToListAsync();
        }

        public async Task<IEnumerable<Log>> GetLogs()
        {
            return await Task.FromResult(this.context.Logs);
        }

        public async Task Remove(Log item)
        {
            context.Logs.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task<Log> Single(int id)
        {
            return await context.Logs.Where(x => x.LogID == id).FirstOrDefaultAsync();
        }

        public async Task UpdateMany(List<Log> items)
        {
            context.Logs.UpdateRange(items);
            await context.SaveChangesAsync();
        }

        public async Task UpdateSingle(Log item)
        {
            context.Logs.Update(item);
            await context.SaveChangesAsync();
        }
    }

}
