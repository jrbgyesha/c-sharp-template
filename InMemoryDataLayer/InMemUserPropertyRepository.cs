﻿using DomainLayer.Models;
using DomainLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryDataLayer
{
    public class InMemUserPropertyRepository : IRepositoryService<UserProperty>
    {
        private readonly InMemDataContext context;

        public InMemUserPropertyRepository(InMemDataContext context)
        {
            if (context == null) throw new ArgumentNullException("context not set");
            this.context = context;
        }

        public async Task AddMany(List<UserProperty> items)
        {
            await context.UserProperties.AddRangeAsync(items);
            await context.SaveChangesAsync();
        }

        public async Task AddSingle(UserProperty item)
        {
            await context.UserProperties.AddAsync(item);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserProperty>> All()
        {
            return await context.UserProperties.ToListAsync();
        }

        public async Task<IEnumerable<UserProperty>> GetUserProperties()
        {
            return await Task.FromResult(this.context.UserProperties);
        }

        public async Task Remove(UserProperty item)
        {
            context.UserProperties.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task<UserProperty> Single(int id)
        {
            return await context.UserProperties.Where(x => x.UserPropertyID == id).FirstOrDefaultAsync();
        }

        public async Task UpdateMany(List<UserProperty> items)
        {
            context.UserProperties.UpdateRange(items);
            await context.SaveChangesAsync();
        }

        public async Task UpdateSingle(UserProperty item)
        {
            context.UserProperties.Update(item);
            await context.SaveChangesAsync();
        }
    }
}
