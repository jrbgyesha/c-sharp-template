ASP.NET Core Template
============

ASP.NET Core is an open-source and cross-platform framework for building modern cloud based internet connected applications, such as web apps, IoT apps and mobile backends. ASP.NET Core apps run on [.NET Core](https://dot.net), a free, cross-platform and open-source application runtime. It was architected to provide an optimized development framework for apps that are deployed to the cloud or run on-premises. It consists of modular components with minimal overhead, so you retain flexibility while constructing your solutions. You can develop and run your ASP.NET Core apps cross-platform on Windows, Mac and Linux. [Learn more about ASP.NET Core](https://docs.microsoft.com/aspnet/core/).

## Get Started

Follow the [Getting Started](https://docs.microsoft.com/aspnet/core/getting-started) instructions in the [ASP.NET Core docs](https://docs.microsoft.com/aspnet/index).

Also check out the [.NET Homepage](https://www.microsoft.com/net) for released versions of .NET, getting started guides, and learning resources.

See the [Issue Management Policies](https://github.com/dotnet/aspnetcore/blob/master/docs/IssueManagementPolicies.md) document for more information on how we handle incoming issues.

## How to Engage, Contribute, and Give Feedback

Some of the best ways to contribute are to try things out, file issues, join in design conversations,
and make pull-requests.

## Getting Started

This template makes heavy use of Dependency Injection. We recommend that you read a few articles related to dependency injection so that you are familiar with the basic concepts.
* https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-3.1
* https://stackify.com/solid-design-principles/

The basic structure we use here is a frameworkless front end combined with a MVC/API hybrid backend. The backend is split into three major sections:

1. Front-End Core - Contains all of the front end logic and views.
2. Domain Layer - Contains the models, interfaces, services, and commands
3. Repositories - Contain the database and repository classes

We have also included some basic structures for the following:

1. SignalR communications
2. Conditional Dependency Injection
3. Command Query Separation
4. Pure Javascript/HTML frontend
5. Mocking vs Production Authentication and DB Dependency Injection

You may need to connect to your local database. This can be achieved by downloading SSMS (SQL Server Management Studio) and loading your localdb instance. 
In some cases your (localdb)\\mssqllocaldb will not show up in SSMS. You should open "cmd" and type "SQLLocalDB.exe info MSSQLLocalDB". Copy the Instance pipe name value and paste it into the "Server Name" on SSMS.

Clone the Site using cmd in your repo folder
```
SET nvar=MyNewTemplate
git clone https://tfurrer.visualstudio.com/Project%20overview/_git/ASPNetCoreAAD_Graph_DI_WithMocking %nvar%
cd %nvar%
dotnet restore
echo Complete
echo Done
```


## Build Status

[![Build Status](https://tfurrer.visualstudio.com/Project%20overview/_apis/build/status/ASPNetCoreAAD_Graph_DI_WithMocking?branchName=master)](https://tfurrer.visualstudio.com/Project%20overview/_build/latest?definitionId=33&branchName=master)

# Recent Changes

1. Upgraded to ASP.Net Core 3.1

